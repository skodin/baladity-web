<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Citoyen;
use App\Models\Probleme;

class Suivre extends Model
{
	
	public $timestamps = true;
	
	
    protected $table="suivre";
	
	
	protected $fillable = [
        'idCitoyen','idReclamation'
    ];
	
	
	
	
	
	
	//relation with Citoyen
	
	public function citoyen(){
		
		 return $this->belongsTo(Citoyen::class,'idCitoyen');
		
	}
	
	
	//relation with Probleme
	
	public function probleme(){
		
		 return $this->belongsTo(Probleme::class,'idReclamation');
		
	}
	
	
	
	
	
	
	
	
	
	
}
