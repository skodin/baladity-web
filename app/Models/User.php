<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Admin;
use App\Models\Citoyen;
use App\Models\Agent;
use App\Models\SuperAdmin;
use App\Models\Commentaire;

class User extends Authenticatable
{
	
	
	public $timestamps = true;
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     
    protected $fillable = [
        'username','nom','prenom', 'email', 'password','photo','role'
    ];

     /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
     
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	
	
	
	//relation with Semblable
	
	public function semblables(){
		
		 return $this->hasMany(Semblable::class,'idUser');
		
	}
	
	
	
	//relation with Admin
	
	public function admin(){
		
		 return $this->hasOne(Admin::class,'idUser');
		
	}
	
	//relation with Super Admin
	
	public function superadmin(){
		
		 return $this->hasOne(SuperAdmin::class,'idUser');
		
	}
	
	
	//relation with Agent
	
	public function agent(){
		
		 return $this->hasOne(Agent::class,'idUser');
		
	}
	
	//relation with Citoyen
	
	public function citoyen(){
		
		 return $this->hasOne(Citoyen::class,'idUser');
		
	}
	
	
	//relation with commentaire 
	public function commentaires(){
		 return $this->hasMany(Commentaire::class,'idUser');
	}
	
	
}
