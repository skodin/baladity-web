<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Categorieetablissement;
use App\Models\Probleme;

class Categorieprobleme extends Model
{
    public $timestamps = true;


    protected $table = 'categoriesproblemes';

    protected $fillable = [
        'nom'
    ];


    //relation with probleme 
    public function problemes()
    {
        return $this->hasMany(Probleme::class, 'idCategorieProleme');
    }

    


}
