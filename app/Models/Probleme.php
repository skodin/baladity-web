<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Commentaire;
use App\Models\Citoyen;
use App\Models\Affectation;
use App\Models\Modification;
use App\Models\Categorieprobleme;
use App\Models\Delegation;
use App\Models\Suivre;
use App\Models\Semblable;
use App\Models\Photo;

class Probleme extends Model
{
	public $timestamps = true;
	
	protected $table="reclamations";
    	
    protected $fillable = [
        'degree','description','flag','longitude','latitude','idCitoyen','idCategorieProleme','idEtat'
    ];
	
	//relation with photo 
	
	public function photos(){
		
		 return $this->hasMany(Photo::class,'idReclamation');
	}
	
	
	//relation with semblable 
	public function semblables(){
		
		 return $this->hasMany(Semblable::class,'idReclamation1');
	}
	
	//relation with commentaire 
	public function commentaires(){
		 return $this->hasMany(Commentaire::class,'idReclamation')->orderBy('updated_at','asc');;
	}
	
	//relation with commentaire 
	public function suivres(){
		 return $this->hasMany(Suivre::class,'idReclamation');
	}
	
	
	//relation with citoyen 
	public function citoyen(){
		 return $this->belongsTo(Citoyen::class,'idCitoyen');
	}
	
	
	
	//relation with Modification
	
	public function modifications(){
		
		 return $this->hasMany(Modification::class,'idReclamation')->orderBy('date','desc');
		
	}
	
	
	//relation with categorieProleme 
	public function categorieproleme(){
		 return $this->belongsTo(Categorieprobleme::class,'idCategorieProleme');
	}
	
	
	
	
	
	
}
