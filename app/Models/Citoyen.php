<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Probleme;
use App\Models\Suivre;

class Citoyen extends Model
{
	
	public $timestamps = true;
	
	
	public $primaryKey  = 'idUser';
	
	
     protected $fillable = [
        'idUser','telephone','estBlocker','estConfirmer'
    ];
	
	
	//relation with User
	
	public function user(){
		 return $this->belongsTo(User::class,'idUser');
	}
	
	
	//relation with probleme
	
	public function problemes(){
		
		 return $this->hasMany(Probleme::class,'idCitoyen');
		
	}
	
	
	//relation with probleme
	
	public function suivres(){
		
		 return $this->hasMany(Suivre::class,'idCitoyen');
		
	}
	
	
	
	
}
