<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Probleme;

class Photo extends Model
{
    protected $table='photos';
	
	public $timestamps = true;
	   	
    protected $fillable = [
        'chemin','idReclamation'
    ];
	
	
	//relation with reclamation 
	public function reclamation(){
		
		 return $this->belongsTo(Probleme::class,'idReclamation');
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
