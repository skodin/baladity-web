<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Probleme;

class Commentaire extends Model
{
    public $timestamps = true;
 
 
  protected $fillable = [
        'message','date','idUser', 'idReclamation','estAbuse'
    ];
   
 
  //relation with user 
	public function user(){
		 return $this->belongsTo(User::class,'idUser');
	}
	
	 //relation with user 
	public function problem(){
		 return $this->belongsTo(Probleme::class,'idReclamation');
	}
 
 
    
}
