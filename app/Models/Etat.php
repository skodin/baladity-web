<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Modification;

class Etat extends Model
{
	public $timestamps = true;
    
	 protected $fillable = [
        'nom'
    ];
	
	
	//relation with Modification
	
	public function modifications(){
		
		 return $this->hasMany(Modification::class,'idEtat')->orderBy('date','desc');
		
	}
	
	
	
	
	
}
