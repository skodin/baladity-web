<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class SuperAdmin extends Model
{
    	
   public $timestamps = true;	
   
   
   public $primaryKey  = 'idUser';
   	
		
    protected $table = 'superadmins';	
   
     protected $fillable = [
        'idUser'
    ];	
	
	
	
	
	
	//relation with User
	
	public function user(){
		
		 return $this->belongsTo(User::class,'idUser');
		
	}
	
	
}
