<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Gouvernorat;

class Admin extends Model
{
	public $timestamps = true;
	
	public $primaryKey  = 'idUser';
	
    protected $fillable = [
        'idUser'
    ];
	
	
	
	
	//relation with User
	
	public function user(){
		
		 return $this->belongsTo(User::class,'idUser');
		
	}
	
	
	
	
	
	
	
	
	
}
