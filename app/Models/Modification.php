<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Etat;
use App\Models\Probleme;

class Modification extends Model
{
	public $timestamps = true;
	
     protected $fillable = [
        'date','idEtat','idReclamation','message'
    ];
	
	
	//relation with etat
	
	public function etat(){
		
		 return $this->belongsTo(Etat::class,'idEtat');
		
	}
	
	
	//relation with probleme
	
	public function probleme(){
		
		 return $this->belongsTo(Probleme::class,'idReclamation');
		
	}
	
	
	
}
