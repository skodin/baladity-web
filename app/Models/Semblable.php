<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Probleme;


class Semblable extends Model
{
	public $timestamps = true;
	
    protected $table="semblable";
	
	
	protected $fillable = [
	
        'message','date','idReclamation1', 'idReclamation2', 'idUser'
    ];
	
	
	//relation with user
	
	public function user(){
		
		 return $this->belongsTo(User::class,'idUser');
		
	}
	
	//relation with probeleme1
	
	public function probleme1(){
		
		 return $this->belongsTo(Probleme::class,'idReclamation1');
		
	}
	
	
	//relation with probeleme2
	
	public function probleme2(){
		
		 return $this->belongsTo(Probleme::class,'idReclamation2');
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
