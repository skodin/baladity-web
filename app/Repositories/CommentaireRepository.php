<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Commentaire;
use Carbon\Carbon;


class CommentaireRepository {
		
	public function showComments($id){
		try{
				
			$commentaires=Commentaire::where('idReclamation','=',$id)->with('user')->orderBy('updated_at','desc')->get();
			
			return response() -> json($commentaires);
			
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
		
	}
	
	public function store(Request $request) {
		try{
			

			$commentaire = Commentaire::create($request -> all());
			
			return response() -> json($commentaire);
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	
	public function show($id)
	{
		try{
			
		     $commentaire =Commentaire::with('user','problem')->where('estAbuse','=',$id)->get();
			 
		     return response()->json($commentaire);
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	
	
	public function update(Request $request, $id){
		try{
			
		   $commentaire = Commentaire::find($id);
		   
           $commentaire ->update($request->all());
		   
           return response()->json($commentaire);
		
		}catch(Exception $e){
			
			return response()->json(['error'=>'request invalid']);
		}
	}
	

	public function destroy($id){
		try{
			
		     $commentaire = Commentaire::find($id);
			 
		     $commentaire ->delete();
			 
		     return response()->json(['success'=>'Commentaire deleted']);
		 
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	






















}
