<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Semblable;
use App\Models\User;
use App\Models\Admin;
use App\Models\Gouvernorat;


class SemblableRepository {
		

	public function index() {
		
		$semblables=Semblable::with('user','probleme1','probleme1.photos','probleme1.modifications','probleme2','probleme2.photos','probleme2.modifications')->paginate(12);
		
		return response()->json($semblables);
	}
	
	
	
	public function getSemblances($id){
		
		$user=User::find($id);
       
       if($user['role'] === "superAdmin"){
       	
		 $semblables=Semblable::with('user','probleme1','probleme1.photos','probleme1.modifications','probleme2','probleme2.photos','probleme2.modifications')->paginate(12);	
		
       }elseif($user['role'] === "admin"){
       	
		 $admin=Admin::find($id);

         $gouvernorat=Gouvernorat::find($admin['idGouvernorat']);
		
		 $semblables=Semblable::with('user','probleme1','probleme1.photos','probleme1.modifications','probleme2','probleme2.photos','probleme2.modifications')->whereHas('probleme1.delegation', function ($query) use ($gouvernorat){
	         
	          $query->where('idGouvernorat', $gouvernorat['id']);
	        
	          })->whereHas('probleme2.delegation', function ($query) use ($gouvernorat){
	         
	          $query->where('idGouvernorat', $gouvernorat['id']);
	        
	     })->paginate(12);

       }
		
		return response()->json($semblables);
		
	}
	
	
	public function store(Request $request) {
		try{
			
			$semblable = Semblable::create($request->all());
			
			$semblable->date=date('Y-m-d_H:i:s');
			
			$semblable->save();
		
			return response() -> json($semblable);
			
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	
	
	
	
	public function show($id){
		
		$semblable=Semblable::with('user','probleme1','probleme2')->find($id);
		
        return response()->json($semblable);
	}
	

	
	public function destroy($id){
		try{
			
			$semblable = Semblable::find($id);
			
			$semblable->delete();
			
	        return response()->json(['success'=>'Semblance deleted']);
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	






















}
