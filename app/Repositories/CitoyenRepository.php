<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Citoyen;
use App\Repositories\UserRepository;


class CitoyenRepository {
		
	protected $userRepository;
		
	public function __construct(UserRepository $userRepository)
	{
	    $this->userRepository=$userRepository;	

	}
		

	public function index() {
		
		$citoyens=Citoyen::with('user','problemes','suivres')->get();
		
		return response()->json($citoyens);

	}
	

 	public function store(Request $request) {
		try{
			
			$user =$this->userRepository->registerCitoyen($request);
			
			$requestCitoyen=['idUser'=>$user->id,
			                 'telephone'=>$request->input('telephone'),
			                 'estBlocker'=>0,
			                   'estConfirmer'=>0
							   ];
			
			$citoyen = Citoyen::create($requestCitoyen);

			$user['telephone']=$citoyen->telephone;
		    $user['estBlocker']=$citoyen->estBlocker;
		    $user['estConfirmer']=$citoyen->estConfirmer;
		
			return response() -> json($user);
		
		}catch(Exception $e){
			
			return response() -> json(['result'=>'error']);
		}
	}
	 


	
	
	public function show($id){
		
		$citoyen = Citoyen::with('user','problemes','suivres')->find($id);
	
        return response()->json($citoyen);
		
	}


	public function updateCitoyen(Request $request){

        try{
			
			$user =$this->userRepository->update($request,$request->input('id'));
	
			$citoyen=Citoyen::find($user->id);				  
			
			$citoyen->update($request->all());

			$user['telephone']=$citoyen->telephone;
		    $user['estBlocker']=$citoyen->estBlocker;
		    $user['estConfirmer']=$citoyen->estConfirmer;
			$user['token']="";
		
			return response() -> json($user);
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}


	}

 
 	public function update(Request $request, $id){
			
		try{
				
			$user =$this->userRepository->update($request,$id);
			
			$citoyen = Citoyen::find($id);
			
			$citoyen ->update($request->all());

			$user['telephone']=$citoyen->telephone;
		    
		    $user['estBlocker']=$citoyen->estBlocker;
		    
		    $user['estConfirmer']=$citoyen->estConfirmer;
	        
	        return response()->json($user);
			
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	
	

	
	
	public function destroy($id){
		try{
						
			$user =$this->userRepository->destroy($id);
			
	        return response()->json(['success'=>'Citoyen deleted']);
			
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	






















}
