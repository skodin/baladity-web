<?php

namespace App\Repositories;
use Illuminate\Http\Request;
use App\Models\Modification;


class ModificationRepository {
	
	
  public function index()
	{
		$modifications=Modification::all();
		
		return response()->json($modifications);
		
	}



	public function store(array $request) {
        	
        try{
        	
		    $modification = Modification::create($request);
		
		    return response() -> json($modification);
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
		
	}



	public function show($id)
	{
		$modifications=Modification::where('idReclamation','=',$id)->with('etat')->get();
		
		return response()->json($modifications);
		
	}
	
	
    public function testeReclamation($id)
	{
		$modifications=Modification::where('idReclamation','=',$id)->with('etat')->get();
		
		return $modifications;
		
	}
   
	
	
	
	
	

}
