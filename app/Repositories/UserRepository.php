<?php

namespace App\Repositories;

use JWTAuth;
use Auth;
use Mail;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Citoyen;
use App\Http\Requests;
use DB;
class UserRepository {
	
/*---------------------------------send Mail----------------------------------------*/

	public function contact(Request $request) {
			
		try{
					Mail::send('email', ['msg' => $request->input('message')], function($message) use ($request) {
				
					$message -> to($request->input('email'));
				
					$message -> subject($request->input('sujet'));
				
				      });
				
		    	return response()->json(['sujet'=>$request->input('sujet'),
		    		                     'message'=>$request->input('message'),
		    		                     'email'=>$request->input('email')
		    		                     ]);
		}catch(Exception $e){
			
			response()->json(['result'=> 'error']);
		}
		
	}
		
	
	/*-----------------------------upload image user-----------------------------------------------*/
	
	public function uploadImage(Request $request,User $user)
	{
				
		$image = $request -> file('photo');
		
		if ($image -> isValid()) {
			
			$chemin = config('images.users');
			
			$extension = $image -> getClientOriginalExtension();
				
			$fileName = 'user_'.$user->id.'_'.date('Y_m_d_H_i_s').'.' . $extension;
							
			if ($image -> move($chemin, $fileName)) {
				
				$user -> photo = $chemin . '/' . $fileName;
				
				$user -> save();
			}
		}
			
			
	}


/*-----------------------------update image user-----------------------------------------------*/
	

	public function updateImageUser(Request $request)
	{
		try{

             $user=User::find($request->input('id'));

		if(is_file ($user->photo)){

			unlink($user->photo);	
		}
				
		$image = $request -> file('photo');
		
		if ($image -> isValid()) {
			
			$chemin = config('images.users');
			
			$extension = $image -> getClientOriginalExtension();
				
			$fileName = 'user_'.$user->id.'_'.date('Y_m_d_H_i_s').'.' . $extension;
							
			if ($image -> move($chemin, $fileName)) {
				
				$user -> photo = $chemin . '/' . $fileName;
				
				$user -> save();
			}
		}
			
	   return response()->json(['result'=> $user -> photo],200); 
			
       	}catch(Exception $e){

         return response()->json(['result'=> 'error'],401); 

		}
	}
	

/*---------------------------------register Agent or Admin----------------------------------------*/

	public function register(Request $request) {
		try{
			
			 $user = User::create($request -> all());
			 
			 $user->password=bcrypt($request -> input('password'));
			 
			 $user->save();
			 
			return $user;
		
		}catch(Exception $e){
			
			return 'error';
		}
		
	}
	
	
	
	/*---------------------------------register Citoyen----------------------------------------*/

	
	public function registerCitoyen(Request $request) {
		try{
			
			 $user = User::create($request -> all());
			 
			 $user->password=bcrypt($request -> input('password'));
			 
			 $user->role="citoyen";

			 $user->save();

			 if ($request -> hasFile('photo')) {
			 	
				$this->uploadImage($request,$user);
				 
			 }
			 			 
			 $token = JWTAuth::fromUser($user);
			 
			 $user['token']=$token;
			
			return $user;
		
		}catch(Exception $e){
			
			return 'error';
		}
		
	}
	
	
/*---------------------------------update user----------------------------------------*/
	
	public function update(Request $request, $id){
		try{
			
			$user = User::find($id);
			
			$user->update($request->except(['password','photo']));	
			
			if ($request->has('password')) {
					
				$user->password=bcrypt($request->input('password'));
			 
			    $user->save();
				    
			} 
	        
	        return $user;
			
		}catch(Exception $e){
			
			return 'error';
		}
	}
	
	

/*---------------------------------delete user----------------------------------------*/
	
	public function destroy($id){
		try{
			$user = User::find($id);
			
	        $user ->delete();
			
	        return "success" ;
			
		}catch(Exception $e){
			
			return "error";
		}
	}
	


	
	
/*---------------------------------login----------------------------------------*/
	
	
	 public function login(Request $request){
   	
		$credentials = $request->only('email','password');
       
        try {
         
            if (!$token=JWTAuth::attempt($credentials)) {
            	
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
          
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
		
		$user =Auth::user();

		if($user->role != "citoyen"){

			$user['token']=$token;

            return response()->json($user,200);
          
		}else{
			return response()->json(['error' => 'invalid_credentials'],401);
		}
	
   }
   
 /*-------------------------------login citoyen----------------------------------------*/  

  public function loginCitoyen(Request $request){
       
        $credentials = $request->only('email','password');
       
        try {
         
            if (!$token=JWTAuth::attempt($credentials)) {
            	
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
          
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
		
		$user =Auth::user();

		if($user->role == "citoyen"){
           $citoyen=Citoyen::find($user->id);
           $user['telephone']=$citoyen->telephone;
		   $user['estBlocker']=$citoyen->estBlocker;
		   $user['estConfirmer']=$citoyen->estConfirmer;
		   $user['token']=$token;
           return response()->json($user,200);
		}else{

			 return response()->json(['error' => 'invalid_credentials'],401);
		}

   }
   

/*---------------------------------logout----------------------------------------*/
   
   
   public function logout(){
            	
            $token = JWTAuth::getToken();
		   
			if ($token) {
			    JWTAuth::setToken($token)->invalidate();
				return response()->json(['result'=>'success'],200);
			}else {
				return response()->json(['result'=>'error'],401);
			}  
			
   }
	




  /*----------------------------reset password ----------------------------------*/

  public function reset(Request $request)
  {
      
      try{
				
		Mail::send('reset', ['token' => $request->input('token')], function($message) use ($request) {
		
			$message -> to($request->input('email'));
		
		   });
		   
		   DB::table('password_resets')->where('email',$request->input('email'))->delete();
	 
		  $password=DB::table('password_resets')->insert([
            'email' => $request->input('email'),
            'token' => $request->input('token')
           ]);
				
		  return response()->json(['result'=>'success']);
				
		}catch(Exception $e){
			
			response()->json(['result'=> 'error']);
		}
	  
  }





  /*-----------------------------------update Passord------------------------------------------*/

   public function updatePassord(Request $request)
   {

	 	   
	 $password = DB::table('password_resets')->where('email',$request->input('email'))->where('token',$request->input('token'))->first();
	
      if (! empty($password)){
      	
		 $user=User::where('email',$request->input('email'))->first();
	   
	     $user->password=bcrypt($request->input('password'));
	 
	     $user->save();
	   
	     return response()->json(['result'=>'success']);
		
      }else{
      	 return response()->json(['result'=>'error']);
      }


	    
   }








}
