<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\SuperAdmin;
use App\Repositories\UserRepository;


class SuperAdminRepository {
	
	protected $userRepository;
		
	public function __construct(UserRepository $userRepository)
	{
	    $this->userRepository=$userRepository;	
	}




	public function register(Request $request) {
				
			try{
				
				$user =$this->userRepository->register($request);
					
				$requestAdmin=['idUser'=>$user->id];
				
				$superAdmin = SuperAdmin::create($requestAdmin);
				
				if ($request -> hasFile('photo')) {
			 	
				$this->userRepository->uploadImage($request,$user);
				 
			 }
			
				return response() -> json($user);
				
			}catch(Exception $e){
				
				return response() -> json(['error'=>'request invalid']);
			}
			
		
	 }
		
		
	public function show($id)
	{
		$superAdmin=SuperAdmin::with('user')->find($id);
		
        return response()->json($superAdmin);
		
	}	


	public function update(Request $request, $id){
				
			try{
				
				$superAdmin =$this->userRepository->update($request,$id);
				   
		        return response()->json($superAdmin);
			
			}catch(Exception $e){
				
				return response() -> json(['error'=>'request invalid']);
			}
	}



    public function destroy($id)
    {
        try{
			$this->userRepository->destroy($id);
			
	        return response()->json(['success'=>'SuperAdminDeleted deleted']);
			
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
    }







}
