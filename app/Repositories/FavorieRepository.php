<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Suivre;


class FavorieRepository {
		

	public function index() {
		
		$favories=Suivre::with('citoyen','probleme')->get();
		
		return response()->json($favories);

	}
	
	
	public function store(Request $request) {
		try{
			
			$favorie = Suivre::create($request->all());
		
			return response() -> json($favorie);
			
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	
	
	
	
	public function show($id){
		
		$favories=Suivre::where('idCitoyen','=',$id)->with('probleme.citoyen.user','probleme.photos','probleme.modifications','probleme.categorieproleme')->get();
		
        return response()->json($favories);
		
	}



	
	
	
	

	
	
	public function destroy($id){
		try{
			
			$favorie = Suivre::find($id);
			
			$favorie->delete();
			
	        return response()->json($favorie);
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	




    public function deleteFavories($idReclamation,$idCitoyen)
    {	
		try{
			
			$favorie = Suivre::where('idReclamation','=',$idReclamation)->where('idCitoyen','=',$idCitoyen)->first();
			
			$favorie->delete();
			
	        return response()->json($favorie);
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
			
    }
     
















}
