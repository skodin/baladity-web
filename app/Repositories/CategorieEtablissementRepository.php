<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Categorieetablissement;



class CategorieEtablissementRepository {
		
		

	public function index() {
		
		$categorieEtablissements=Categorieetablissement::with('etablissements','categorieproblemes')->get();
		
		return response()->json($categorieEtablissements);
		
	 
		
	}
	
	
	public function store(Request $request) {
	   try{		
			$categorieEtablissement = CategorieEtablissement::create($request -> all());
		
			return response() -> json($categorieEtablissement);
			
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	
	
	
	
	public function show($id){
		
		$categorieEtablissement = CategorieEtablissement::with('etablissements','categorieProblemes')->find($id);
		
        return response()->json($categorieEtablissement);
		
	}
	
	
	
	public function update(Request $request, $id){
		try{
			
			$categorieEtablissement = CategorieEtablissement::find($id);
			
	        $categorieEtablissement ->update($request->all());
			
	        return response()->json($categorieEtablissement);
			
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	
	
	
	public function destroy($id){
		try{
			
			$categorieEtablissement = CategorieEtablissement::find($id);
			
	        $categorieEtablissement ->delete();
			
	        return response()->json(['success'=>'categorieEtablissement deleted']);
			
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	






















}
