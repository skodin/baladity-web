<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Affectation;
use App\Models\Probleme;
use App\Models\Categorieprobleme;
use App\Models\Etablissement;
use App\Models\Agent;


class AffectationRepository {
		

		
	public function index()
	{
		$affectations = Affectation::with('agent','probleme')->get();
	
		return response() -> json($affectations);
	}
		
		
	
	public function store(Request $request) {
		
		try{
		
			$probleme = Probleme::find($request->input('idReclamation'));
	        
	        $probleme ->idMunicipalite=$request->input('idMunicipalite');
			
			$probleme ->idCategorieProleme=$request->input('idCategorieProleme');
			
			$probleme->save();


			$categoryProbleme=Categorieprobleme::find($request->input('idCategorieProleme'));

			$etablissement=Etablissement::where('idCategorieEtablissement',$categoryProbleme['idCategorieEtablissement'])->where('idMunicipalite',$request->input('idMunicipalite'))->first();
            
            $agents=Agent::where('idEtablissement',$etablissement['id'])->get();

            if(! empty($agents)){
               
               for($i=0;$i<count($agents);$i++){
                  
	                  $requestAffectation=[
	                  'idAgent' => $agents[$i]['idUser'],
	                  'idReclamation'=>$request->input('idReclamation'),
	                  'message'=>$request->input('message')
	                ];

               	$affectation = Affectation::create($requestAffectation);
			
				$affectation->date=date("Y-m-d h:i:s");
				
				$affectation->save();

               }

            }
			
		
		
			return response() -> json(['result'=>'succes']);
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	
	
	
	
	public function show($id)
	{
		$affectation = Affectation::with('agent','probleme')->find($id);
		
		return response()->json($affectation);
	}
	
	
	
	
	public function update(Request $request, $id){
		try{
				
			$probleme = Probleme::find($request->input('idReclamation'));
	        
	        $probleme ->update($request->all('idDelagation','idCategorieProleme'));
	
			$affectation = Affectation::find($id);
	        $affectation ->update($request->all());
	        return response()->json($affectation);
			
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	
	
	
	
	
	public function destroy($id){
		try{
			$affectation = Affectation::find($id);
	        $affectation ->delete();
	        return response()->json(['success'=>'Affectation est supprimer']);
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	


}
