<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Agent;
use App\Models\User;
use App\Models\Admin;
use App\Models\Etablissement;
use App\Repositories\UserRepository;

class AgentRepository {
	
	
	protected $userRepository;
		
	public function __construct(UserRepository $userRepository)
	{
	    $this->userRepository=$userRepository;	
	}
		
	
		
	public function index() {
		
		$agents=Agent::with('user','etablissement','affectations')->orderBy('updated_at','desc')->paginate(12);
		
		return response()->json($agents);
	}
	
	public function getAgents($id){
		
		$user=User::find($id);
       
       if($user['role'] === "superAdmin"){
         
		  $agents=Agent::with('user','etablissement','affectations')->orderBy('updated_at','desc')->paginate(12);
		
	   }elseif($user['role'] === "admin"){
	   	
		  $admin=Admin::find($id);
		
		   $agents=Agent::with('user','etablissement','affectations')->orderBy('updated_at','desc')->whereHas('etablissement.delegation', function ($query) use ($admin){
	         
	          $query->where('idGouvernorat',$admin['idGouvernorat']);
	        
	        })->paginate(12);
		 
	   }
		
		return response()->json($agents);
	}
	
	
	public function store(Request $request) {
		try{
				
			$request->offsetSet('password',"azerty");
				
			$user =$this->userRepository->register($request);
			
			$requestAgent=['idUser'=>$user->id,'idEtablissement'=>$request->input('idEtablissement')];
			
			$agent = Agent::create($requestAgent);
			
			if ($request -> hasFile('photo')) {
			 	
				$this->userRepository->uploadImage($request,$user);
				 
			 }
		
			return response() -> json(['success'=> 'agent created']);
			
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	
	
	public function show($id){

	}
	
	public function getAgnt($id1,$id){

		

		$agent = Agent::with('user','etablissement','affectations')->find($id);

		if(! empty($agent)){

          $user=User::find($id1);   

	       if($user['role'] === "superAdmin"){

	       	$etablissements=Etablissement::all();

	       }elseif($user['role'] === "admin"){
		   	
			  $admin=Admin::find($id1);

	       	  $etablissements = Etablissement::with('delegation')->whereHas('delegation', function ($query) use ($admin){
		         
		          $query->where('idGouvernorat',$admin['idGouvernorat']);
		        
		        })->get();
	       }
         
			$agent['etabs']=$etablissements;

			return response()->json($agent,200);
		}else{
			return response()->json(['result'=>'data not found'],404);
		}
		
	}
	
	
	
	public function update(Request $request, $id){
		try{
			
			$user =$this->userRepository->update($request,$id);
			
			if($request->has('idEtablissement')){
				
				$agent = Agent::find($id);
			
			    $requestAgent=['idEtablissement'=>$request->input('idEtablissement')];
			
	            $agent ->update($requestAgent);
			}

	        return response()->json(['result'=>'agent updated ']);
		
		}catch(Exception $e){
			
			return response() -> json(['result'=>'request invalid']);
		}
		
	}
	
	
	
	public function destroy($id){
			
			$result =$this->userRepository->destroy($id);
			
	        return response()->json(['result'=>$result]);
		
	}
	






















}
