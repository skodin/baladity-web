<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Gouvernorat;

class GouvernoratRepository {
		
		

	public function index() {
		
		$gouvernorats=Gouvernorat::with('admins','delegations')->get();
		
		return response()->json($gouvernorats);

	}
	
	
	public function store(Request $request) {
		try{

		    $gouvernorat = Gouvernorat::create($request -> all());
	
		    return response() -> json($gouvernorat);
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	
	
	
	
	public function show($id){
		
		$gouvernorat = Gouvernorat::with('admins','delegations')->find($id);
		
        return response()->json($gouvernorat);
		
	}
	
	
	
	public function update(Request $request, $id){
		try{
		     $gouvernorat = Gouvernorat::find($id);
		 
             $gouvernorat ->update($request->all());
		
             return response()->json($gouvernorat);
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
		
	}
	
	
	
	public function destroy($id){
		try{
			
		   $gouvernorat = Gouvernorat::find($id);
		   
           $gouvernorat ->delete();
		   
          return response()->json(['success'=>'Gouvernorat deleted']);
		  
		  }catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	






















}
