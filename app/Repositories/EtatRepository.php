<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Etat;



class EtatRepository {
		
		

	public function index() {
		
		$etats=Etat::with('modifications')->where('id','!=',4)->get();
		
		return response()->json($etats);
	}
	
	
	public function store(Request $request) {
		try{
		
		    $etat = Etat::create($request -> all());
	
		    return response() -> json($etat);
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	

	public function getEtats(){

		$etats=Etat::with('modifications')->where('id','!=',4)->get();
		
		return response()->json($etats);

	}
	
	
	
	public function show($id){
			
		$etat = Etat::with('modifications')->find($id);
        
        return response()->json($etat);
		
	}
	
	
	
	
	
	public function update(Request $request, $id){
		try{	
		 	
		     $etat = Etat::find($id);
			 
             $etat ->update($request->all());
			 
             return response()->json($etat);
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
		
	}
	
	
	
	public function destroy($id){
		try{
		    $etat = Etat::find($id);
			
            $etat ->delete();
			
            return response()->json(['success'=>'Etat deleted']);
			
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	






















}
