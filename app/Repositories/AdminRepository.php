<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Gouvernorat;
use App\Repositories\UserRepository;

class AdminRepository {
		
		
	protected $userRepository;
		
	public function __construct(UserRepository $userRepository)
	{
	    $this->userRepository=$userRepository;	
	}
		
		

	public function index() {
		
		$admins=Admin::with('gouvernorat','user')->orderBy('updated_at','desc')->paginate(12);
		
		return response()->json($admins);

	}
	
	
	public function store(Request $request) {
		try{
			
			$request->offsetSet('password',"azerty");
			
			$user =$this->userRepository->register($request);
			
			$requestAdmin=['idUser'=>$user->id,'idGouvernorat'=>$request->input('idGouvernorat')];
			
			$admin = Admin::create($requestAdmin);
			
			if ($request -> hasFile('photo')) {
			 	
				$this->userRepository->uploadImage($request,$user);
				 
			 }
		
			return response() -> json($admin);
			
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
		
	
	}
	
	
	
	
	public function show($id){
		
		$admin = Admin::with('gouvernorat','user')->find($id);

        $admin['gouvernorats']=Gouvernorat::all(); 

		if(! empty($admin)){

			return response()->json($admin,200);

		}else{
			
			return response()->json(['result'=>'data not found'],404);
		}
		
	}

	
	
	public function update(Request $request, $id){
			
		try{
			
			$user =$this->userRepository->update($request,$id);
			
			if($request->has('idGouvernorat')){
					
				$admin=Admin::find($id);
			
				$admin->update(['idGouvernorat'=>$request->input('idGouvernorat')]);
				
				return response()->json($admin);	
				
			}else {
				return response()->json($user);
			}
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	
	
	
	public function destroy($id){
		try{
			
			$this->userRepository->destroy($id);
			
	        return response()->json(['success'=>'admin deleted']);
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	






















}
