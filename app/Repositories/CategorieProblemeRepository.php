<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Categorieprobleme;

class CategorieProblemeRepository {
		
		

	public function index() {
		
		$categorieProblemes=Categorieprobleme::with('problemes')->get();
		
		return response()->json($categorieProblemes);
		
		
	}
	
	
	public function store(Request $request) {
		try{
			
			$categorieProbleme = CategorieProbleme::create($request -> all());
		
			return response() -> json($categorieProbleme);
			
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	
	
	
	
	public function show($id){
		$categorieProbleme = CategorieProbleme::with('problemes')->find($id);
		
        return response()->json($categorieProbleme);
		
	}
	
	
	
	public function update(Request $request, $id){
		try{
			
			$categorieProbleme = CategorieProbleme::find($id);
			
	        $categorieProbleme ->update($request->all());
			
	        return response()->json($categorieProbleme);
			
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	
	
	
	public function destroy($id){
		try{
			$categorieProbleme = CategorieProbleme::find($id);
			
	        $categorieProbleme ->delete();
			
	        return response()->json(['success'=>'CategorieProbleme deleted']);
			
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	






















}
