<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Delegation;
use App\Models\User;
use App\Models\Admin;


class DelegationRepository {
		
		

	public function index() {
		
		$delegations=Delegation::with('gouvernorat','problemes','etablissements')->orderBy('updated_at','desc')->get();
		
		return response()->json($delegations);
	}
	
	
	public function getMunicipalites($id)
	{
		$user=User::find($id);
       
       if($user['role'] === "superAdmin"){
          
		  $delegations=Delegation::with('gouvernorat','problemes','etablissements')->orderBy('updated_at','desc')->get();
		
        
	   }else{
	   	
		   $admin=Admin::find($id);
		
		   $delegations=Delegation::with('gouvernorat','problemes','etablissements')->whereHas('gouvernorat', function ($query) use ($admin){
	         
	          $query->where('idGouvernorat',$admin['idGouvernorat']);
	        
	        })->orderBy('updated_at','desc')->get();
	   }
		
		return response()->json($delegations);
		
	}
	
	
	
	public function store(Request $request) {
			
		try{
		
		    $delegation = Delegation::create($request -> all());
	
		    return response() -> json($delegation);
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	
	
	
	
	public function show($id){
		
		   $delegation = Delegation::with('gouvernorat','problemes','etablissements')->find($id);
		   
           return response()->json($delegation);
	}
	
	
	
	public function update(Request $request, $id){
		try{
				
		     $delegation = Delegation::find($id);
			 
             $delegation ->update($request->all());
			 
             return response()->json($delegation);
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	
	
	
	public function destroy($id){
		try{
				
		    $delegation = Delegation::find($id);
			
            $delegation ->delete();
			
            return response()->json(['success'=>'Delegation deleted']);
			
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	






















}
