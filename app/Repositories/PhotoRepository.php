<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Photo;



class PhotoRepository {
		
   public function addPhoto(Request $request)
	{
		 try{

		 if($request->hasFile('chemin')){

				   $image = $request -> file('chemin');

						if ($image -> isValid()) {

							$chemin = config('images.problemes');

						$extension = $image -> getClientOriginalExtension();

		                $fileName = 'claim_'.$request->input('idReclamation').'_'.date('Y_m_d_H_i_s').'.' . $extension;

							if ($image -> move($chemin, $fileName)) {

								$photo=new Photo;

								$photo->chemin = $chemin . '/' . $fileName;

								$photo->idReclamation = $request->input('idReclamation');

								$photo-> save();

							}
						}

				}
		  return response()->json($photo);

		}catch(Exception $e){

			return response() -> json(['error'=>'request invalid']);
		}
		

		return response() -> json(['error'=>'request invalid']);
	}	
	
	
	public function update(Request $request){
		try{	

			$id=$request->input('id');
		 	
		     $photo = Photo::find($id);
			 
             if($request->hasFile('chemin')){
				
				   $image = $request -> file('chemin');
				
						if ($image -> isValid()) {
							
							$chemin = config('images.problemes');
							
						$extension = $image -> getClientOriginalExtension();
							
		                $fileName = 'claim_'.$photo->idReclamation.'_'.$id.'_'.date('Y_m_d_H_i_s').'.' . $extension;

		                    unlink($photo->chemin);
							
							if ($image -> move($chemin, $fileName)) {
								
								
								$photo->chemin = $chemin . '/' . $fileName;
	
								$photo-> save();
								
							}
						}	
					
				}

			 
             return response()->json($photo);
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
		
	}



	
	
	
	
	





















}
