<?php

namespace App\Repositories;

use App\Models\Probleme;
use App\Models\Photo;
use App\Models\User;
use App\Models\Suivre;
use App\Models\Admin;


use App\Repositories\ModificationRepository;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;

class ProblemeRepository
{


    protected $modificationRepository;

    public function __construct(ModificationRepository $modificationRepository)
    {
        $this->modificationRepository = $modificationRepository;
    }


    public function index()
    {

    }


    public function getClaims($id)
    {

        $user = User::find($id);

        $problemes = Probleme::with('commentaires', 'citoyen.user', 'modifications', 'categorieproleme', 'photos')->orderBy('updated_at', 'desc')->paginate(12);

        return response()->json($problemes);
    }


    public function filterByEtat($idEtat, $idCitoyen)
    {

        $problemes = Probleme::where('flag', '=', '1')->with('commentaires', 'citoyen.user', 'modifications', 'categorieproleme', 'photos')->whereHas('modifications.etat', function ($query) use ($idEtat) {

            $query->where('id', '=', $idEtat);

        })->orderBy('updated_at', 'desc')->paginate(10);

        /*  $suivres = Suivre::where('idCitoyen', '=', $idCitoyen)->get();

          for ($i = 0; $i < count($problemes); $i++) {

              $problemes[$i]['idMunicipalite'] = 0;

              for ($j = 0; $j < count($suivres); $j++) {

                  if ($problemes[$i]['id'] == $suivres[$j]['idReclamation']) {

                      $problemes[$i]['idMunicipalite'] = 1;
                  }
              }
          }
        */

        return response()->json($problemes);
    }

    
        public function filterByCategoryAndMunicipality($idEtat, $idCategory, $idCitoyen)
        {

            if ($idEtat == 0) {
                if ($idCategory != 0) {

                    $problemes = Probleme::where('idCategorieProleme', $idCategory)->where('flag', '=', '1')->with('commentaires', 'citoyen.user', 'modifications', 'categorieproleme', 'photos')->orderBy('updated_at', 'desc')->paginate(10);

                }  else {

                    $problemes = Probleme::where('flag', '=', '1')->with('commentaires', 'citoyen.user', 'modifications', 'categorieproleme', 'photos')->orderBy('updated_at', 'desc')->paginate(10);
                }
            } else {

                if ($idCategory != 0 ) {

                    $problemes = Probleme::where('idCategorieProleme', $idCategory)->where('flag', '=', '1')->with('commentaires', 'citoyen.user', 'modifications', 'categorieproleme', 'photos')->whereHas('modifications.etat', function ($query) use ($idEtat) {

                        $query->where('id', '=', $idEtat);

                    })->orderBy('updated_at', 'desc')->paginate(10);

                }  else {

                    $problemes = Probleme::where('flag', '=', '1')->with('commentaires', 'citoyen.user', 'modifications', 'categorieproleme', 'photos')->whereHas('modifications.etat', function ($query) use ($idEtat) {

                        $query->where('id', '=', $idEtat);

                    })->orderBy('updated_at', 'desc')->paginate(10);

                }
            }


            $suivres = Suivre::where('idCitoyen', '=', $idCitoyen)->get();


            for ($i = 0; $i < count($problemes); $i++) {

                $problemes[$i]['idMunicipalite'] = 0;

                for ($j = 0; $j < count($suivres); $j++) {

                    if ($problemes[$i]['id'] == $suivres[$j]['idReclamation']) {

                        $problemes[$i]['idMunicipalite'] = 1;
                    }
                }

            }

            return response()->json($problemes);
        }
    

    public function showByDescription($description, $idCitoyen)
    {

        $problemes = Probleme::where('flag', '=', '1')->where('description', 'LIKE', "%$description%")->with('commentaires', 'citoyen.user', 'modifications', 'categorieproleme', 'photos')->orderBy('updated_at', 'desc')->get();


         $suivres = Suivre::where('idCitoyen', '=', $idCitoyen)->get();

         for ($i = 0; $i < count($problemes); $i++) {

             $problemes[$i]['idMunicipalite'] = 0;

             for ($j = 0; $j < count($suivres); $j++) {

                 if ($problemes[$i]['id'] == $suivres[$j]['idReclamation']) {

                     $problemes[$i]['idMunicipalite'] = 1;
                 }
             }
         }



        return response()->json($problemes);
    }


    /*----------------------------------demandes des suppression------------------------------------*/

     public function getSuppressionClaim($id)
     {

         $user = User::find($id);

         if ($user['role'] === "superAdmin") {

             $problemes = Probleme::with('commentaires', 'citoyen.user', 'modifications.etat', 'categorieproleme','photos')->whereHas('modifications.etat', function ($query) {

                 $query->where('id', 4);

             })->orderBy('updated_at', 'desc')->paginate(12);

         } elseif ($user['role'] === "admin") {

               $admin = Admin::find($id);

             $gouvernorat = Gouvernorat::find($admin['idGouvernorat']);

             $problemes = Probleme::with('commentaires', 'citoyen.user', 'affectations', 'modifications.etat', 'categorieproleme', 'delegation', 'photos')->whereHas('modifications.etat', function ($query) {

                 $query->where('id', 4);

             })->whereHas('delegation', function ($query) use ($gouvernorat) {

                 $query->where('idGouvernorat', $gouvernorat['id']);

             })->orderBy('updated_at', 'desc')->paginate(12);

         }

         return response()->json($problemes);
     }


    /*----------------------------------Listes des abuses ------------------------------------*/

        public function getAbuses($id)
        {

            $user = User::find($id);


                $problemes = Probleme::with('commentaires', 'citoyen.user', 'modifications.etat', 'categorieproleme','photos')->whereHas('commentaires', function ($query) {

                    $query->where('estAbuse', '=', '1');

                })->orderBy('updated_at', 'desc')->paginate(12);


            return response()->json($problemes);

        }


    /*-------------------------------upload image for claim-----------------------------------------*/

    public function uploadImage(Request $request, $id)
    {
        for ($i = 1; $i < 6; $i++) {

            if ($request->hasFile('photo' . $i)) {

                $image = $request->file('photo' . $i);

                if ($image->isValid()) {

                    $chemin = config('images.problemes');

                    $extension = $image->getClientOriginalExtension();

                    $fileName = 'claim_' . $id . '_' . $i . '_' . date('Y_m_d_H_i_s') . '.' . $extension;

                    if ($image->move($chemin, $fileName)) {

                        $photo = new Photo;

                        $photo->chemin = $chemin . '/' . $fileName;

                        $photo->idReclamation = $id;

                        $photo->save();

                    }
                }

            }

        }

    }


    /*------------------------------add claim-----------------------------------*/

    public function store(Request $request)
    {
        try {

            $probleme = Probleme::create($request->all());

            $requestModification = [
                'idReclamation' => $probleme->id
                , 'idEtat' => 1
                , 'date' => date("Y-m-d h:i:s")
                , 'message' => 'réclamation en attente'
            ];

            $this->modificationRepository->store($requestModification);

            $this->uploadImage($request, $probleme->id);

            return response()->json($probleme);

        } catch (Exception $e) {

            return response()->json(['error' => 'request invalid']);
        }

    }


    public function show($id)
    {

        $probleme = Probleme::with('commentaires', 'citoyen.user', 'modifications.etat', 'categorieproleme', 'photos')->find($id);

        if (!empty($probleme)) {

            return response()->json($probleme, 200);

        } else {

            return response()->json(['result' => 'data not found'], 404);

        }
    }


    public function getDetailOfClaim($id)
    {

        // $idMuniciplaite = Probleme::find($id)['idMunicipalite'];

        $probleme = Probleme::with('commentaires', 'citoyen.user', 'modifications.etat', 'categorieproleme', 'photos')->find($id);

        // $etablissement = Etablissement::where('idMunicipalite', $idMuniciplaite)->first();

        // $probleme['etablissement'] = $etablissement['nom'];

        if (!empty($probleme)) {

            return response()->json($probleme, 200);

        } else {

            return response()->json(['result' => 'data not found'], 404);

        }
    }


    /*----------------------------------- Réclamation de citoyen --------------------------------------------------*/

    public function citoyenReclamation($id)
    {

        $problemes = Probleme::where('idCitoyen', '=', $id)->with('commentaires', 'citoyen.user', 'modifications', 'categorieproleme', 'photos')->orderBy('updated_at', 'desc')->paginate(10);

        return response()->json($problemes);

    }


    public function update(Request $request, $id)
    {

        if (sizeof($this->modificationRepository->testeReclamation($id)) > 1) {

            return response()->json(['result' => 'impossible']);

        } else {

            try {

                $probleme = Probleme::find($id);

                $probleme->update($request->all());

                return response()->json(['result'=> 'success']);

            } catch (Exception $e) {

                return response()->json(['result' => 'error']);
            }
        }


    }


    public function destroy($id)
    {

        if (sizeof($this->modificationRepository->testeReclamation($id)) > 1) {

            return response()->json(['result' => 'impossible']);

        } else {

            try {

                $probleme = Probleme::find($id);

                $probleme->delete();

                return response()->json(['result' => 'success']);

            } catch (Exception $e) {

                return response()->json(['result' => 'error']);
            }
        }
    }

    /*-------------------------------- delete Claim ---------------------------------------*/


    public function deleteClaim($id)
    {

        $probleme = Probleme::find($id);

        $probleme->delete();

        return response()->json(['result' => 'success']);
    }


    /*-------------------------------- get id Claims ---------------------------------------*/

      public function getidClaims($id)
      {

          $probleme = Probleme::where('id', '!=', $id)->get();

          return response()->json($probleme);
      }



}
