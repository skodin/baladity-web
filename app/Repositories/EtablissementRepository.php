<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Etablissement;
use App\Models\User;
use App\Models\Admin;
use App\Models\Gouvernorat;
use App\Models\Delegation;
use App\Models\Categorieetablissement;


class EtablissementRepository {
		
		

	public function index() {
		
		$etablissements=Etablissement::with('agents','delegation','categorieEtablissement')->get();
		
		return response()->json($etablissements);
	}
	
	
	
	public function getEstablishement($id) {
		
		$user=User::find($id);
       
       if($user['role'] === "superAdmin"){
          	
          $etablissements=Etablissement::with('agents','delegation','categorieEtablissement')->orderBy('updated_at','desc')->paginate(12);		
		
	   }elseif($user['role'] === "admin"){
	   	  	
	   	  $admin=Admin::find($id);

          $gouvernorat=Gouvernorat::find($admin['idGouvernorat']);
		  
		  $etablissements=Etablissement::with('agents','delegation','categorieEtablissement')->whereHas('delegation', function ($query) use ($gouvernorat){
	         
	          $query->where('idGouvernorat', $gouvernorat['id']);
	        
	        })->orderBy('updated_at','desc')->paginate(12);		  
	   }
			
	
	   return response()->json($etablissements);
	}
	
	
	
	public function store(Request $request) {
		try{
			
		     $etablissement = Etablissement::create($request -> all());
	
		     return response() -> json($etablissement);
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	
	
	
	
	public function show($id){
		
		$etablissement = Etablissement::with('agents','delegation','categorieEtablissement')->find($id);

		$municipalites=Delegation::all();

		$categorieetablissement=Categorieetablissement::all();

		$etablissement['municipalites']=$municipalites;
		
		$etablissement['categorieetablissement']=$categorieetablissement;

		
		if(! empty($etablissement)){
			return response()->json($etablissement,200);
		}else{
			return response()->json(['result'=>'data not found'],404);
		}
		 	
	}

	public function getEtabs($id1,$id2){

		$user=User::find($id1);
       
       if($user['role'] === "superAdmin"){

            $municipalites=Delegation::all();
      
        }else{

         $admin=Admin::find($id1);
          
		  $municipalites=Delegation::where('idGouvernorat',$admin['idGouvernorat'])->get();
		 
        }
		
		$etablissement = Etablissement::with('agents','delegation','categorieEtablissement')->find($id2);

		$categorieetablissement=Categorieetablissement::all();

		$etablissement['municipalites']=$municipalites;
		
		$etablissement['categorieetablissement']=$categorieetablissement;

		
		if(! empty($etablissement)){
			return response()->json($etablissement,200);
		}else{
			return response()->json(['result'=>'data not found'],404);
		}
		 	
	}


	
	
	
	
	public function update(Request $request, $id){
		try{
			
		    $etablissement = Etablissement::find($id);
			
            $etablissement ->update($request->all());
			
            return response()->json($etablissement);
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
	}
	
	
	
	public function destroy($id){
		try{
				
		   $etablissement = Etablissement::find($id);
		   
           $etablissement ->delete();
		   
           return response()->json(['success'=>'Etablissement deleted']);
		
		}catch(Exception $e){
			
			return response() -> json(['error'=>'request invalid']);
		}
		
	}
	






















}
