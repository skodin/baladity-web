<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\FavorieRepository;

class FavorieController extends Controller
{
    
	protected $favorieRepository;
	
	public function __construct(FavorieRepository $favorieRepository)
	{
		$this->favorieRepository=$favorieRepository;
	}
	
	
	public function index()
	{
		return $this->favorieRepository->index();
	}
	
	public function store(Request $request)
	{
		return $this->favorieRepository->store($request);
	}
	
	
	public function show($id)
	{
		return $this->favorieRepository->show($id);
		
	}
	
	
	public function citoyenFavorie($id)
	{
		return $this->favorieRepository->citoyenFavorie($id);
		
	}
	
	
	
	
	public function destroy($id)
	{
		return $this->favorieRepository->destroy($id);
	}
	
	
	public function deleteFavories($idReclamation,$idCitoyen)
	{
		return $this->favorieRepository->deleteFavories($idReclamation,$idCitoyen);
	}
	
	
	
	
}
