<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\PhotoRepository;

class PhotoController extends Controller
{
    
    protected $photoRepository;
	
	public function __construct(PhotoRepository $photoRepository)
	{
	
	  $this->photoRepository =$photoRepository;	
	}
	


    public function store(Request $request)
    {
       return $this->photoRepository->update($request);


    }

	public function addPhoto(Request $request)
	{
		 return $this->photoRepository->addPhoto($request);
		
	}

}
