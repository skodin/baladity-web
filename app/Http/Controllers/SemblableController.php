<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\SemblableRepository;

class SemblableController extends Controller
{
    protected $semblableRepository;
	
	public function __construct(SemblableRepository $semblableRepository)
	{
		$this->semblableRepository=$semblableRepository;
	}
	
	
	
	
	public function index()
	{
		return $this->semblableRepository->index();
	}
	
	
	
	public function getSemblances($id){
		
		return $this->semblableRepository->getSemblances($id);
	}
	
	
	public function store(Request $request)
	{
		return $this->semblableRepository->store($request);
	}
	
	
	
	
	public function show($id)
	{
		return $this->semblableRepository->show($id);
	}
	
	
	
	
	public function destroy($id)
	{
		return $this->semblableRepository->destroy($id);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
