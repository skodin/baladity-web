<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\EtablissementRepository;

class EtablissementController extends Controller
{
	
	protected $etablissementRepository;
	
	public function __construct(EtablissementRepository $etablissementRepository)
	{
		$this->etablissementRepository=$etablissementRepository;
	}

    public function getEtabs($id1,$id2){

        return $this->etablissementRepository->getEtabs($id1,$id2);
    }

	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->etablissementRepository->index();
    }
	
	public function getEstablishement($id)
	{
	   return $this->etablissementRepository->getEstablishement($id);	
	}


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->etablissementRepository->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       return $this->etablissementRepository->show($id);
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->etablissementRepository->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->etablissementRepository->destroy($id);
    }
}
