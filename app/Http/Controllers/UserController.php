<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;




class UserController extends Controller
{
	
	protected $userRepository;
	
	public function __construct(UserRepository $userRepository)
	{
		$this->userRepository=$userRepository;
	}
	

	
   
   
    public function logout()
    {
    	return $this->userRepository->logout();
        
    }

    
    public function login(Request $request)
    {
        return $this->userRepository->login($request);
    }



    public function loginCitoyen(Request $request)
    {
        return $this->userRepository->loginCitoyen($request);
    }


    public function contact(Request $request)
    {
        return $this->userRepository->contact($request);
    }
	
	public function updateImageUser(Request $request)
	{
		return $this->userRepository->updateImageUser($request);
	}
	
	
	public function reset(Request $request)
	{
		
		return $this->userRepository->reset($request);
	}
	
	
	public function updatePassord(Request $request)
	{
		return $this->userRepository->updatePassord($request);
	}
   
}
