<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ProblemeRepository;

class ProblemeController extends Controller
{
	
	protected $problemeRepository;
	
	public function __construct(ProblemeRepository $problemeRepository){
			
		$this->problemeRepository=$problemeRepository;
		
	}
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->problemeRepository->index();
    }
	
	


    public function getClaims($id){

        return $this->problemeRepository->getClaims($id);

    }
   


    public function filterByCategoryAndMunicipality($idEtat,$idCategory,$idCitoyen){

        return $this->problemeRepository->filterByCategoryAndMunicipality($idEtat,$idCategory,$idCitoyen);
    }


    public function showByDescription($description,$idCitoyen){
        
        return $this->problemeRepository->showByDescription($description,$idCitoyen);
    }


   public function getSuppressionClaim($id)
    {
        return $this->problemeRepository->getSuppressionClaim($id);
    } 
	
	
	public function getidClaims($id)
	{
		  return $this->problemeRepository->getidClaims($id);
	}
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->problemeRepository->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->problemeRepository->show($id);
    }
	
	
	public function getDetailOfClaim($id)
	{
		 return $this->problemeRepository->getDetailOfClaim($id);
	}

    
    public function citoyenReclamation($id)
    {
        return $this->problemeRepository->citoyenReclamation($id);
    }

   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->problemeRepository->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->problemeRepository->destroy($id);
    }
	
    public function deleteClaim($id)
	{
		return $this->problemeRepository->deleteClaim($id);
	}
	
	public function getAbuses($id)
	{
		return $this->problemeRepository->getAbuses($id);
	}
	
	
}
