<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\AgentRepository;

class AgentController extends Controller
{
	protected $agentRepository;
	
	public function __construct(AgentRepository $agentRepository)
	{
		$this->agentRepository=$agentRepository;
	}
	
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->agentRepository->index();
    }
	
	public function getAgents($id)
	{
	 
	    return $this->agentRepository->getAgents($id);
	  	
	}
	
    public function getAgnt($id1,$id){

        return $this->agentRepository->getAgnt($id1,$id);
    }
	
	public function getAgent($id)
	{
		return $this->agentRepository->show($id);
	}

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->agentRepository->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->agentRepository->show($id);
    }

   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       return $this->agentRepository->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->agentRepository->destroy($id);
    }
}
