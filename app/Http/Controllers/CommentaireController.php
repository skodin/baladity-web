<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\CommentaireRepository;

class CommentaireController extends Controller
{
	protected $commentaireRepository;
	
	public function __construct(CommentaireRepository $commentaireRepository)
	{
	
	  $this->commentaireRepository =$commentaireRepository;	
	}
	
	
	public function showComments($id)
	{
		return $this->commentaireRepository->showComments($id);
	}
	
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->commentaireRepository->store($request);
    }


     public function show($id)
     {
         return $this->commentaireRepository->show($id);
     }
    
   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->commentaireRepository->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       return $this->commentaireRepository->destroy($id);
    }
}
