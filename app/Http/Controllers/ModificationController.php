<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ModificationRepository;

class ModificationController extends Controller
{
	
	protected $modificationRepository;
	
	public function __construct(ModificationRepository $modificationRepository)
	{
		$this->modificationRepository=$modificationRepository;
	}
	
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->modificationRepository->index();
    }

   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
           $requestModification=[ 'idReclamation'=>$request->input('idReclamation')
                                    ,'idEtat'=>$request->input('idEtat')
                                    ,'date'=>$request->input('date')
                                    ,'message'=>$request->input('message')
                                  ];

       
        return $this->modificationRepository->store($requestModification);
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->modificationRepository->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	public function demandeSupp(Request $request)
	{
		
		$requestModification=[ 'idReclamation'=>49
					                        ,'idEtat'=>1
					                        ,'date'=>date("Y-m-d h:i:s")
											,'message'=>'réclamation en attente'
					                      ];
				
			$this->modificationRepository->store($requestModification);

		
	}
}
