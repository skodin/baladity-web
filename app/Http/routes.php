<?php

/*------------------------------routes with token access-----------------------------------------*/


Route::group(['middleware' => ['web','cors','jwt.auth']], function () {


	/*--------------------------------Routes only  for SuperAdmin-----------------------------------*/

	Route::group(['middleware'=>'permission:superAdmin'],function(){

		Route::resource('admins','AdminController',['except' => ['create', 'edit']]);

		Route::resource('superAdmin','SuperAdminController',['only' => ['update', 'show']]);

	});


	/*--------------------------------Routes only  for Admin,SuperAdmuin-----------------------------------*/

	Route::group(['middleware'=>'permission:admin,superAdmin'],function(){

		//Route::resource('agents','AgentController',['except' => ['create', 'edit']]);
		
		//Route::get('getAgents/{id}','AgentController@getAgents');

		Route::get('getSuppressionClaim/{id}','ProblemeController@getSuppressionClaim');

		//Route::get('getAgnt/{id1}/{id}','AgentController@getAgnt');

		Route::get('getSemblances/{id}','SemblableController@getSemblances');

		Route::get('getAbuses/{id}','ProblemeController@getAbuses');
		
		//Route::get('getEstablishement/{id}','EtablissementController@getEstablishement');
		
		Route::get('getAdmin/{id}','AdminController@getAdmin');
	
		Route::put('admin/{id}','AdminController@update');
		
		//Route::get('getMunicipalites/{id}','DelegationController@getMunicipalites');

		Route::get('getClaims/{id}','ProblemeController@getClaims');

		Route::resource('affectations','AffectationController',['except' => ['create', 'edit']]);

	//	Route::get('getAgent/{id}','AgentController@getAgent');

	//	Route::put('agent/{id}','AgentController@update');

		Route::get('getDetailOfClaim/{id}','ProblemeController@getDetailOfClaim');

		Route::resource('semblances','SemblableController',['except' => ['create', 'edit']]);


	});




	/*----------------------------------------Routes only  for Citoyen---------------------------------------------*/

	Route::group(['middleware'=>'permission:citoyen'],function(){

		Route::delete('deleteFavories/{idReclamation}/{idCitoyen}','FavorieController@deleteFavories');

		Route::get('citoyenReclamation/{id}','ProblemeController@citoyenReclamation');

		Route::get('showByDescription/{description}/{idCitoyen}','ProblemeController@showByDescription');

		Route::get('filterByCategoryAndMunicipality/{idEtat}/{idCategory}/{idCitoyen}','ProblemeController@filterByCategoryAndMunicipality');

		Route::resource('favories','FavorieController',['except' => ['create', 'edit','update']]);

		Route::get('citoyenFavorie/{id}','FavorieController@citoyenFavorie');

	    Route::resource('citoyens','CitoyenController',['except' => ['create', 'edit']]);

	    Route::post('updateCitoyen','CitoyenController@updateCitoyen');	



	});


	/*----------------------------------------Routes only  for Citoyen,Admin,SuperAdmuin---------------------------------------------*/

	Route::group(['middleware'=>'permission:citoyen,admin,superAdmin'],function(){

		Route::delete('deleteClaim/{id}','ProblemeController@deleteClaim');

		Route::get('logout','UserController@logout');

		//Route::resource('gouvernorats','GouvernoratController',['except' => ['create', 'edit']]);

		Route::resource('etats','EtatController',['except' => ['create', 'edit']]);

		//Route::resource('categoriesEtablissements','CategorieEtablissementController',['except' => ['create', 'edit']]);

		Route::resource('categorieProblemes','CategorieProblemeController',['except' => ['create', 'edit']]);

		//Route::resource('municipalites','DelegationController',['except' => ['create', 'edit']]);

		//Route::resource('etablissements','EtablissementController',['except' => ['create', 'edit']]);

		//Route::get('getEtabs/{id1}/{id2}','EtablissementController@getEtabs');

		Route::get('getidClaims/{id}','ProblemeController@getidClaims');

		Route::resource('reclamations','ProblemeController',['except' => ['create', 'edit']]);

		Route::resource('commentaires','CommentaireController',['except' => ['create', 'edit']]);

		Route::get('showComments/{id}','CommentaireController@showComments');

		Route::post('contacts','UserController@contact');

		Route::post('updateImageUser','UserController@updateImageUser');

		Route::resource('photos','PhotoController',['except' => ['create', 'edit']]);

		Route::post('addPhoto','PhotoController@addPhoto');

		Route::get('getEtats','EtatController@getEtats');

		Route::resource('modifications','ModificationController',['except' => ['create', 'edit']]);


	});
	
});




/*------------------------------routes without token access-----------------------------------------*/

Route::group(['middleware' => ['web','cors']], function () {

	
		/*--------------------------------Routes   only admin,agent,superAdmin -----------------------------------*/
            Route::post('login','UserController@login');
			
			Route::post('sendReset','UserController@reset');
			
			Route::post('updatePassword','UserController@updatePassord');


	   /*--------------------------------Routes  only for  citoyen -----------------------------------*/

		Route::group(['middleware'=>'permission:citoyen'],function(){

			Route::post('loginCitoyen','UserController@loginCitoyen');
			
			Route::post('registerCitoyen','CitoyenController@store');

		});

	

		Route::get('/',function(){
			
			return view('index');
		});
		
		Route::any('{path?}', function()
		{
			return view("index");

		})->where("path", ".+");

});





