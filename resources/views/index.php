<!DOCTYPE html>
<html lang='fr_FR'>
	<head>
		<base href="/">
		<title>BaladiTY</title>

		<meta charset="UTF-8">
		
		<link rel="shortcut icon" href="images/ic_logo.png"/>

        <meta name="description" content="BaladiTY est une application tunisienne qui permet aux citoyens de déclarer les réclamations d'infrastructure" />
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		
		<link href="css/materialize.css" rel="stylesheet" media="screen,projection">
		<link href='css/angular-carousel.css' rel='stylesheet' type='text/css'>
		<link href="css/style.css" rel="stylesheet" media="screen,projection">
		<link href="css/responsive.css" rel="stylesheet" media="screen,projection">
		
      
		<!-- angular -->
		<script src="js/jquery-2.1.1.min.js"></script> 
		<script src="js/materialize.min.js"></script>
		 <script src="http://cdn.tinymce.com/4/tinymce.min.js"></script>
		<script src="angular/components/angular.min.js"></script>
		<script src="angular/components/angular-route.min.js"></script>

		
	

	</head>

	<body ng-app="myApp" ng-controller="IndexController">

		<div ng-view></div>
		

		<!-- components -->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-touch.min.js"></script>
	    <script src="angular/components/angular-carousel.js"></script>
		

		<script src="angular/components/angular-ui-tinymce.js"></script>
		<script src="angular/components/angular-resource.min.js"></script>
		<script src="angular/components/angular-messages.min.js"></script>
		<script src="angular/components/angular-cookies.min.js"></script>
		<script src="angular/components/ng-map.min.js"></script>
		<script src="angular/components/ngtimeago.js"></script>

		
		
        
		

		<!-- routes -->
		<script src="angular/scripts/routes.js"></script>


		
		

		<!-- services -->
		<script src="angular/scripts/services/authServices.js"></script>
		<script src="angular/scripts/services/resourceServices.js"></script>
		<script src="angular/scripts/services/customizeServices.js"></script>



		<!-- controllers -->
		<script src="angular/scripts/controllers/PasswordController.js"></script>
		<script src="angular/scripts/controllers/IndexController.js"></script>
		<script src="angular/scripts/controllers/SuperAdminController.js"></script>
		<script src="angular/scripts/controllers/AdministrateursController.js"></script>
		<script src="angular/scripts/controllers/AgentsController.js"></script>
		<script src="angular/scripts/controllers/EtablissementsController.js"></script>
		<script src="angular/scripts/controllers/ReclamationsController.js"></script>
		<script src="angular/scripts/controllers/DemandeController.js"></script>
		<script src="angular/scripts/controllers/MunicipalitesController.js"></script>
		<script src="angular/scripts/controllers/ContactController.js"></script>
		<script src="angular/scripts/controllers/AbusesController.js"></script>
		<script src="angular/scripts/controllers/SemblablesController.js"></script>
		

		<!-- filtres -->
		<script src="angular/scripts/customize/filtres.js"></script>
		<script src="angular/scripts/customize/directives.js"></script>
	    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD0oFiiR15VLD-qrlloSXNvgEo4yTFsKLI"></script>
	    
	    	<!-- js -->
		<script src="angular/components/angular-materialize.js"></script>
       <script src="js/index.js"></script>


     
      <script type="text/ng-template" id="error-messages">
			<div ng-message="required">Champ requis</div>
			<div ng-message="minlength">Champ trop courte</div>
			<div ng-message="maxlength">Champ trop longue </div>
			<div ng-message="email">Email non valide</div>
			<div ng-message="pattern">Champ invalide</div>
			<div ng-message="number">Caractères alphabétiques non autorisés</div> 
	 </script>
       
		 

	</body>
</html>