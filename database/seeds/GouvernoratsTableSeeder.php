<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class GouvernoratsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       for($i=1;$i<24;$i++){
       	 DB::table('gouvernorats')->insert([
            'nom' => 'gouvernorat'.$i,
             'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
       }
    }
}
