<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class EtatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('etats')->insert(['nom' => 'Nouvelle', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
        DB::table('etats')->insert(['nom' => 'Confirmé', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
        DB::table('etats')->insert(['nom' => 'Traitement en cours', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
        DB::table('etats')->insert(['nom' => 'Demande de suppression', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
        DB::table('etats')->insert(['nom' => 'En attente', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
        DB::table('etats')->insert(['nom' => 'Résolu', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);

    }
}
