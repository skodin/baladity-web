<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       //  $this->call(GouvernoratsTableSeeder::class);
       //  $this->call(MunicipalitesTableSeeder::class);
         $this->call(EtatTableSeeder::class);
        // $this->call(CategoriesetablissementsTableSeeder::class);
         $this->call(CategoriesproblemesTableSeeder::class);
         $this->call(SuperadminTableSeeder::class);
         
        
    }
}
