<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class MunicipalitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         for($i=1;$i<10;$i++){
       	 DB::table('municipalites')->insert([
            'nom' => 'municipalite'.$i,
            'idGouvernorat'=>$i,
             'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
       }
    }
}
