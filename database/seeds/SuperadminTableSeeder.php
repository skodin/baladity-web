<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class SuperadminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $idUser= DB::table('users')->insertGetId([
            'username' => 'Riadh Rahmi',
            'nom' => 'admin',
            'prenom' => 'admin',
            'email'   => 'rahmiriadh@gmail.com',
            'password' => bcrypt('azerty'),
            'role' => 'superAdmin',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);


       	 DB::table('superadmins')->insert([
            'idUser' => $idUser
        ]);
       
	   
	   //update superAdmin

    }
}
