<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class CategoriesproblemesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

       DB::table('categoriesproblemes')->insert(['nom' => 'Routes dégradées', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
       DB::table('categoriesproblemes')->insert(['nom' => 'Dangers', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
       DB::table('categoriesproblemes')->insert(['nom' => 'Pollution', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
       DB::table('categoriesproblemes')->insert(['nom' => 'Moustiques', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
       DB::table('categoriesproblemes')->insert(['nom' => 'Incivisme / Dépassement', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
       DB::table('categoriesproblemes')->insert(['nom' => 'Problèmes d\'eau', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
       DB::table('categoriesproblemes')->insert(['nom' => 'Problèmes d\'électricité', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
       DB::table('categoriesproblemes')->insert(['nom' => 'Appropriation du trottoir', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
       DB::table('categoriesproblemes')->insert(['nom' => 'Dos-d\'âne', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
       DB::table('categoriesproblemes')->insert(['nom' => 'Autres', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);

    }
}
