<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuperadminsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('superadmins', function(Blueprint $table) {
			$table -> integer('idUser') -> unsigned();
			$table -> primary('idUser');
			$table->timestamps();

		});

		Schema::create('suivre', function(Blueprint $table) {
			$table -> increments('id');
			$table->timestamps();


		});

		Schema::create('semblable', function(Blueprint $table) {
			$table -> increments('id');
			$table -> string('message');
			$table -> dateTime('date');
			$table->timestamps();

		});
		
		Schema::create('photos', function (Blueprint $table) {
            $table->increments('id');
			$table->string('chemin');
			$table->timestamps();
			
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		

	}

}
