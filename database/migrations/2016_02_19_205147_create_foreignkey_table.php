<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignkeyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::table('superadmins', function (Blueprint $table) {
            $table->foreign('idUser')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('admins', function (Blueprint $table) {
            $table->foreign('idUser')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
        

        Schema::table('citoyens', function (Blueprint $table) {
            $table->foreign('idUser')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('commentaires', function (Blueprint $table) {
            $table->integer('idUser')->unsigned();
            $table->foreign('idUser')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('idReclamation')->unsigned();
            $table->foreign('idReclamation')->references('id')->on('reclamations')->onDelete('cascade')->onUpdate('cascade');
        });
        

        Schema::table('reclamations', function (Blueprint $table) {
            $table->integer('idCitoyen')->unsigned();
            $table->foreign('idCitoyen')->references('idUser')->on('citoyens')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('idCategorieProleme')->unsigned();
            $table->foreign('idCategorieProleme')->references('id')->on('categoriesproblemes')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('modifications', function (Blueprint $table) {
            $table->integer('idEtat')->unsigned();
            $table->foreign('idEtat')->references('id')->on('etats')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('idReclamation')->unsigned();
            $table->foreign('idReclamation')->references('id')->on('reclamations')->onDelete('cascade')->onUpdate('cascade');
        });


        Schema::table('photos', function (Blueprint $table) {
            $table->integer('idReclamation')->unsigned();
            $table->foreign('idReclamation')->references('id')->on('reclamations')->onDelete('cascade')->onUpdate('cascade');
        });




        Schema::table('suivre', function (Blueprint $table) {

            $table->integer('idReclamation')->unsigned();
            $table->foreign('idReclamation')->references('id')->on('reclamations')->onDelete('cascade')->onUpdate('cascade');


            $table->integer('idCitoyen')->unsigned();
            $table->foreign('idCitoyen')->references('idUser')->on('citoyens')->onDelete('cascade')->onUpdate('cascade');

        });


        Schema::table('semblable', function (Blueprint $table) {

            $table->integer('idReclamation1')->unsigned();
            $table->foreign('idReclamation1')->references('id')->on('reclamations')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('idReclamation2')->unsigned();
            $table->foreign('idReclamation2')->references('id')->on('reclamations')->onDelete('cascade')->onUpdate('cascade');


            $table->integer('idUser')->unsigned();
            $table->foreign('idUser')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('superadmins', function (Blueprint $table) {
            $table->dropForeign('superadmins_idUser_foreign');
        });

        Schema::table('admins', function (Blueprint $table) {
            $table->dropForeign('admins_idUser_foreign');
        });



        Schema::table('citoyens', function (Blueprint $table) {
            $table->dropForeign('citoyens_idUser_foreign');
        });


        Schema::table('commentaires', function (Blueprint $table) {
            $table->dropForeign('commentaires_idUser_foreign');
            $table->dropForeign('commentaires_idReclamation_foreign');
        });



        Schema::table('reclamations', function (Blueprint $table) {
            $table->dropForeign('reclamations_idCitoyen_foreign');
            $table->dropForeign('reclamations_idCategorieProleme_foreign');
        });

        Schema::table('modifications', function (Blueprint $table) {
            $table->dropForeign('modifications_idEtat_foreign');
            $table->dropForeign('modifications_idReclamation_foreign');
        });

        Schema::table('photos', function (Blueprint $table) {
            $table->dropForeign('photos_idReclamation_foreign');
        });


        Schema::table('suivre', function (Blueprint $table) {
            $table->dropForeign('suivre_idReclamation_foreign');
            $table->dropForeign('suivre_idCitoyen_foreign');
        });

        Schema::table('semblable', function (Blueprint $table) {
            $table->dropForeign('semblable_idReclamation1_foreign');
            $table->dropForeign('semblable_idReclamation2_foreign');
            $table->dropForeign('semblable_idUser_foreign');
        });

        //drop tables

        Schema::drop('users');
        Schema::drop('admins');
        Schema::drop('categoriesproblemes');
        Schema::drop('citoyens');
        Schema::drop('commentaires');
        Schema::drop('etats');
        Schema::drop('modifications');
        Schema::drop('reclamations');
        Schema::drop('superadmins');
        Schema::drop('suivre');
        Schema::drop('semblable');
        Schema::drop('photos');


    }

}
