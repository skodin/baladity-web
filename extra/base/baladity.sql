-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 13 Mai 2016 à 19:19
-- Version du serveur :  5.6.28-0ubuntu0.15.10.1
-- Version de PHP :  5.6.11-1ubuntu3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `baladity`
--

-- --------------------------------------------------------

--
-- Structure de la table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `idUser` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idGouvernorat` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `admins`
--

INSERT INTO `admins` (`idUser`, `created_at`, `updated_at`, `idGouvernorat`) VALUES
(21, '2016-05-11 09:39:28', '2016-05-11 09:39:28', 5),
(22, '2016-05-11 09:39:49', '2016-05-11 09:39:49', 3),
(23, '2016-05-11 09:40:08', '2016-05-11 09:40:08', 7),
(24, '2016-05-11 09:40:30', '2016-05-11 09:40:30', 24),
(25, '2016-05-11 09:40:48', '2016-05-11 09:40:48', 10),
(26, '2016-05-11 09:41:14', '2016-05-11 09:41:14', 22),
(27, '2016-05-11 09:41:35', '2016-05-11 09:41:35', 1),
(28, '2016-05-11 09:41:57', '2016-05-11 09:41:57', 2),
(29, '2016-05-11 09:42:22', '2016-05-11 09:42:22', 3),
(30, '2016-05-11 09:42:51', '2016-05-11 09:42:51', 3),
(31, '2016-05-11 09:43:22', '2016-05-11 09:43:22', 4),
(32, '2016-05-11 09:43:41', '2016-05-11 09:43:41', 5),
(33, '2016-05-11 13:57:05', '2016-05-11 13:57:05', 9);

-- --------------------------------------------------------

--
-- Structure de la table `affectations`
--

CREATE TABLE IF NOT EXISTS `affectations` (
  `id` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idAgent` int(10) unsigned NOT NULL,
  `idReclamation` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `agents`
--

CREATE TABLE IF NOT EXISTS `agents` (
  `idUser` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idEtablissement` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `agents`
--

INSERT INTO `agents` (`idUser`, `created_at`, `updated_at`, `idEtablissement`) VALUES
(4, '2016-05-11 09:21:19', '2016-05-11 09:21:19', 2),
(5, '2016-05-11 09:21:38', '2016-05-11 09:21:38', 10),
(6, '2016-05-11 09:21:53', '2016-05-11 09:21:53', 9),
(7, '2016-05-11 09:22:13', '2016-05-11 09:22:13', 12),
(8, '2016-05-11 09:25:04', '2016-05-11 09:25:04', 7),
(9, '2016-05-11 09:25:22', '2016-05-11 09:25:22', 8),
(10, '2016-05-11 09:25:40', '2016-05-11 09:25:40', 11),
(11, '2016-05-11 09:25:58', '2016-05-11 09:25:58', 13),
(13, '2016-05-11 09:26:32', '2016-05-11 09:26:32', 8),
(14, '2016-05-11 09:26:57', '2016-05-11 09:26:57', 2),
(15, '2016-05-11 09:27:20', '2016-05-11 09:27:20', 4),
(17, '2016-05-11 09:27:41', '2016-05-11 09:27:41', 7),
(18, '2016-05-11 09:37:47', '2016-05-11 09:37:47', 8),
(20, '2016-05-11 09:38:14', '2016-05-11 09:38:14', 9);

-- --------------------------------------------------------

--
-- Structure de la table `categoriesetablissements`
--

CREATE TABLE IF NOT EXISTS `categoriesetablissements` (
  `id` int(10) unsigned NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `categoriesetablissements`
--

INSERT INTO `categoriesetablissements` (`id`, `nom`, `created_at`, `updated_at`) VALUES
(1, 'categoriesetablissement1', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(2, 'categoriesetablissement2', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(3, 'categoriesetablissement3', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(4, 'categoriesetablissement4', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(5, 'categoriesetablissement5', '2016-05-10 14:50:50', '2016-05-10 14:50:50'),
(6, 'categoriesetablissement6', '2016-05-10 14:50:50', '2016-05-10 14:50:50'),
(7, 'categoriesetablissement7', '2016-05-10 14:50:50', '2016-05-10 14:50:50'),
(8, 'categoriesetablissement8', '2016-05-10 14:50:50', '2016-05-10 14:50:50'),
(9, 'categoriesetablissement9', '2016-05-10 14:50:50', '2016-05-10 14:50:50');

-- --------------------------------------------------------

--
-- Structure de la table `categoriesproblemes`
--

CREATE TABLE IF NOT EXISTS `categoriesproblemes` (
  `id` int(10) unsigned NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idCategorieEtablissement` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `categoriesproblemes`
--

INSERT INTO `categoriesproblemes` (`id`, `nom`, `created_at`, `updated_at`, `idCategorieEtablissement`) VALUES
(1, 'categoriesprobleme1', '2016-05-10 14:50:50', '2016-05-10 14:50:50', 1),
(2, 'categoriesprobleme2', '2016-05-10 14:50:50', '2016-05-10 14:50:50', 2),
(3, 'categoriesprobleme3', '2016-05-10 14:50:50', '2016-05-10 14:50:50', 3),
(4, 'categoriesprobleme4', '2016-05-10 14:50:50', '2016-05-10 14:50:50', 4),
(5, 'categoriesprobleme5', '2016-05-10 14:50:50', '2016-05-10 14:50:50', 5),
(6, 'categoriesprobleme6', '2016-05-10 14:50:50', '2016-05-10 14:50:50', 6),
(7, 'categoriesprobleme7', '2016-05-10 14:50:50', '2016-05-10 14:50:50', 7),
(8, 'categoriesprobleme8', '2016-05-10 14:50:50', '2016-05-10 14:50:50', 8),
(9, 'categoriesprobleme9', '2016-05-10 14:50:50', '2016-05-10 14:50:50', 9);

-- --------------------------------------------------------

--
-- Structure de la table `citoyens`
--

CREATE TABLE IF NOT EXISTS `citoyens` (
  `idUser` int(10) unsigned NOT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estBlocker` tinyint(1) NOT NULL,
  `estConfirmer` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `citoyens`
--

INSERT INTO `citoyens` (`idUser`, `telephone`, `estBlocker`, `estConfirmer`, `created_at`, `updated_at`) VALUES
(38, '97221967', 0, 0, '2016-05-12 09:51:56', '2016-05-12 09:51:56');

-- --------------------------------------------------------

--
-- Structure de la table `commentaires`
--

CREATE TABLE IF NOT EXISTS `commentaires` (
  `id` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estAbuse` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idUser` int(10) unsigned NOT NULL,
  `idReclamation` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `commentaires`
--

INSERT INTO `commentaires` (`id`, `date`, `message`, `estAbuse`, `created_at`, `updated_at`, `idUser`, `idReclamation`) VALUES
(12, '2016-05-12 12:22:01', 'pouvez-vous mettre une description s''il vous plaît', 0, '2016-05-12 11:22:01', '2016-05-12 11:22:01', 1, 12),
(13, '2016-05-12 12:22:41', 'oui', 0, '2016-05-12 11:22:26', '2016-05-12 11:22:26', 38, 12),
(14, '2016-05-12 15:09:35', 'this is test', 0, '2016-05-12 14:09:35', '2016-05-12 14:09:35', 1, 30);

-- --------------------------------------------------------

--
-- Structure de la table `etablissements`
--

CREATE TABLE IF NOT EXISTS `etablissements` (
  `id` int(10) unsigned NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idMunicipalite` int(10) unsigned NOT NULL,
  `idCategorieEtablissement` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `etablissements`
--

INSERT INTO `etablissements` (`id`, `nom`, `telephone`, `adresse`, `email`, `fax`, `site`, `created_at`, `updated_at`, `idMunicipalite`, `idCategorieEtablissement`) VALUES
(2, 'teste', '97221967', 'teste', 'rahmiriadh@gmail.com', '77887788', 'https://design.google.com/icons/', '2016-05-10 17:12:32', '2016-05-10 17:12:32', 9, 5),
(3, 'ffbf', '97221978', 'ererer', 'fbfbf@fbf.fb', '798451245', 'erereer', '2016-05-11 08:54:41', '2016-05-11 08:54:41', 8, 4),
(4, 'grgr', '97221978', 'ererer', 'rgrg@rgrg.rg', '78451278', 'rgrgrg', '2016-05-11 08:55:05', '2016-05-11 08:55:05', 3, 2),
(5, 'rgrg', '97221978', 'ererer', 'rahmiriadh@rg.com', '784512487', 'erereer', '2016-05-11 08:55:27', '2016-05-11 08:55:27', 8, 8),
(6, 'ffbf', '97221978', 'ererer', 'rahmiriadh@er.com', '78451236', 'erereer', '2016-05-11 08:55:46', '2016-05-11 08:55:46', 11, 7),
(7, 'ffbf', '97221978', 'ererer', 'fbfbf@azfbf.fb', '7845125', 'erereer', '2016-05-11 08:56:12', '2016-05-11 08:56:12', 8, 4),
(8, 'ffbf', '97221978', 'ererer', 'rahmiriadh@gh.com', '99663322', 'erereer', '2016-05-11 08:56:35', '2016-05-11 08:56:35', 8, 3),
(9, 'io', '97221978', 'ererer', 'rahmiriadh@io.com', '78451245', 'ioioio', '2016-05-11 08:57:00', '2016-05-11 08:57:00', 9, 5),
(10, 'ffbf', '97221978', 'ererer', 'rahmiriadh@yy.com', '99666699', 'erereer', '2016-05-11 08:58:36', '2016-05-11 08:58:36', 8, 9),
(11, 'ffbf', '97221978', 'ererer', 'rahmiriadh@jj.com', '33221155', 'erereer', '2016-05-11 08:58:57', '2016-05-11 08:58:57', 21, 5),
(12, 'ffbf', '97221978', 'ererer', 'rahmiriadh@gmail.ll', '45127899', 'erereer', '2016-05-11 08:59:23', '2016-05-11 08:59:23', 11, 7),
(13, 'ffbf', '97221978', 'ererer', 'rahmiriadh@gmail.mm', '45124544', 'erereer', '2016-05-11 08:59:48', '2016-05-11 08:59:48', 9, 5),
(14, 'ffbf', '97221978', 'ererer', 'rahmiriadh@er.com', '78451256', 'erereer', '2016-05-11 09:01:24', '2016-05-11 09:01:24', 8, 7),
(15, 'ffbf', '97221978', 'ererer', 'rahmiriadh@gmail.ty', '78451269', 'erereer', '2016-05-11 09:01:46', '2016-05-11 09:01:46', 7, 2),
(16, 'ffbf', '97221978', 'ererer', 'rahmiriadh@oo.com', '78451296', 'erereer', '2016-05-11 09:02:05', '2016-05-11 09:02:05', 7, 2),
(17, 'ffbf', '97221978', 'ererer', 'rahmiriadh@op.com', '78457845', 'erereer', '2016-05-11 09:02:25', '2016-05-11 09:02:25', 5, 5);

-- --------------------------------------------------------

--
-- Structure de la table `etats`
--

CREATE TABLE IF NOT EXISTS `etats` (
  `id` int(10) unsigned NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `etats`
--

INSERT INTO `etats` (`id`, `nom`, `created_at`, `updated_at`) VALUES
(1, 'etat1', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(2, 'etat2', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(3, 'etat3', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(4, 'etat4', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(5, 'etat5', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(6, 'etat6', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(7, 'etat7', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(8, 'etat8', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(9, 'etat9', '2016-05-10 14:50:49', '2016-05-10 14:50:49');

-- --------------------------------------------------------

--
-- Structure de la table `gouvernorats`
--

CREATE TABLE IF NOT EXISTS `gouvernorats` (
  `id` int(10) unsigned NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `gouvernorats`
--

INSERT INTO `gouvernorats` (`id`, `nom`, `created_at`, `updated_at`) VALUES
(1, 'Tunis', '2016-05-10 14:50:48', '2016-05-10 14:50:48'),
(2, 'Monastir', '2016-05-10 14:50:48', '2016-05-10 14:50:48'),
(3, 'Sousse', '2016-05-10 14:50:48', '2016-05-10 14:50:48'),
(4, 'Mahdia', '2016-05-10 14:50:48', '2016-05-10 14:50:48'),
(5, 'Bizerte', '2016-05-10 14:50:48', '2016-05-10 14:50:48'),
(6, 'Beja', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(7, 'Ariana', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(8, 'Ben arous', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(9, 'Gabes', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(10, 'Gafsa', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(11, 'Jendouba', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(12, 'Kairouan', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(13, 'Kasserine', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(14, 'Kebili', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(15, 'Manouba', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(16, 'Medenine', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(17, 'Nabeul', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(18, 'Sfax', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(19, 'Sidi bouzid', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(20, 'Siliana', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(21, 'Tataouine', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(22, 'Tozeur', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(23, 'Zaghouan', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(24, 'Le Kef', '2016-05-10 14:52:50', '2016-05-10 14:52:50');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_02_18_222636_create_admins_table', 1),
('2016_02_18_222729_create_affectations_table', 1),
('2016_02_18_222749_create_agents_table', 1),
('2016_02_18_222836_create_categoriesEtablissement_table', 1),
('2016_02_18_222920_create_categoriesProbleme_table', 1),
('2016_02_18_222939_create_citoyens_table', 1),
('2016_02_18_223006_create_commentaires_table', 1),
('2016_02_18_223033_create_delegations_table', 1),
('2016_02_18_223118_create_etablissements_table', 1),
('2016_02_18_223132_create_etats_table', 1),
('2016_02_18_223201_create_gouvernorats_table', 1),
('2016_02_18_223225_create_modifications_table', 1),
('2016_02_18_223250_create_problemes_table', 1),
('2016_02_18_223418_create_superadmins_table', 1),
('2016_02_19_205147_create_foreignkey_table', 1);

-- --------------------------------------------------------

--
-- Structure de la table `modifications`
--

CREATE TABLE IF NOT EXISTS `modifications` (
  `id` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idEtat` int(10) unsigned NOT NULL,
  `idReclamation` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `modifications`
--

INSERT INTO `modifications` (`id`, `date`, `message`, `created_at`, `updated_at`, `idEtat`, `idReclamation`) VALUES
(16, '2016-05-12 11:50:18', 'réclamation en attente', '2016-05-12 10:50:18', '2016-05-12 10:50:18', 1, 11),
(17, '2016-05-12 11:51:59', 'réclamation en attente', '2016-05-12 10:51:59', '2016-05-12 10:51:59', 1, 12),
(18, '2016-05-12 11:53:05', 'réclamation en attente', '2016-05-12 10:53:05', '2016-05-12 10:53:05', 1, 13),
(19, '2016-05-12 11:55:30', 'réclamation en attente', '2016-05-12 10:55:30', '2016-05-12 10:55:30', 1, 14),
(20, '2016-05-12 12:00:24', 'réclamation en attente', '2016-05-12 11:00:24', '2016-05-12 11:00:24', 1, 15),
(21, '2016-05-12 12:02:42', 'réclamation en attente', '2016-05-12 11:02:42', '2016-05-12 11:02:42', 1, 16),
(22, '2016-05-12 12:05:46', 'réclamation en attente', '2016-05-12 11:05:46', '2016-05-12 11:05:46', 1, 17),
(23, '2016-05-12 12:07:38', 'réclamation en attente', '2016-05-12 11:07:38', '2016-05-12 11:07:38', 1, 18),
(24, '2016-05-12 12:09:22', 'réclamation en attente', '2016-05-12 11:09:22', '2016-05-12 11:09:22', 1, 19),
(25, '2016-05-12 12:10:38', 'réclamation en attente', '2016-05-12 11:10:38', '2016-05-12 11:10:38', 1, 20),
(26, '2016-05-12 12:13:21', 'réclamation en attente', '2016-05-12 11:13:21', '2016-05-12 11:13:21', 1, 21),
(27, '2016-05-12 12:15:51', 'réclamation en attente', '2016-05-12 11:15:51', '2016-05-12 11:15:51', 1, 22),
(28, '2016-05-12 12:18:26', 'réclamation en attente', '2016-05-12 11:18:26', '2016-05-12 11:18:26', 1, 23),
(29, '2016-05-12 12:20:24', 'réclamation en attente', '2016-05-12 11:20:24', '2016-05-12 11:20:24', 1, 24),
(30, '2016-05-12 12:24:39', 'réclamation en attente', '2016-05-12 11:24:39', '2016-05-12 11:24:39', 1, 25),
(31, '2016-05-12 12:27:46', 'réclamation en attente', '2016-05-12 11:27:46', '2016-05-12 11:27:46', 1, 26),
(32, '2016-05-12 12:28:59', 'réclamation en attente', '2016-05-12 11:28:59', '2016-05-12 11:28:59', 1, 27),
(33, '2016-05-12 12:30:02', 'réclamation en attente', '2016-05-12 11:30:02', '2016-05-12 11:30:02', 1, 28),
(34, '2016-05-12 12:35:25', 'réclamation en attente', '2016-05-12 11:35:25', '2016-05-12 11:35:25', 1, 29),
(35, '2016-05-12 12:37:35', 'réclamation en attente', '2016-05-12 11:37:35', '2016-05-12 11:37:35', 1, 30),
(36, '2016-05-12 12:40:03', 'réclamation en attente', '2016-05-12 11:40:03', '2016-05-12 11:40:03', 1, 31),
(37, '2016-05-12 12:41:00', 'réclamation en attente', '2016-05-12 11:41:00', '2016-05-12 11:41:00', 1, 32),
(38, '2016-05-12 12:41:52', 'réclamation en attente', '2016-05-12 11:41:52', '2016-05-12 11:41:52', 1, 33),
(39, '2016-05-12 12:43:25', 'réclamation en attente', '2016-05-12 11:43:25', '2016-05-12 11:43:25', 1, 34),
(40, '2016-05-12 12:45:24', 'réclamation en attente', '2016-05-12 11:45:24', '2016-05-12 11:45:24', 1, 35),
(41, '2016-05-12 12:47:17', 'réclamation en attente', '2016-05-12 11:47:17', '2016-05-12 11:47:17', 1, 36),
(42, '2016-05-12 15:09:22', 'réclamation corrigé', '2016-05-12 14:09:22', '2016-05-12 14:09:22', 2, 30);

-- --------------------------------------------------------

--
-- Structure de la table `municipalites`
--

CREATE TABLE IF NOT EXISTS `municipalites` (
  `id` int(10) unsigned NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idGouvernorat` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `municipalites`
--

INSERT INTO `municipalites` (`id`, `nom`, `created_at`, `updated_at`, `idGouvernorat`) VALUES
(1, 'municipalité Tunisie', '2016-05-10 14:50:49', '2016-05-11 10:37:47', 1),
(2, 'municipalite2', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 2),
(3, 'municipalite3', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 3),
(4, 'municipalite4', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 4),
(5, 'municipalite5', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 5),
(6, 'municipalite6', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 6),
(7, 'municipalite7', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 7),
(8, 'municipalite8', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 8),
(9, 'municipalite9', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 9),
(10, 'municipalite9', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 10),
(11, 'municipalite9', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 11),
(12, 'municipalite9', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 12),
(13, 'municipalite9', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 13),
(14, 'municipalite9', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 14),
(15, 'municipalite9', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 15),
(16, 'municipalite9', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 16),
(17, 'municipalite9', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 17),
(18, 'municipalite9', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 18),
(19, 'municipalite9', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 19),
(20, 'municipalite9', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 20),
(21, 'municipalite9', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 21),
(22, 'municipalite9', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 22),
(23, 'municipalite9', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 23),
(24, 'municipalite9', '2016-05-10 14:50:49', '2016-05-10 14:50:49', 24);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(10) unsigned NOT NULL,
  `chemin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idReclamation` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `photos`
--

INSERT INTO `photos` (`id`, `chemin`, `created_at`, `updated_at`, `idReclamation`) VALUES
(11, 'images/problemes/claim_11_1_2016_05_12_11_50_18.png', '2016-05-12 10:50:18', '2016-05-12 10:50:18', 11),
(12, 'images/problemes/claim_12_1_2016_05_12_11_52_00.png', '2016-05-12 10:52:00', '2016-05-12 10:52:00', 12),
(13, 'images/problemes/claim_13_1_2016_05_12_11_53_05.png', '2016-05-12 10:53:05', '2016-05-12 10:53:05', 13),
(14, 'images/problemes/claim_14_1_2016_05_12_11_55_30.png', '2016-05-12 10:55:30', '2016-05-12 10:55:30', 14),
(15, 'images/problemes/claim_15_1_2016_05_12_12_00_24.png', '2016-05-12 11:00:24', '2016-05-12 11:00:24', 15),
(16, 'images/problemes/claim_16_1_2016_05_12_12_02_42.png', '2016-05-12 11:02:42', '2016-05-12 11:02:42', 16),
(17, 'images/problemes/claim_17_1_2016_05_12_12_05_46.png', '2016-05-12 11:05:46', '2016-05-12 11:05:46', 17),
(18, 'images/problemes/claim_18_1_2016_05_12_12_07_38.png', '2016-05-12 11:07:38', '2016-05-12 11:07:38', 18),
(19, 'images/problemes/claim_19_1_2016_05_12_12_09_22.png', '2016-05-12 11:09:22', '2016-05-12 11:09:22', 19),
(20, 'images/problemes/claim_20_1_2016_05_12_12_10_38.png', '2016-05-12 11:10:38', '2016-05-12 11:10:38', 20),
(21, 'images/problemes/claim_21_1_2016_05_12_12_13_21.png', '2016-05-12 11:13:21', '2016-05-12 11:13:21', 21),
(22, 'images/problemes/claim_22_1_2016_05_12_12_15_51.png', '2016-05-12 11:15:51', '2016-05-12 11:15:51', 22),
(23, 'images/problemes/claim_23_1_2016_05_12_12_18_26.png', '2016-05-12 11:18:26', '2016-05-12 11:18:26', 23),
(24, 'images/problemes/claim_24_1_2016_05_12_12_20_24.png', '2016-05-12 11:20:24', '2016-05-12 11:20:24', 24),
(25, 'images/problemes/claim_25_1_2016_05_12_12_24_39.png', '2016-05-12 11:24:39', '2016-05-12 11:24:39', 25),
(26, 'images/problemes/claim_26_1_2016_05_12_12_27_46.png', '2016-05-12 11:27:46', '2016-05-12 11:27:46', 26),
(27, 'images/problemes/claim_27_1_2016_05_12_12_28_59.png', '2016-05-12 11:28:59', '2016-05-12 11:28:59', 27),
(28, 'images/problemes/claim_28_1_2016_05_12_12_30_02.png', '2016-05-12 11:30:02', '2016-05-12 11:30:02', 28),
(29, 'images/problemes/claim_29_1_2016_05_12_12_35_25.png', '2016-05-12 11:35:25', '2016-05-12 11:35:25', 29),
(30, 'images/problemes/claim_30_1_2016_05_12_12_37_35.png', '2016-05-12 11:37:35', '2016-05-12 11:37:35', 30),
(31, 'images/problemes/claim_31_1_2016_05_12_12_40_03.png', '2016-05-12 11:40:03', '2016-05-12 11:40:03', 31),
(32, 'images/problemes/claim_32_1_2016_05_12_12_41_00.png', '2016-05-12 11:41:00', '2016-05-12 11:41:00', 32),
(33, 'images/problemes/claim_33_1_2016_05_12_12_41_52.png', '2016-05-12 11:41:52', '2016-05-12 11:41:52', 33),
(34, 'images/problemes/claim_34_1_2016_05_12_12_43_25.png', '2016-05-12 11:43:25', '2016-05-12 11:43:25', 34),
(35, 'images/problemes/claim_35_1_2016_05_12_12_45_24.png', '2016-05-12 11:45:24', '2016-05-12 11:45:24', 35),
(36, 'images/problemes/claim_36_1_2016_05_12_12_47_17.png', '2016-05-12 11:47:17', '2016-05-12 11:47:17', 36);

-- --------------------------------------------------------

--
-- Structure de la table `reclamations`
--

CREATE TABLE IF NOT EXISTS `reclamations` (
  `id` int(10) unsigned NOT NULL,
  `degree` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` double(9,6) NOT NULL,
  `latitude` double(9,6) NOT NULL,
  `flag` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idCitoyen` int(10) unsigned NOT NULL,
  `idMunicipalite` int(10) unsigned NOT NULL,
  `idCategorieProleme` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `reclamations`
--

INSERT INTO `reclamations` (`id`, `degree`, `description`, `longitude`, `latitude`, `flag`, `created_at`, `updated_at`, `idCitoyen`, `idMunicipalite`, `idCategorieProleme`) VALUES
(11, 1, 'panneau publicitaire cache les voitures qui passent devant l entree du parking md 5 a tunis', 10.819021, 35.769883, 1, '2016-05-12 10:50:18', '2016-05-12 10:50:18', 38, 1, 3),
(12, 1, 'fhvvvg', 10.819109, 35.769625, 1, '2016-05-12 10:51:59', '2016-05-12 10:51:59', 38, 1, 4),
(13, 1, 'fgggg', 10.819033, 35.769859, 1, '2016-05-12 10:53:05', '2016-05-12 10:53:05', 38, 4, 4),
(14, 1, 'c''est vraimant grand grand pollution', 10.819058, 35.769820, 1, '2016-05-12 10:55:30', '2016-05-12 10:55:30', 38, 5, 4),
(15, 1, 'الرّجاء التدخّل روائح كريهة وأوساخ نتيجة مياه المحلاّت المطعم والمقهى', 10.819018, 35.769879, 1, '2016-05-12 11:00:24', '2016-05-12 11:00:24', 38, 2, 2),
(16, 1, 'Trou dans le trottoir.', 10.819095, 35.769830, 1, '2016-05-12 11:02:42', '2016-05-12 11:02:42', 38, 3, 3),
(17, 1, 'Dans la rue de Limam Ibn Arfa a El manzah 8 a Tunis, nous rencontrons ce tas de poubelle.', 10.819064, 35.769897, 1, '2016-05-12 11:05:46', '2016-05-12 11:05:46', 38, 5, 5),
(18, 1, 'Chaque fois que la pluie tombe, cette route sera impasable pour un piétant sauf si vous êtes motorisé, et elle est dans cette situation depuis longtemps que je ne pas même me souvenir', 10.819062, 35.769888, 1, '2016-05-12 11:07:38', '2016-05-12 11:07:38', 38, 4, 5),
(19, 1, 'chaque hiver, cette route se transforme en une lac', 10.819068, 35.769882, 1, '2016-05-12 11:09:22', '2016-05-12 11:09:22', 38, 4, 5),
(20, 1, 'fggv', 10.819075, 35.769844, 1, '2016-05-12 11:10:38', '2016-05-12 11:10:38', 38, 4, 2),
(21, 1, 'C''est le quotidien que nous vivons avec les poubelles, les ordures au rue de la trippe devant la mosquée Sidi El haffi', 10.818992, 35.769971, 1, '2016-05-12 11:13:21', '2016-05-12 11:13:21', 38, 5, 6),
(22, 1, 'couvercle regard absent', 10.819067, 35.769878, 1, '2016-05-12 11:15:50', '2016-05-12 11:15:50', 38, 6, 5),
(23, 1, 'البناء العشوائي والفوضوي لﻷكشاك قبالة بلدية تستور دون رقيب من رئيس النيابة الخصوصية البحري الميساوي', 10.819081, 35.769865, 1, '2016-05-12 11:18:26', '2016-05-12 11:18:26', 38, 6, 5),
(24, 1, 'l''infra structure à gabes', 10.819085, 35.769847, 1, '2016-05-12 11:20:24', '2016-05-12 11:20:24', 38, 6, 7),
(25, 1, 'danger mortel au plein centre de la ville de testour. juste devant la poste et tout près du point de vente d''articles d''électronique ménager de Bahri missaoui; président de la commune de testour', 10.819074, 35.769866, 1, '2016-05-12 11:24:38', '2016-05-12 11:24:38', 38, 5, 5),
(26, 1, 'ordures non ramassées accumulation. Une benne à ordures pour 2 immeubles', 10.819054, 35.769925, 1, '2016-05-12 11:27:46', '2016-05-12 11:27:46', 38, 5, 5),
(27, 1, 'Risque d''accidents en cas de freinage', 10.819017, 35.769908, 1, '2016-05-12 11:28:59', '2016-05-12 11:28:59', 38, 3, 3),
(28, 1, 'Trop d''ordures devant l''hôpital ! C''est un établissement de santé ! Tout devrait être propre à l''intérieur comme à l''extérieur', 10.819061, 35.769888, 1, '2016-05-12 11:30:02', '2016-05-12 11:30:02', 38, 3, 4),
(29, 1, 'passage des personnes aveugles', 10.819032, 35.769935, 1, '2016-05-12 11:35:25', '2016-05-12 11:35:25', 38, 5, 5),
(30, 1, 'odeur insupportable eau * produits chimique', 10.819145, 35.769565, 1, '2016-05-12 11:37:35', '2016-05-12 11:37:35', 38, 4, 4),
(31, 1, 'qertty', 10.819097, 35.769655, 1, '2016-05-12 11:40:03', '2016-05-12 11:40:03', 38, 5, 5),
(32, 1, 'qegv vgggg', 10.819064, 35.769891, 1, '2016-05-12 11:41:00', '2016-05-12 11:41:00', 38, 3, 3),
(33, 1, 'chaque matin les marchands de légumes et de fruits jettent leurs déchets sur le trottoir de la rue Taïeb mehiri en plein centre de la ville de Bizerte ces tas d''ordures s''amoncellent tout au long de la journée sans qu''elles soient levées', 10.819056, 35.769849, 1, '2016-05-12 11:41:52', '2016-05-12 11:41:52', 38, 6, 7),
(34, 1, 'changement du tompon regar onas', 10.819159, 35.769712, 1, '2016-05-12 11:43:25', '2016-05-12 11:43:25', 38, 6, 5),
(35, 1, 'Construction anarchique avec empiètement sur les biens publics (poteau d''éclairage)', 10.819169, 35.769644, 1, '2016-05-12 11:45:24', '2016-05-12 11:45:24', 38, 6, 6),
(36, 1, 'égout sans couvercle', 10.819017, 35.769884, 1, '2016-05-12 11:47:17', '2016-05-12 11:47:17', 38, 6, 6);

-- --------------------------------------------------------

--
-- Structure de la table `semblable`
--

CREATE TABLE IF NOT EXISTS `semblable` (
  `id` int(10) unsigned NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idReclamation1` int(10) unsigned NOT NULL,
  `idReclamation2` int(10) unsigned NOT NULL,
  `idUser` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `suivre`
--

CREATE TABLE IF NOT EXISTS `suivre` (
  `id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idReclamation` int(10) unsigned NOT NULL,
  `idCitoyen` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `suivre`
--

INSERT INTO `suivre` (`id`, `created_at`, `updated_at`, `idReclamation`, `idCitoyen`) VALUES
(1, '2016-05-13 14:35:11', '2016-05-13 14:35:11', 36, 38),
(2, '2016-05-13 14:35:13', '2016-05-13 14:35:13', 34, 38),
(3, '2016-05-13 14:35:15', '2016-05-13 14:35:15', 32, 38);

-- --------------------------------------------------------

--
-- Structure de la table `superadmins`
--

CREATE TABLE IF NOT EXISTS `superadmins` (
  `idUser` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `superadmins`
--

INSERT INTO `superadmins` (`idUser`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` enum('citoyen','agent','admin','superAdmin') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `username`, `nom`, `prenom`, `email`, `password`, `photo`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Riadh Rahmi', 'Rahmi', 'Riadh', 'rahmiriadh@gmail.com', '$2y$10$mV5XT2XLFAUemDYdoa..Ze2LdAQ73zg0wtfNvEtHBqs3JjK6tHyrK', '', 'superAdmin', '2016-05-10 14:50:50', '2016-05-10 14:50:50'),
(3, 'dfdfdf', 'teste', 'dfdfdf', 'ddfd@dfdf.df', '$2y$10$zj31tKT7HpVq8HqGO2KYAeTP0GXPtS3dLQXZGR0.yFOFic0D.Zx9q', 'images/users/user_3_2016_05_10_18_08_56.png', 'agent', '2016-05-10 17:08:56', '2016-05-10 17:08:56'),
(4, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@rt.com', '$2y$10$cSdLSEFafwP1QpkxZB9fL.pJm4DfMb/pDKAwZXqyTid.gH957IO5G', 'images/users/user_4_2016_05_11_10_21_19.jpg', 'agent', '2016-05-11 09:21:19', '2016-05-11 09:21:19'),
(5, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@gmail.tyt', '$2y$10$/5dtaooCI5T92K5M35IzHewNQiA6S/OxpuJUbKZkTGOD4ec1ONxT.', 'images/users/user_5_2016_05_11_10_21_38.jpg', 'agent', '2016-05-11 09:21:38', '2016-05-11 09:21:38'),
(6, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@ty.com', '$2y$10$pIrYRVLd9ov/t6JBBMe8m.tIC49Z2yIsRJdGumwMyekves/cCmHbi', '', 'agent', '2016-05-11 09:21:53', '2016-05-11 09:21:53'),
(7, 'ghghgh', 'ffbf', 'ty', 'rahmiriadh@gmail.tyty', '$2y$10$ovCbiwlM3VjH829rAhqzYOL76yheu8TSFJ2oEgjp7VfZlNbY7/gIm', 'images/users/user_7_2016_05_11_10_22_13.jpg', 'agent', '2016-05-11 09:22:13', '2016-05-11 09:22:13'),
(8, 'yjyj', 'yjyj', 'yjyjy', 'rahmiriadh@yj.com', '$2y$10$h9NQw5q4uy4SNzFTvfgkVuj9qbq/rRFpWRQ82oy1.vs514ZBMYZem', 'images/users/user_8_2016_05_11_10_25_04.jpg', 'agent', '2016-05-11 09:25:04', '2016-05-11 09:25:04'),
(9, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@gmail.yy', '$2y$10$izdzJEw.ymkQgLL20m0/euTgbBq9VZO9gHPui62rTGamSJ10I9m52', '', 'agent', '2016-05-11 09:25:22', '2016-05-11 09:25:22'),
(10, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@gmail.zz', '$2y$10$N.I33rpJ.VHF4l7ivgqbpe/FoedJ9Rn1GaWngs5C7bqkilKgg9lwS', '', 'agent', '2016-05-11 09:25:40', '2016-05-11 09:25:40'),
(11, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@gmail.io', '$2y$10$MivXJSz/lq2RerSdKr7YbOfoBsGKzgIVD0EPA0R5.dizakCzHwTsW', '', 'agent', '2016-05-11 09:25:58', '2016-05-11 09:25:58'),
(13, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@ty.uu', '$2y$10$II6lQeORu1CtZN5OTzF9P.n/lNbdJ5eWRYxd2eV4uAPg5fncKfN1.', '', 'agent', '2016-05-11 09:26:32', '2016-05-11 09:26:32'),
(14, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@yyyy.com', '$2y$10$3bptjgxzYdYQDmMZ9E7Zl.r4cg6snf7JbFrimf1Q3wslOSUKzw/1e', '', 'agent', '2016-05-11 09:26:57', '2016-05-11 09:26:57'),
(15, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@gmail.hh', '$2y$10$GOP84ttqz0WOWignjZ8e/OymfAy3z31TBuh26Hjd77L5zKyPKy61.', '', 'agent', '2016-05-11 09:27:20', '2016-05-11 09:27:20'),
(17, 'ghghgh', 'yjyj', 'yjyjy', 'rahmiriadh@gmail.huh', '$2y$10$KZQKEgz9Of55nVZOediJMuk1Qu2WlhxLHSQ514RgZ5PK4wNlQg1Sa', '', 'agent', '2016-05-11 09:27:41', '2016-05-11 09:27:41'),
(18, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@gmail.ddd', '$2y$10$VIG3tkTTjwAlYG/yK9eWlOonyttxOcrxG.6yMcmijXhG.szZiugri', '', 'agent', '2016-05-11 09:37:47', '2016-05-11 09:37:47'),
(20, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@rtrt.com', '$2y$10$qSaUifpMT6Ev6hul3TQrHuRUuV3OcOVzgWmiBJcWTjALdB1frv3jm', '', 'agent', '2016-05-11 09:38:14', '2016-05-11 09:38:14'),
(21, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@jklk.com', '$2y$10$Av95WRLXbITWpdQgU3bgzeDN914cPFuLkW98In855RbDJQ0BdI4uC', '', 'admin', '2016-05-11 09:39:28', '2016-05-11 09:39:28'),
(22, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@lmlm.com', '$2y$10$SCeenBF46gnH9Ub1Iu0yEePsmpznRK5/w7hMQMzfTnNNNjIxEwB9C', 'images/users/user_22_2016_05_11_10_39_49.png', 'admin', '2016-05-11 09:39:49', '2016-05-11 09:39:49'),
(23, 'ghghgh', 'yjyj', 'ghgh', 'rahmiriadh@gmail.ghgh', '$2y$10$vDA.tMeI2xgxFHxQl9FmYuGhvk9dmFUcUIV1VLx01bnLspgWvPz3.', 'images/users/user_23_2016_05_11_10_40_08.jpg', 'admin', '2016-05-11 09:40:08', '2016-05-11 09:40:08'),
(24, 'yjyj', 'yjyj', 'yjyjy', 'fbfbf@fbf.fbh', '$2y$10$4/AYaqEaffZHOcFcO75laeC.8g2o8jupgU3QMT3sSF9BiNjCWGupC', 'images/users/user_24_2016_05_11_10_40_30.png', 'admin', '2016-05-11 09:40:30', '2016-05-11 09:40:30'),
(25, 'yjyj', 'ffbf', 'ghgh', 'fbfbf@fbf.ty', '$2y$10$j4ZsTfiOBAAZrO/stHGqGufyOfnBLJ3k/fr7S/jbNoUIZqxFG12tG', '', 'admin', '2016-05-11 09:40:48', '2016-05-11 09:40:48'),
(26, 'ghghgh', 'ty', 'ghgh', 'fbfbf@fbf.fby', '$2y$10$aJOqMyHJMHre0PEGrXN4QejzKv7nCBmFzARekrKp/EdCoO1lI8ivq', '', 'admin', '2016-05-11 09:41:14', '2016-05-11 09:41:14'),
(27, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@gmail.nnn', '$2y$10$4kWxuhAU.xlhQUxGVBsiIeDF4c5YaOIree009oY91iJCTYZOtCt7y', 'images/users/user_27_2016_05_11_10_41_35.png', 'admin', '2016-05-11 09:41:34', '2016-05-11 09:41:35'),
(28, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@aqw.com', '$2y$10$qe9pghKNeUEUGTwCwnhOXuJmOVbCxuPaWWXpJ8mFhZOXVYYF60tE.', '', 'admin', '2016-05-11 09:41:57', '2016-05-11 09:41:57'),
(29, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@azerty.com', '$2y$10$7PS4Vbx/cQeb.IM1SqwEGu8Ryt8H6Mg0OJR1CwERRBkUz/LVR5a8G', '', 'admin', '2016-05-11 09:42:22', '2016-05-11 09:42:22'),
(30, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@sdxcfv.com', '$2y$10$ljV2tlIwsck5E8RlBMFM.upDYeUYUc0JzdCW7.faJnUnMWiGvPq7K', '', 'admin', '2016-05-11 09:42:51', '2016-05-11 09:42:51'),
(31, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@gmail.drd', '$2y$10$3nVrFNu9rQ7H6qJqI96UieWnc1z3Q9Pz17DQyLn07ppNRBnp5l2mG', '', 'admin', '2016-05-11 09:43:22', '2016-05-11 09:43:22'),
(32, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@sdsde.com', '$2y$10$XmHksjLBH07cijV49Io9Pu5VJDivS4fqjDvO/W8XWQ9yWqg/L5ut.', '', 'admin', '2016-05-11 09:43:41', '2016-05-11 09:43:41'),
(33, 'ghghgh', 'ffbf', 'ghgh', 'rahmiriadh@teste.com', '$2y$10$nmCS0HOTICwqGjb7Z3udxeaONd3iErVjA8MqWOKIzNc.siOXLXP7y', '', 'admin', '2016-05-11 13:57:04', '2016-05-11 13:57:04'),
(38, 'Riadh Rahmi', 'riadh', 'rahmi', 'rahmiriadh@gmail.fr', '$2y$10$e5TJd0zaRUCs.AmF3PfF/.Aq6JzaCNXRMEHTOhEW4CRZoLUjKu4SO', 'images/users/user_38_2016_05_12_11_58_25.png', 'citoyen', '2016-05-12 09:51:55', '2016-05-12 10:58:25');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`idUser`),
  ADD KEY `admins_idgouvernorat_foreign` (`idGouvernorat`);

--
-- Index pour la table `affectations`
--
ALTER TABLE `affectations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `affectations_idagent_foreign` (`idAgent`),
  ADD KEY `affectations_idreclamation_foreign` (`idReclamation`);

--
-- Index pour la table `agents`
--
ALTER TABLE `agents`
  ADD PRIMARY KEY (`idUser`),
  ADD KEY `agents_idetablissement_foreign` (`idEtablissement`);

--
-- Index pour la table `categoriesetablissements`
--
ALTER TABLE `categoriesetablissements`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `categoriesproblemes`
--
ALTER TABLE `categoriesproblemes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoriesproblemes_idcategorieetablissement_foreign` (`idCategorieEtablissement`);

--
-- Index pour la table `citoyens`
--
ALTER TABLE `citoyens`
  ADD PRIMARY KEY (`idUser`);

--
-- Index pour la table `commentaires`
--
ALTER TABLE `commentaires`
  ADD PRIMARY KEY (`id`),
  ADD KEY `commentaires_iduser_foreign` (`idUser`),
  ADD KEY `commentaires_idreclamation_foreign` (`idReclamation`);

--
-- Index pour la table `etablissements`
--
ALTER TABLE `etablissements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `etablissements_idmunicipalite_foreign` (`idMunicipalite`),
  ADD KEY `etablissements_idcategorieetablissement_foreign` (`idCategorieEtablissement`);

--
-- Index pour la table `etats`
--
ALTER TABLE `etats`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `gouvernorats`
--
ALTER TABLE `gouvernorats`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `modifications`
--
ALTER TABLE `modifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `modifications_idetat_foreign` (`idEtat`),
  ADD KEY `modifications_idreclamation_foreign` (`idReclamation`);

--
-- Index pour la table `municipalites`
--
ALTER TABLE `municipalites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `municipalites_idgouvernorat_foreign` (`idGouvernorat`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Index pour la table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `photos_idreclamation_foreign` (`idReclamation`);

--
-- Index pour la table `reclamations`
--
ALTER TABLE `reclamations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reclamations_idcitoyen_foreign` (`idCitoyen`),
  ADD KEY `reclamations_idmunicipalite_foreign` (`idMunicipalite`),
  ADD KEY `reclamations_idcategorieproleme_foreign` (`idCategorieProleme`);

--
-- Index pour la table `semblable`
--
ALTER TABLE `semblable`
  ADD PRIMARY KEY (`id`),
  ADD KEY `semblable_idreclamation1_foreign` (`idReclamation1`),
  ADD KEY `semblable_idreclamation2_foreign` (`idReclamation2`),
  ADD KEY `semblable_iduser_foreign` (`idUser`);

--
-- Index pour la table `suivre`
--
ALTER TABLE `suivre`
  ADD PRIMARY KEY (`id`),
  ADD KEY `suivre_idreclamation_foreign` (`idReclamation`),
  ADD KEY `suivre_idcitoyen_foreign` (`idCitoyen`);

--
-- Index pour la table `superadmins`
--
ALTER TABLE `superadmins`
  ADD PRIMARY KEY (`idUser`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `affectations`
--
ALTER TABLE `affectations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `categoriesetablissements`
--
ALTER TABLE `categoriesetablissements`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `categoriesproblemes`
--
ALTER TABLE `categoriesproblemes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `commentaires`
--
ALTER TABLE `commentaires`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `etablissements`
--
ALTER TABLE `etablissements`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `etats`
--
ALTER TABLE `etats`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `gouvernorats`
--
ALTER TABLE `gouvernorats`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT pour la table `modifications`
--
ALTER TABLE `modifications`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT pour la table `municipalites`
--
ALTER TABLE `municipalites`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT pour la table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT pour la table `reclamations`
--
ALTER TABLE `reclamations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT pour la table `semblable`
--
ALTER TABLE `semblable`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `suivre`
--
ALTER TABLE `suivre`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `admins`
--
ALTER TABLE `admins`
  ADD CONSTRAINT `admins_idgouvernorat_foreign` FOREIGN KEY (`idGouvernorat`) REFERENCES `gouvernorats` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `admins_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `affectations`
--
ALTER TABLE `affectations`
  ADD CONSTRAINT `affectations_idagent_foreign` FOREIGN KEY (`idAgent`) REFERENCES `agents` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `affectations_idreclamation_foreign` FOREIGN KEY (`idReclamation`) REFERENCES `reclamations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `agents`
--
ALTER TABLE `agents`
  ADD CONSTRAINT `agents_idetablissement_foreign` FOREIGN KEY (`idEtablissement`) REFERENCES `etablissements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `agents_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `categoriesproblemes`
--
ALTER TABLE `categoriesproblemes`
  ADD CONSTRAINT `categoriesproblemes_idcategorieetablissement_foreign` FOREIGN KEY (`idCategorieEtablissement`) REFERENCES `categoriesetablissements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `citoyens`
--
ALTER TABLE `citoyens`
  ADD CONSTRAINT `citoyens_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `commentaires`
--
ALTER TABLE `commentaires`
  ADD CONSTRAINT `commentaires_idreclamation_foreign` FOREIGN KEY (`idReclamation`) REFERENCES `reclamations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `commentaires_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `etablissements`
--
ALTER TABLE `etablissements`
  ADD CONSTRAINT `etablissements_idcategorieetablissement_foreign` FOREIGN KEY (`idCategorieEtablissement`) REFERENCES `categoriesetablissements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `etablissements_idmunicipalite_foreign` FOREIGN KEY (`idMunicipalite`) REFERENCES `municipalites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `modifications`
--
ALTER TABLE `modifications`
  ADD CONSTRAINT `modifications_idetat_foreign` FOREIGN KEY (`idEtat`) REFERENCES `etats` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `modifications_idreclamation_foreign` FOREIGN KEY (`idReclamation`) REFERENCES `reclamations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `municipalites`
--
ALTER TABLE `municipalites`
  ADD CONSTRAINT `municipalites_idgouvernorat_foreign` FOREIGN KEY (`idGouvernorat`) REFERENCES `gouvernorats` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `photos`
--
ALTER TABLE `photos`
  ADD CONSTRAINT `photos_idreclamation_foreign` FOREIGN KEY (`idReclamation`) REFERENCES `reclamations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `reclamations`
--
ALTER TABLE `reclamations`
  ADD CONSTRAINT `reclamations_idcategorieproleme_foreign` FOREIGN KEY (`idCategorieProleme`) REFERENCES `categoriesproblemes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamations_idcitoyen_foreign` FOREIGN KEY (`idCitoyen`) REFERENCES `citoyens` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamations_idmunicipalite_foreign` FOREIGN KEY (`idMunicipalite`) REFERENCES `municipalites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `semblable`
--
ALTER TABLE `semblable`
  ADD CONSTRAINT `semblable_idreclamation1_foreign` FOREIGN KEY (`idReclamation1`) REFERENCES `reclamations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `semblable_idreclamation2_foreign` FOREIGN KEY (`idReclamation2`) REFERENCES `reclamations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `semblable_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `suivre`
--
ALTER TABLE `suivre`
  ADD CONSTRAINT `suivre_idcitoyen_foreign` FOREIGN KEY (`idCitoyen`) REFERENCES `citoyens` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `suivre_idreclamation_foreign` FOREIGN KEY (`idReclamation`) REFERENCES `reclamations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `superadmins`
--
ALTER TABLE `superadmins`
  ADD CONSTRAINT `superadmins_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
