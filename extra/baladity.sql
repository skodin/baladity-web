-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 08 Juin 2016 à 11:02
-- Version du serveur :  5.6.28-0ubuntu0.15.10.1
-- Version de PHP :  5.6.11-1ubuntu3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `baladity`
--

-- --------------------------------------------------------

--
-- Structure de la table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `idUser` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idGouvernorat` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `admins`
--

INSERT INTO `admins` (`idUser`, `created_at`, `updated_at`, `idGouvernorat`) VALUES
(55, '2016-05-31 16:53:44', '2016-05-31 16:53:44', 2);

-- --------------------------------------------------------

--
-- Structure de la table `affectations`
--

CREATE TABLE IF NOT EXISTS `affectations` (
  `id` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idAgent` int(10) unsigned NOT NULL,
  `idReclamation` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `agents`
--

CREATE TABLE IF NOT EXISTS `agents` (
  `idUser` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idEtablissement` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `categoriesetablissements`
--

CREATE TABLE IF NOT EXISTS `categoriesetablissements` (
  `id` int(10) unsigned NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `categoriesetablissements`
--

INSERT INTO `categoriesetablissements` (`id`, `nom`, `created_at`, `updated_at`) VALUES
(1, 'Steg', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(2, 'Sonede', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(3, 'Onas', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(4, 'Municipalité', '2016-05-10 14:50:49', '2016-05-10 14:50:49');

-- --------------------------------------------------------

--
-- Structure de la table `categoriesproblemes`
--

CREATE TABLE IF NOT EXISTS `categoriesproblemes` (
  `id` int(10) unsigned NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idCategorieEtablissement` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `categoriesproblemes`
--

INSERT INTO `categoriesproblemes` (`id`, `nom`, `created_at`, `updated_at`, `idCategorieEtablissement`) VALUES
(1, 'Routes dégradées', '2016-05-10 14:50:50', '2016-05-10 14:50:50', 1),
(2, 'Dangers', '2016-05-10 14:50:50', '2016-05-10 14:50:50', 2),
(3, 'Pollution', '2016-05-10 14:50:50', '2016-05-10 14:50:50', 3),
(4, 'Moustiques', '2016-05-10 14:50:50', '2016-05-10 14:50:50', 4),
(5, 'Incivisme / Dépassement', '2016-05-10 14:50:50', '2016-05-10 14:50:50', 1),
(6, 'Problèmes d''eau', '2016-05-10 14:50:50', '2016-05-10 14:50:50', 1),
(7, 'Problèmes d''électricité', '2016-05-10 14:50:50', '2016-05-10 14:50:50', 2),
(8, 'Appropriation du trottoir', '2016-05-10 14:50:50', '2016-05-10 14:50:50', 2),
(9, 'Dos-d''âne', '2016-05-10 14:50:50', '2016-05-10 14:50:50', 2),
(10, 'Autres', '2016-05-10 14:50:50', '2016-05-10 14:50:50', 3);

-- --------------------------------------------------------

--
-- Structure de la table `citoyens`
--

CREATE TABLE IF NOT EXISTS `citoyens` (
  `idUser` int(10) unsigned NOT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estBlocker` tinyint(1) NOT NULL,
  `estConfirmer` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `commentaires`
--

CREATE TABLE IF NOT EXISTS `commentaires` (
  `id` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estAbuse` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idUser` int(10) unsigned NOT NULL,
  `idReclamation` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `etablissements`
--

CREATE TABLE IF NOT EXISTS `etablissements` (
  `id` int(10) unsigned NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idMunicipalite` int(10) unsigned NOT NULL,
  `idCategorieEtablissement` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `etablissements`
--

INSERT INTO `etablissements` (`id`, `nom`, `telephone`, `adresse`, `email`, `fax`, `site`, `created_at`, `updated_at`, `idMunicipalite`, `idCategorieEtablissement`) VALUES
(17, 'Sonede monastir', '78945612', 'Monastir', 'sonede.monastir@gmail.com', '97554125', 'http://sonede.monastir.tn', '2016-06-02 09:18:33', '2016-06-02 10:31:41', 31, 2);

-- --------------------------------------------------------

--
-- Structure de la table `etats`
--

CREATE TABLE IF NOT EXISTS `etats` (
  `id` int(10) unsigned NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `etats`
--

INSERT INTO `etats` (`id`, `nom`, `created_at`, `updated_at`) VALUES
(1, 'Nouvelle', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(2, 'Confirmé', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(3, 'Traitement en cours', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(4, 'Suppression', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(5, 'En Attente', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(6, 'Résolu', '2016-05-10 14:50:49', '2016-05-10 14:50:49');

-- --------------------------------------------------------

--
-- Structure de la table `gouvernorats`
--

CREATE TABLE IF NOT EXISTS `gouvernorats` (
  `id` int(10) unsigned NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `gouvernorats`
--

INSERT INTO `gouvernorats` (`id`, `nom`, `created_at`, `updated_at`) VALUES
(1, 'Tunis', '2016-05-10 14:50:48', '2016-05-10 14:50:48'),
(2, 'Monastir', '2016-05-10 14:50:48', '2016-05-10 14:50:48'),
(3, 'Sousse', '2016-05-10 14:50:48', '2016-05-10 14:50:48'),
(4, 'Mahdia', '2016-05-10 14:50:48', '2016-05-10 14:50:48'),
(5, 'Bizerte', '2016-05-10 14:50:48', '2016-05-10 14:50:48'),
(6, 'Beja', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(7, 'Ariana', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(8, 'Ben arous', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(9, 'Gabes', '2016-05-10 14:50:49', '2016-05-10 14:50:49'),
(10, 'Gafsa', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(11, 'Jendouba', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(12, 'Kairouan', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(13, 'Kasserine', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(14, 'Kebili', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(15, 'Manouba', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(16, 'Medenine', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(17, 'Nabeul', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(18, 'Sfax', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(19, 'Sidi bouzid', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(20, 'Siliana', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(21, 'Tataouine', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(22, 'Tozeur', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(23, 'Zaghouan', '2016-05-10 14:52:50', '2016-05-10 14:52:50'),
(24, 'Le Kef', '2016-05-10 14:52:50', '2016-05-10 14:52:50');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_02_18_222636_create_admins_table', 1),
('2016_02_18_222729_create_affectations_table', 1),
('2016_02_18_222749_create_agents_table', 1),
('2016_02_18_222836_create_categoriesEtablissement_table', 1),
('2016_02_18_222920_create_categoriesProbleme_table', 1),
('2016_02_18_222939_create_citoyens_table', 1),
('2016_02_18_223006_create_commentaires_table', 1),
('2016_02_18_223033_create_delegations_table', 1),
('2016_02_18_223118_create_etablissements_table', 1),
('2016_02_18_223132_create_etats_table', 1),
('2016_02_18_223201_create_gouvernorats_table', 1),
('2016_02_18_223225_create_modifications_table', 1),
('2016_02_18_223250_create_problemes_table', 1),
('2016_02_18_223418_create_superadmins_table', 1),
('2016_02_19_205147_create_foreignkey_table', 1);

-- --------------------------------------------------------

--
-- Structure de la table `modifications`
--

CREATE TABLE IF NOT EXISTS `modifications` (
  `id` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idEtat` int(10) unsigned NOT NULL,
  `idReclamation` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `municipalites`
--

CREATE TABLE IF NOT EXISTS `municipalites` (
  `id` int(10) unsigned NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idGouvernorat` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `municipalites`
--

INSERT INTO `municipalites` (`id`, `nom`, `created_at`, `updated_at`, `idGouvernorat`) VALUES
(11, 'Sidi Hassine', '2016-05-10 14:50:49', '2016-06-01 10:46:53', 1),
(28, 'Ettadhamen-Mnihla', '2016-06-01 10:42:52', '2016-06-01 10:42:52', 7),
(31, 'Monastir', '2016-06-01 10:51:56', '2016-06-01 10:51:56', 2),
(46, 'Moknine', '2016-06-01 10:58:58', '2016-06-01 10:58:58', 2),
(48, 'Kalâa Kebira', '2016-06-01 10:59:50', '2016-06-01 10:59:50', 3),
(57, 'Ksar Hellal', '2016-06-01 11:03:58', '2016-06-01 11:03:58', 2),
(63, 'Dar Chaâbane', '2016-06-01 11:06:10', '2016-06-01 11:06:10', 17),
(65, 'Korba', '2016-06-01 11:07:08', '2016-06-01 11:07:08', 17),
(66, 'Hammam Sousse', '2016-06-01 11:07:56', '2016-06-01 11:07:56', 3),
(67, 'Menzel Temime', '2016-06-01 11:08:24', '2016-06-01 11:08:24', 17),
(68, 'Tozeur', '2016-06-01 11:08:45', '2016-06-01 11:08:45', 22),
(69, 'Ezzahra', '2016-06-01 11:09:11', '2016-06-01 11:09:11', 8),
(70, 'Mateur', '2016-06-01 11:09:31', '2016-06-01 11:09:31', 5),
(71, 'Téboulba', '2016-06-01 11:09:49', '2016-06-01 11:09:49', 2),
(72, 'El Ksar', '2016-06-01 11:10:39', '2016-06-01 11:10:39', 10),
(73, 'Soliman', '2016-06-01 11:11:56', '2016-06-01 11:11:56', 17),
(74, 'Goulette', '2016-06-01 11:12:24', '2016-06-01 11:12:24', 1),
(75, 'Bou Mhel el-Bassatine', '2016-06-01 11:12:53', '2016-06-01 11:12:53', 8),
(76, 'Douz', '2016-06-01 11:13:13', '2016-06-01 11:13:13', 14),
(77, 'Manouba', '2016-06-01 11:13:40', '2016-06-01 11:13:40', 15),
(78, 'Thyna', '2016-06-01 11:14:04', '2016-06-01 11:14:04', 18),
(79, 'Mornag', '2016-06-01 11:14:22', '2016-06-01 11:14:22', 8),
(80, 'Ksour Essef', '2016-06-01 11:14:38', '2016-06-01 11:14:38', 4),
(81, 'Redeyef', '2016-06-01 11:14:58', '2016-06-01 11:14:58', 10);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(10) unsigned NOT NULL,
  `chemin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idReclamation` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `reclamations`
--

CREATE TABLE IF NOT EXISTS `reclamations` (
  `id` int(10) unsigned NOT NULL,
  `degree` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` double(9,6) NOT NULL,
  `latitude` double(9,6) NOT NULL,
  `flag` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idCitoyen` int(10) unsigned NOT NULL,
  `idMunicipalite` int(10) unsigned NOT NULL,
  `idCategorieProleme` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `semblable`
--

CREATE TABLE IF NOT EXISTS `semblable` (
  `id` int(10) unsigned NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idReclamation1` int(10) unsigned NOT NULL,
  `idReclamation2` int(10) unsigned NOT NULL,
  `idUser` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `suivre`
--

CREATE TABLE IF NOT EXISTS `suivre` (
  `id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idReclamation` int(10) unsigned NOT NULL,
  `idCitoyen` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `superadmins`
--

CREATE TABLE IF NOT EXISTS `superadmins` (
  `idUser` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `superadmins`
--

INSERT INTO `superadmins` (`idUser`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` enum('citoyen','agent','admin','superAdmin') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `username`, `nom`, `prenom`, `email`, `password`, `photo`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Riadh Rahmi', 'Rahmi Rahmi', 'Riadh', 'rahmiriadh@gmail.com', '$2y$10$fPOosJoxmQWwWSf.p2b4quEuFxAUJck.XqVLm/PcooSn0uWgd7JEW', 'images/users/user_1_2016_06_02_17_02_00.png', 'superAdmin', '2016-05-10 14:50:50', '2016-06-02 16:02:00'),
(54, 'Helmi hadj youssef', 'Helmi', 'Bel hadj youssef', 'Helmibelhadjyoussef@gmail.com', '$2y$10$hUq9aXzFZxKNA.7xjy.B4u.4jMxCnhhDgfng6PeAIdUGJHHf0WhWS', '', 'agent', '2016-05-31 16:36:01', '2016-05-31 16:48:48'),
(55, 'maher sakka', 'Sakka', 'Maher', 'mahersakka@gmail.com', '$2y$10$DiaeMTEvZCGwQ796mDrb2OFi9gXHcYTzrcph5ogHYjbFA20ogSQ3m', '', 'admin', '2016-05-31 16:53:43', '2016-05-31 16:58:07');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`idUser`),
  ADD KEY `admins_idgouvernorat_foreign` (`idGouvernorat`);

--
-- Index pour la table `affectations`
--
ALTER TABLE `affectations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `affectations_idagent_foreign` (`idAgent`),
  ADD KEY `affectations_idreclamation_foreign` (`idReclamation`);

--
-- Index pour la table `agents`
--
ALTER TABLE `agents`
  ADD PRIMARY KEY (`idUser`),
  ADD KEY `agents_idetablissement_foreign` (`idEtablissement`);

--
-- Index pour la table `categoriesetablissements`
--
ALTER TABLE `categoriesetablissements`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `categoriesproblemes`
--
ALTER TABLE `categoriesproblemes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoriesproblemes_idcategorieetablissement_foreign` (`idCategorieEtablissement`);

--
-- Index pour la table `citoyens`
--
ALTER TABLE `citoyens`
  ADD PRIMARY KEY (`idUser`);

--
-- Index pour la table `commentaires`
--
ALTER TABLE `commentaires`
  ADD PRIMARY KEY (`id`),
  ADD KEY `commentaires_iduser_foreign` (`idUser`),
  ADD KEY `commentaires_idreclamation_foreign` (`idReclamation`);

--
-- Index pour la table `etablissements`
--
ALTER TABLE `etablissements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `etablissements_idmunicipalite_foreign` (`idMunicipalite`),
  ADD KEY `etablissements_idcategorieetablissement_foreign` (`idCategorieEtablissement`);

--
-- Index pour la table `etats`
--
ALTER TABLE `etats`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `gouvernorats`
--
ALTER TABLE `gouvernorats`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `modifications`
--
ALTER TABLE `modifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `modifications_idetat_foreign` (`idEtat`),
  ADD KEY `modifications_idreclamation_foreign` (`idReclamation`);

--
-- Index pour la table `municipalites`
--
ALTER TABLE `municipalites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `municipalites_idgouvernorat_foreign` (`idGouvernorat`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Index pour la table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `photos_idreclamation_foreign` (`idReclamation`);

--
-- Index pour la table `reclamations`
--
ALTER TABLE `reclamations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reclamations_idcitoyen_foreign` (`idCitoyen`),
  ADD KEY `reclamations_idmunicipalite_foreign` (`idMunicipalite`),
  ADD KEY `reclamations_idcategorieproleme_foreign` (`idCategorieProleme`);

--
-- Index pour la table `semblable`
--
ALTER TABLE `semblable`
  ADD PRIMARY KEY (`id`),
  ADD KEY `semblable_idreclamation1_foreign` (`idReclamation1`),
  ADD KEY `semblable_idreclamation2_foreign` (`idReclamation2`),
  ADD KEY `semblable_iduser_foreign` (`idUser`);

--
-- Index pour la table `suivre`
--
ALTER TABLE `suivre`
  ADD PRIMARY KEY (`id`),
  ADD KEY `suivre_idreclamation_foreign` (`idReclamation`),
  ADD KEY `suivre_idcitoyen_foreign` (`idCitoyen`);

--
-- Index pour la table `superadmins`
--
ALTER TABLE `superadmins`
  ADD PRIMARY KEY (`idUser`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `affectations`
--
ALTER TABLE `affectations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `categoriesetablissements`
--
ALTER TABLE `categoriesetablissements`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `categoriesproblemes`
--
ALTER TABLE `categoriesproblemes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `commentaires`
--
ALTER TABLE `commentaires`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `etablissements`
--
ALTER TABLE `etablissements`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `etats`
--
ALTER TABLE `etats`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `gouvernorats`
--
ALTER TABLE `gouvernorats`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT pour la table `modifications`
--
ALTER TABLE `modifications`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT pour la table `municipalites`
--
ALTER TABLE `municipalites`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT pour la table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT pour la table `reclamations`
--
ALTER TABLE `reclamations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT pour la table `semblable`
--
ALTER TABLE `semblable`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `suivre`
--
ALTER TABLE `suivre`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=59;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `admins`
--
ALTER TABLE `admins`
  ADD CONSTRAINT `admins_idgouvernorat_foreign` FOREIGN KEY (`idGouvernorat`) REFERENCES `gouvernorats` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `admins_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `affectations`
--
ALTER TABLE `affectations`
  ADD CONSTRAINT `affectations_idagent_foreign` FOREIGN KEY (`idAgent`) REFERENCES `agents` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `affectations_idreclamation_foreign` FOREIGN KEY (`idReclamation`) REFERENCES `reclamations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `agents`
--
ALTER TABLE `agents`
  ADD CONSTRAINT `agents_idetablissement_foreign` FOREIGN KEY (`idEtablissement`) REFERENCES `etablissements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `agents_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `categoriesproblemes`
--
ALTER TABLE `categoriesproblemes`
  ADD CONSTRAINT `categoriesproblemes_idcategorieetablissement_foreign` FOREIGN KEY (`idCategorieEtablissement`) REFERENCES `categoriesetablissements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `citoyens`
--
ALTER TABLE `citoyens`
  ADD CONSTRAINT `citoyens_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `commentaires`
--
ALTER TABLE `commentaires`
  ADD CONSTRAINT `commentaires_idreclamation_foreign` FOREIGN KEY (`idReclamation`) REFERENCES `reclamations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `commentaires_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `etablissements`
--
ALTER TABLE `etablissements`
  ADD CONSTRAINT `etablissements_idcategorieetablissement_foreign` FOREIGN KEY (`idCategorieEtablissement`) REFERENCES `categoriesetablissements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `etablissements_idmunicipalite_foreign` FOREIGN KEY (`idMunicipalite`) REFERENCES `municipalites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `modifications`
--
ALTER TABLE `modifications`
  ADD CONSTRAINT `modifications_idetat_foreign` FOREIGN KEY (`idEtat`) REFERENCES `etats` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `modifications_idreclamation_foreign` FOREIGN KEY (`idReclamation`) REFERENCES `reclamations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `municipalites`
--
ALTER TABLE `municipalites`
  ADD CONSTRAINT `municipalites_idgouvernorat_foreign` FOREIGN KEY (`idGouvernorat`) REFERENCES `gouvernorats` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `photos`
--
ALTER TABLE `photos`
  ADD CONSTRAINT `photos_idreclamation_foreign` FOREIGN KEY (`idReclamation`) REFERENCES `reclamations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `reclamations`
--
ALTER TABLE `reclamations`
  ADD CONSTRAINT `reclamations_idcategorieproleme_foreign` FOREIGN KEY (`idCategorieProleme`) REFERENCES `categoriesproblemes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamations_idcitoyen_foreign` FOREIGN KEY (`idCitoyen`) REFERENCES `citoyens` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamations_idmunicipalite_foreign` FOREIGN KEY (`idMunicipalite`) REFERENCES `municipalites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `semblable`
--
ALTER TABLE `semblable`
  ADD CONSTRAINT `semblable_idreclamation1_foreign` FOREIGN KEY (`idReclamation1`) REFERENCES `reclamations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `semblable_idreclamation2_foreign` FOREIGN KEY (`idReclamation2`) REFERENCES `reclamations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `semblable_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `suivre`
--
ALTER TABLE `suivre`
  ADD CONSTRAINT `suivre_idcitoyen_foreign` FOREIGN KEY (`idCitoyen`) REFERENCES `citoyens` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `suivre_idreclamation_foreign` FOREIGN KEY (`idReclamation`) REFERENCES `reclamations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `superadmins`
--
ALTER TABLE `superadmins`
  ADD CONSTRAINT `superadmins_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
