'use strict';

var app = angular.module('myApp');


/*---------------------------------- admins Resource ----------------------------------------------*/

app.factory('adminsResource',['$resource', function ($resource) {

    return $resource('admins/:id',
              {id: '@id'},
               {update: {method: "PUT"}},
               {save: {
                    method: "POST",
                    transformRequest: angular.indentity,
                    headers: { 'Content-Type':undefined, enctype:'multipart/form-data' }
                  }}
               
               );

}]);




/*---------------------------------- gouvernorat Resource ----------------------------------------------*/

app.factory('gouvernoratResource',['$resource',  function ($resource) {
	
    return $resource('gouvernorats/:id',{id: '@id'},{update: {method: "PUT"}});
    
}]);





/*---------------------------------- établissement Resource ----------------------------------------------*/

app.factory('etablissementResource',['$resource',  function ($resource) {
	
    return $resource('etablissements/:id',{id: '@id'},{update: {method: "PUT"}});
    
}]);
	



/*----------------------------------catégorie établissement Resource ----------------------------------------------*/


app.factory('categoriesEtablissementsResource',['$resource',  function ($resource) {
	
    return $resource('categoriesEtablissements/:id',{id: '@id'},{update: {method: "PUT"}});
    
}]);




/*----------------------------------municipalites Resource ----------------------------------------------*/


app.factory('municipalitesResource',['$resource',  function ($resource) {
	
    return $resource('municipalites/:id',{id: '@id'},{update: {method: "PUT"}});
    
}]);




/*----------------------------------categorie Problemes Resource ----------------------------------------------*/


app.factory('categorieProblemesResource',['$resource',  function ($resource) {
	
    return $resource('categorieProblemes/:id',{id: '@id'},{update: {method: "PUT"}});
    
}]);







/*----------------------------------réclamation Resource ----------------------------------------------*/


app.factory('reclamationsResource',['$resource','$cookies',  function ($resource,$cookies) {
	
    return $resource('reclamations/:id',{id: '@id'},{update: {method: "PUT"}});
    
}]);


/*----------------------------------réclamation état ----------------------------------------------*/


app.factory('etatsResource',['$resource',  function ($resource) {
	
    return $resource('etats/:id',{id: '@id'},{update: {method: "PUT"}});
    
}]);




/*----------------------------------semblable----------------------------------------------*/


app.factory('semblableResource',['$resource',  function ($resource) {
  
    return $resource('semblances/:id',{id: '@id'},{update: {method: "PUT"}});
    
}]);







/*----------------------------------agents Resource ----------------------------------------------*/


app.factory('agentsResource',['$resource', function ($resource) {

    return $resource('agents/:id',
              {id: '@id'},
               {update: {method: "PUT"}},
               {save: {
	                    method: "POST",
	                    transformRequest: angular.indentity,
	                    headers: { 'Content-Type': undefined }
                     }
                }
               
               );

}]);









