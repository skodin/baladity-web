'use strict';

var app = angular.module('myApp');

app.service('customizeServices', ['$http', function($http){

   
   
       /*---------------------------------update Image User ---------------------------------------*/
       
       this.updatePhoto = function(id,photo,onSuccess,onError){
            var fd = new FormData();
            for(var i in photo ){
                fd.append("photo", photo[i]);
            }
            fd.append("id",id);                 
            $http.post("updateImageUser",fd, {
                transformRequest: angular.indentity,
                headers: { 'Content-Type': undefined }
            }).then(function (response) {

                onSuccess(response);

            }, function (response) {

                onError(response);

            });
        };
    
   
    




}]);