'use strict';

var app = angular.module('myApp');

app.factory('authServices',['$http','$cookies', function ($http, $cookies) {


    /*----------------------test if user is logged in----------------------------------*/

    function checkIfLoggedIn() {

        if ($cookies.get('token'))
            return true;
        else
            return false;

    }
    
    

    /*----------------------function login----------------------------------*/    

    function login(email, password, onSuccess, onError) {

        $http.post('login',{ email: email,password: password }).then(function (response) {
  
            onSuccess(response);

        }, function (response) {

            onError(response);

        });

    }
    



   /*--------------------------return functions ---------------------------------------------*/

    return {
        checkIfLoggedIn: checkIfLoggedIn,
        login: login
    };
    

}]);