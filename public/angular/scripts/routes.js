'use strict';


/*-----------------------------------my Module -------------------------------------*/

var app = angular.module("myApp", ['ngRoute','angular-carousel', 'ui.materialize', 'ngResource', 'ngMessages', 'ngCookies','ngMap','ui.tinymce','ngtimeago']);


app.constant("USER_ROLES", {
	    "ALL" :"all",
        "SUPER_ADMIN": "superAdmin",
        "ADMIN": "admin",
        "AGENT": "agent"
});



/*----------------------------------- add header to all request ------------------------------------------------*/

app.factory('httpRequestInterceptor', function ($cookies) {
  return {
    request: function (config) {
   
       config.headers['Authorization'] = ' Bearer ' + $cookies.get('token');
	
		config.headers['role'] = ''+ $cookies.get('role');

      return config;
    }
  };
});



/*-------------------------------------All routes ---------------------------------------------------*/

app.config(function($routeProvider,$locationProvider,$httpProvider,USER_ROLES) {
	
	
	$locationProvider.html5Mode(true);

   $httpProvider.interceptors.push('httpRequestInterceptor');

	/*------------------------------------------- routes for login----------------------------------------------- */
	$routeProvider.when('/administrateursLogin', {
		title: 'Authentification',
		templateUrl : 'angular/views/auth/login.html',
		controller : 'SuperAdminController',
		data: {
			      authorizedRoles: [USER_ROLES.ALL]
			  }
		
	}).when('/reset', {
		title: 'Mot de passe oublié',
		templateUrl : 'angular/views/auth/reset.html',
		controller : 'PasswordController',
		data: {
			      authorizedRoles: [USER_ROLES.ALL]
			  }
		
	}).when('/resetPassword/:token', {
		title: 'Réinitialiser Mot de passe',
		templateUrl : 'angular/views/auth/resetPassword.html',
		controller : 'PasswordController',
		data: {
			      authorizedRoles: [USER_ROLES.ALL]
			  }
		
	}).when('/profile', {
		title: 'Mon profile',
		templateUrl : 'angular/views/auth/profile.html',
		controller :  'IndexController',
		data: {
			      authorizedRoles: [USER_ROLES.ALL]
			  }
		
	})

	/*------------------------------------------- routes for administrateurs------------------------------------ */
	.when('/administrateursCreate', {
		title: 'Ajouter administrateur',
		templateUrl : 'angular/views/administrateurs/create.html',
		controller : 'AdministrateursController',
		data: {
			    authorizedRoles: [USER_ROLES.SUPER_ADMIN]
			  }
		
	}).when('/administrateursEdit/:id', {
		title: 'Modifier administrateur',
		templateUrl : 'angular/views/administrateurs/edit.html',
		controller : 'AdministrateursController',
		data: {
			      authorizedRoles: [USER_ROLES.SUPER_ADMIN]
			  }
		
	}).when('/administrateursIndex', {
		title: 'Listes des administrateurs',
		templateUrl : 'angular/views/administrateurs/index.html',
		controller : 'AdministrateursController',
		data: {
			      authorizedRoles: [USER_ROLES.SUPER_ADMIN]
			  }
	})

	/*------------------------------------------- routes for agents----------------------------------------------- */
	.when('/agentsCreate', {
		title: 'Ajouter agent',
		templateUrl : 'angular/views/agents/create.html',
		controller : 'AgentsController',
		data: {
			      authorizedRoles: [USER_ROLES.SUPER_ADMIN,USER_ROLES.ADMIN]
			  }
		
	}).when('/agentsEdit/:id', {
		title: 'Modifier Agent',
		templateUrl : 'angular/views/agents/edit.html',
		controller : 'AgentsController',
		data: {
			      authorizedRoles: [USER_ROLES.SUPER_ADMIN,USER_ROLES.ADMIN]
			  }
		
	}).when('/agentsIndex', {
		title: 'Listes des agents',
		templateUrl : 'angular/views/agents/index.html',
		controller : 'AgentsController',
		data: {
			      authorizedRoles: [USER_ROLES.SUPER_ADMIN,USER_ROLES.ADMIN]
			  }
	})

	/*------------------------------------------- routes for etablissements----------------------------------------------- */
	.when('/etablissementsCreate', {
		title: 'Ajouter établissement',
		templateUrl : 'angular/views/etablissements/create.html',
		controller : 'EtablissementsController',
		data: {
			      authorizedRoles: [USER_ROLES.SUPER_ADMIN,USER_ROLES.ADMIN]
			  }
		
	}).when('/etablissementsEdit/:id', {
		title: 'Modifier établissement',
		templateUrl : 'angular/views/etablissements/edit.html',
		controller : 'EtablissementsController',
		data: {
			      authorizedRoles: [USER_ROLES.SUPER_ADMIN,USER_ROLES.ADMIN]
			  }
		
	}).when('/etablissementsIndex', {
		title: 'Listes des établissements',
		templateUrl : 'angular/views/etablissements/index.html',
		controller : 'EtablissementsController',
		data: {
			      authorizedRoles: [USER_ROLES.SUPER_ADMIN,USER_ROLES.ADMIN]
			  }
	})

	/*------------------------------------------- routes for reclamations----------------------------------------------- */
	.when('/claimsEdit/:id', {
		title: 'Détail de réclamation',
		templateUrl : 'angular/views/reclamations/edit.html',
		controller : 'ReclamationsController',
		data: {
			      authorizedRoles: [USER_ROLES.ALL]
			  }
		
	}).when('/claimsIndex', {
		title: 'Listes des réclamations',
		templateUrl : 'angular/views/reclamations/index.html',
		controller : 'ReclamationsController',
		data: {
			      authorizedRoles: [USER_ROLES.ALL]
			  }
		
	})
	
	/*------------------------------------------routes of demandes-------------------------------------------------------*/
	
	.when('/SuppressionClaimsIndex', {
		title: 'Demandes de suppression',
		templateUrl : 'angular/views/demandes/index.html',
		controller : 'DemandeController',
		data: {
			      authorizedRoles: [USER_ROLES.SUPER_ADMIN,USER_ROLES.ADMIN]
			  }
		
	}).when('/editSuppression/:id', {
		title: 'Détail de  réclamation',
		templateUrl : 'angular/views/demandes/edit.html',
		controller : 'DemandeController',
		data: {
			      authorizedRoles: [USER_ROLES.SUPER_ADMIN,USER_ROLES.ADMIN]
			  }
		
	})

	/*--------------------------------------------routes for municipalites----------------------------------*/
	
	.when('/municipalitesIndex', {
		title: 'Listes des zones municipales',
		templateUrl : 'angular/views/municipalites/index.html',
		controller : 'MunicipalitesController',
		data: {
			      authorizedRoles: [USER_ROLES.SUPER_ADMIN,USER_ROLES.ADMIN]
			  }
	})

	/*--------------------------------------------route for contact----------------------------------*/
	
	.when('/contact', {
		title: 'Contact',
		templateUrl : 'angular/views/contact/create.html',
		controller : 'ContactController',
		data: {
			      authorizedRoles: [USER_ROLES.AGENT,USER_ROLES.ADMIN]
			  }
	})

    .when('/errors',{
    	title: 'Erreur',
    	templateUrl : 'angular/views/errors/404.html',
    	data: {
			      authorizedRoles: [USER_ROLES.ALL]
			  }
    })
    
    /*-----------------------------------route of Abuses---------------------------------------------------------*/
   
    .when('/abusesIndex',{
    	title: 'Listes des abuses',
    	templateUrl : 'angular/views/abuses/index.html',
    	controller : 'AbusesController',
    	data: {
			      authorizedRoles: [USER_ROLES.SUPER_ADMIN,USER_ROLES.ADMIN]
			  }
    	
    })
    
    .when('/abuseEdit/:id',{
    	title: "Détail d'une abuse",
    	templateUrl : 'angular/views/abuses/editAbuse.html',
    	controller : 'AbusesController',
    	data: {
			      authorizedRoles: [USER_ROLES.SUPER_ADMIN,USER_ROLES.ADMIN]
			  }
    	
    })
    
     /*-----------------------------------route of Abuses---------------------------------------------------------*/
   
    .when('/semblancesIndex',{
    	title: 'Listes des semblables',
    	templateUrl : 'angular/views/semblables/index.html',
    	controller : 'SemblablesController',
    	data: {
			      authorizedRoles: [USER_ROLES.SUPER_ADMIN,USER_ROLES.ADMIN]
			  }
    	
    })
    
    .when('/semblancesEdit/:id',{
    	title: "Détail d'une réclamation",
    	templateUrl : 'angular/views/semblables/edit.html',
    	controller : 'SemblablesController',
    	data: {
			      authorizedRoles: [USER_ROLES.SUPER_ADMIN,USER_ROLES.ADMIN]
			  }
    	
    })
    

	/*--------------------------------------------route otherwise----------------------------------*/
	.otherwise({

		redirectTo : '/administrateursLogin',
		data: {
			      authorizedRoles: [USER_ROLES.ALL]
			  }
	});


});



app.run(function($rootScope,$http, $cookies,authServices,USER_ROLES,$location) {
		
		if (!authServices.checkIfLoggedIn() && !$location.path().match("administrateursLogin")) {
		
		     $location.path('/administrateursLogin');
	     }	   
	    
	    
	    $rootScope.$on('$routeChangeStart', function (event, next) {
	
	  	 if(next.data.authorizedRoles[0] != USER_ROLES.ALL){
	
		  	 	if(next.data.authorizedRoles.indexOf(''+$cookies.get('role')) === -1 ){
		                  
			        $location.path('/errors');
			         
			        Materialize.toast("vous n'avez pas les droits d'accés !", 5000, "red");
		  	 	}
		  	}
		  });
		  
	    
	    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
	   
	        $rootScope.title = current.$$route.title;
	         	
	          if($cookies.get('role') ==="superAdmin"){
	
	            $rootScope.hideItem= false;
	            
	            $rootScope.hideItemAdmin= true;
	
	            $rootScope.hideItemContact=true;
	              
	          }else if($cookies.get('role')==="admin"){
	
	             $rootScope.hideItem= false;
	
	             $rootScope.hideItemAdmin= false;
	
	             $rootScope.hideItemContact=false;
	
	          }else if($cookies.get('role')==="agent"){
	
	            $rootScope.hideItem=true;
	
	            $rootScope.hideItemAdmin= false;
	
	            $rootScope.hideItemContact=false;
	
	          }	   
	    });

});










