'use strict';

var app = angular.module("myApp");

app.controller('SemblablesController',['$scope','$location','$cookies','$filter','$route', '$routeParams','$http','semblableResource','reclamationsResource','municipalitesResource','categorieProblemesResource','etatsResource', function($scope,$location,$cookies,$filter,$route, $routeParams,$http,semblableResource,reclamationsResource,municipalitesResource,categorieProblemesResource,etatsResource) {
	
	/*--------------------------------Define scopes--------------------------------------*/
	
	$scope.semlblances = [];

	$scope.Reclamation={};

	$scope.comments=[];

	$scope.user=$cookies.getObject('user');

    $scope.carouselIndex = 0;

	$scope.emptyArray=true;
	
	$scope.emptyPhotos=true;


    

   /*----------------------------------get liste of semlblances ----------------------------------------------------*/

	refresh($scope.user.id);
	
	function refresh(id) {
		
		$http.get("getSemblances/"+id).then(function (response) {

		    $scope.nbre=response.data.total; 
     	 	
           $scope.semlblances=response.data.data;
           
           $scope.emptyArray=$scope.semlblances.length > 0; 
           
		});  
	};
	

	
	
   /*----------------------------------change page------------------------------------------*/ 

	$scope.changePage=function(page){

		$http.get("getSemblances/"+$scope.user.id+"?page="+page).then(function (response) {

		   $scope.nbre=response.data.total; 
     	 	
           $scope.semlblances=response.data.data;

		});  
	};





    
  

  /*---------------get reclamation By Id---------------*/
 
    var editClaim = angular.isUndefined($routeParams.id);

	if (!editClaim) {
		
	   refreshReclamation();
	}
	
	function refreshReclamation () {
		
		
		$http.get('getDetailOfClaim/'+$routeParams.id).then(function (response) {
			
			$scope.Reclamation=response.data;
			
			$scope.emptyPhotos=$scope.Reclamation.photos.length > 0;

		},function(error){
			
	        $location.path('/errors');
	    });
		
	}
	
	
	/*--------------------------previous poicture-----------------------------------*/

	$scope.previous=function(){

		if($scope.carouselIndex > 0){

			$scope.carouselIndex--;

		}

	};
	


	/*--------------------------next picture-----------------------------------*/

	$scope.next=function(){

		if($scope.Reclamation.photos.length-1 > $scope.carouselIndex){

			$scope.carouselIndex++;

		}

	};

	
	
	

	/*--------------------comments of réclamation------------------------*/
	
	 if (!editClaim) {
	 	
	 	  getComments();
	 }
    

     function getComments() {
     	
     	 $http.get('showComments/'+$routeParams.id).then(function (response) {
     	 	
           $scope.comments=response.data;
           
        });
       
     }
    



	/*-------------------------------------delete réclamation--------------------------------------------------*/

	$scope.remove = function(id) {		
		
		angular.element('#demoModal').closeModal();
		
		 $http.delete('deleteClaim/' + id).then(function (response) {
           
           refresh($scope.user.id);
                    
        }, function (response) {

            Materialize.toast("Opération échoué",3000,"red");

        });

	};





	/*-----------------------------------remove réclamation from view update--------------------------------*/
	
	$scope.deleteClaim = function(id) {
		
		angular.element('#demoModalDelete').closeModal();
		
		$http.delete('deleteClaim/' + id).then(function (response) {

            $location.path('/semblancesIndex');

        }, function (error) {

            Materialize.toast("Opération échoué",3000,"red");

        });	
	};



   /*-----------------------when I click to remove réclamation ---------------------------*/
   
	$scope.setIdReclamation = function(IdReclamation) {
		
		$scope.IdReclamation = IdReclamation;
		
	};



  	/*------------------------------send comment-----------------------------------*/
	$scope.sendComment=function(isValid){

		if(isValid){

			$http.post('commentaires',{
				idReclamation: $routeParams.id,
				idUser:$scope.user.id,
				estAbuse:0,
				message:$scope.message,
				date: $filter('date')(new Date(), "yyyy-MM-dd HH:mm:ss")        

			}).then(function (response) {

				$scope.message="";

				$route.reload();

				getComments();

			});
		}
	};





}]);
