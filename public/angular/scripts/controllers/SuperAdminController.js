'use strict';
var app = angular.module('myApp');

app.controller('SuperAdminController',['$scope','$http', '$location','$cookies', '$routeParams','authServices','$window', function($scope, $http, $location,$cookies, $routeParams,authServices,$window) {

	/*--------------------------------------constant------------------------------------------*/	
	
	if (authServices.checkIfLoggedIn()) {
		
	  $location.path('/claimsIndex');
	}	


	/*--------------------------------function login--------------------------------------*/

	$scope.login = function(isValid) {

		if (isValid) {
			
			authServices.login($scope.email, $scope.password, function(response) {

				 $cookies.put('token', response.data.token);

				 $cookies.put('role', response.data.role);
               
                 $cookies.putObject('user', response.data);
                 
				 $location.path('/claimsIndex');	
				
			}, function(response) {
				
				Materialize.toast("vérifiez vos données svp !", 2000, "red");
			});
		}
	};

	






}]);
