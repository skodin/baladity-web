'use strict';

var app = angular.module('myApp');

app.controller('ContactController',['$scope', '$location','$route','$http', '$cookies', '$routeParams', '$resource', function($scope, $location,$route,$http, $cookies, $routeParams, $resource) {

    
	/*--------------------------------------------get current user ------------------*/

	$scope.user = $cookies.getObject('user');
	
	

	/*----------------------function contact----------------------------------*/

	$scope.contact = function(isValid) {

		if(isValid){
			$http.post('contacts', {
				email : $scope.user.email,
				message : $scope.message,
				sujet : $scope.sujet
			}).then(function(response) {
	
				Materialize.toast(" opération avec succés ", 1000, "green");
				
				$scope.message="";

				$scope.sujet="";
                 
                 $route.reload();
	
			}, function(response) {

				Materialize.toast(" opération echoué ", 1000, "red");
			});
		}

	};

    /*------------------------------options tinymce---------------------------------------------*/

	 $scope.tinymceOptions = {
	    plugins: 'link image code',
	    toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code',
	    height: 250
	  };
	  
  
	
}]);
