'use strict';

var app = angular.module("myApp");

app.controller('MunicipalitesController', ['$scope','$http','$cookies','$timeout', '$location', '$route', '$routeParams', 'municipalitesResource', 'gouvernoratResource',
function($scope,$http,$cookies,$timeout, $location, $route, $routeParams, municipalitesResource, gouvernoratResource) {

	/*------------Define scopes----------------*/
	$scope.municipalites = [];

	$scope.municipalite = {};
	
	$scope.user=$cookies.getObject('user');
	
	$scope.emptyArray=true;
	
	$scope.isAdmin=false;
	
	
	
	if($scope.user.role=="admin"){
		 
		 $scope.isAdmin=true;     	      	 
	 }
	 
	 

	/*------------------delete Municipalite------------*/

	$scope.remove = function(id) {

		angular.element('#demoModal').closeModal();

		municipalitesResource.delete({id : id}, function() {

			refresh($scope.user.id);

		});
	};
	
	

	/*-----------------save Municipalite--------------*/

	$scope.ajouter = function() {


			$scope.Muni = {};

			$scope.Muni.nom = $scope.addNom;
			
			if($scope.user.role=="admin"){
				
				$scope.Muni.idGouvernorat = $cookies.get('idGouvernorat');
	
			}else{
				
				$scope.Muni.idGouvernorat = $scope.addIdMuni;
			}

			angular.element('#demoModalSave').closeModal();

			municipalitesResource.save($scope.Muni, function() {

				Materialize.toast("Opération avec succés ", 3000, "green");

				refresh($scope.user.id);

				$scope.addNom = "";

				$scope.addIdMuni = "";

				$route.reload();

			},function(error){
				
				Materialize.toast("Opération échoué ", 3000, "red");
				
			});

	};

	/*-----------------------------------hide Modal Insert---------------------------------------*/

	$scope.hideModalInsert = function() {

		angular.element('#demoModalSave').closeModal();

	};

	
	
	/*-------------------------------------------get All Gouvernorats-------------------*/

	$scope.gouvernorats = gouvernoratResource.query();
	
	

	/*---------- get All municipalites ----------*/

	refresh($scope.user.id);

	function refresh(id) {
		
		$http.get('getMunicipalites/'+id).then(function(response){
			
			$scope.municipalites=response.data;	
			
			$scope.emptyArray=$scope.municipalites.length > 0; 
				
		});
		
	};
	
	
	
	

	/*-----------------------update municipalite------------------------------*/

	$scope.update = function(isValid) {

		if (isValid) {
			
			angular.element('#demoModalUpdate').closeModal();

			municipalitesResource.update($scope.municipalite,function(response){
				
				Materialize.toast("Opération avec succés ", 3000, "green");

				refresh($scope.user.id);

				$route.reload();
				
			},function(error){
				
				Materialize.toast("Opération échoué", 3000, "red");
				
			});
		}
	};




	/*-----------------------------------hide Modal Update---------------------------------------*/

	$scope.hideModalUpdate = function() {

		angular.element('#demoModalUpdate').closeModal();

	};



	/*-----------------------when I click to remove municipalite ---------------------------*/

	$scope.setIdMunicipalite = function(IdMunicipalite) {

		$scope.IdMunicipalite = IdMunicipalite;
	};



	/*-------------------------------------------------------------------------------------*/

	$scope.setIdMunicipaliteUpdate = function(IdMunicipalite) {

		$scope.municipalite = municipalitesResource.get({

			id : IdMunicipalite
		});
	};

}]);
