'use strict';

var app = angular.module("myApp");

app.controller('AdministrateursController',['$scope','$cookies','$http', '$location', '$routeParams','$resource', 'gouvernoratResource','adminsResource','customizeServices',function($scope,$cookies,$http,$location, $routeParams,$resource, gouvernoratResource,adminsResource,customizeServices) {

	/*-------------------------ALL Admins---------------------------------*/
	
	$scope.admins = [];
	
	$scope.emptyArray=true;

     $scope.user=$cookies.getObject('user');
     

 

	/*------------------------------delete Admin--------------------------------------*/
	
	$scope.remove = function(id) {
		
		angular.element('#demoModal').closeModal();
		
		adminsResource.delete({id : id},function() {
		  
		  refresh();
		
		});
	};



	/*--------------------------------set photo---------------------------------------*/
	
	$scope.setFiles = function(element) {
		$scope.$apply(function($scope) {
			$scope.photo = [];
			for (var i = 0; i < element.files.length; i++) {
				$scope.photo.push(element.files[i]);
			}
		});
	};




	/*---------------------------------save Admin-------------------------------------*/
	$scope.ajout = function(isValid) {

		if(isValid){

				 var fd = new FormData();
	        for(var i in $scope.photo ){
	            fd.append("photo", $scope.photo[i]);
	        }
	        fd.append("nom",$scope.nom);
	        fd.append("prenom",$scope.prenom);
	        fd.append("username",$scope.username);
	        fd.append("email",$scope.email);
	        fd.append("password",$scope.password);
	        fd.append("role","admin");
	        fd.append("idGouvernorat",$scope.selectedOption);
			
			$resource('admins/:id',
		              {id: '@id'},
		               {save: {
		                    method: "POST",
		                    params:fd,
		                    transformRequest: angular.indentity,
		                    headers: { 'Content-Type':undefined}
		                  }
		                }
		               
		               ).save(fd).$promise.then(function(response){
		               	
			               	Materialize.toast("Opération avec succés ", 3000, "green");
							
						    $location.path('/administrateursIndex');
		               	
		               }).catch(function(error){
		               	
		               	  Materialize.toast("Adresse email existe déja ", 3000, "red");
		               	  
		               });
		}
	   
	};





	/*-------------------------------------------get All Gouvernorats-------------------*/
	
	$scope.gouvernorats = gouvernoratResource.query();




	/*----------------------------------------get All admins By Default-------------------*/
	
	refresh();
    
    function refresh() {
           	
		$http.get("admins").then(function(response) {
			
			$scope.nbre=response.data.total; 

			$scope.admins = response.data.data;

			$scope.emptyArray=$scope.admins.length > 0; 

		});
	};
	
	
	/*----------------------------------change page------------------------------------------*/ 

	$scope.changePage=function(page){

		$http.get("admins?page="+page).then(function (response) {

			$scope.nbre=response.data.total; 

			$scope.admins = response.data.data;

		});  
	};
	



	/*----------------------------------------get Admin By Id---------------------*/
	
	 var editAdmin = angular.isUndefined($routeParams.id);

	if (!editAdmin) {
         
         $scope.Admin = adminsResource.get({ id : $routeParams.id},function(response){
	       
	        //alert($scope.Admin.idGouvernorat);
	     
	     },function(err){
	     
	        $location.path('/errors');
	    
	    });
         
      }
	
	
    /*---------------------------update Image Agent-----------------------------*/
   
   $scope.updateImage=function(element){
   	
   	    $scope.photo = [];
   	
   	    $scope.$apply(function($scope) {
			
			for (var i = 0; i < element.files.length; i++) {
				
				$scope.photo.push(element.files[i]);
			}
		});	

        customizeServices.updatePhoto($scope.Admin.user.id,$scope.photo,function(response){
        	
	            $scope.Admin.user.photo=response.data.result;
	            
	            Materialize.toast("Opération avec succés ", 3000, "green");
	        
	        },function(response){
	        
	             Materialize.toast("Opération échoué ", 3000, "red");
	        
	        }
	    );
         
   };




	/*------------------------------------- update administrateur ---------------------------------------*/
	
	$scope.update = function(isValid) {
     	
		if(isValid){

			$scope.User={};
	     	$scope.User.id=$routeParams.id;
	     	$scope.User.nom=$scope.Admin.user.nom;
	     	$scope.User.prenom=$scope.Admin.user.prenom;
	     	$scope.User.username=$scope.Admin.user.username;
	     	$scope.User.email=$scope.Admin.user.email;
	     	$scope.User.idGouvernorat=$scope.Admin.idGouvernorat;
	     	
			adminsResource.update($scope.User,function(response){
	            
	           Materialize.toast("Opération avec succés ", 3000, "green");
			
			    $location.path('/administrateursIndex');
	            
	        },function(error){
	        	
	        	 Materialize.toast("Opération échoué ", 3000, "red");
	        
	        });
		}
	};





    /*---------------------------------------set IdAdmin when I click remove -------------------------------------------*/
    
	$scope.setIdAdmin = function(idAdmin) {
		
		$scope.idAdmin = idAdmin;
	
	};

}]);
