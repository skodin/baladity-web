'use strict';
var app = angular.module('myApp');

app.controller('PasswordController',['$scope', '$http', '$location', '$routeParams',function($scope, $http, $location, $routeParams) {

     

	/*---------------------reset Password ----------------------------------*/

	$scope.resetPassword = function(isValid) {

		if (isValid) {
			$http.post('sendReset',{
				email : $scope.email,
				token: randomString()
			}).then(function(response) {
                 
				Materialize.toast("opération avec succés",1000, "green");

			}, function(response) {

				Materialize.toast("opération échoué", 1000, "red");

			});
		}
		

	};
	


	
	/*----------------------------update Password---------------------------------*/
	
	$scope.updatePassword=function(isValid){

		var testeConfirmation=$scope.password==$scope.passwordConfirmation;
		
		if (isValid && testeConfirmation) {
			$http.post('updatePassword',{
				email : $scope.email,
				password: $scope.password,
				token :$routeParams.token
			}).then(function(response) {

               Materialize.toast("opération avec succés", 1000, "green");

               $location.path('/administrateursLogin');

                 
			}, function(response) {

				Materialize.toast("opération échoué", 1000, "red");

			});
		}

		
	};




  /*-----------------------------generate Random-------------------------------------------*/
 
   function randomString() {
	 var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
	 var string_length = 64;
	 var randomstring = '';
	 for (var i=0; i<string_length; i++) {
	  var rnum = Math.floor(Math.random() * chars.length);
	  randomstring += chars.substring(rnum,rnum+1);
	 }
	 return randomstring;
   }




}]);
