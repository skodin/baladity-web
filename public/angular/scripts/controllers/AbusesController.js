'use strict';

var app = angular.module("myApp");

app.controller('AbusesController',['$scope','$route','$location','$cookies','$filter','$routeParams','$http','reclamationsResource','municipalitesResource','categorieProblemesResource','etatsResource', function($scope,$route,$location,$cookies,$filter, $routeParams,$http,reclamationsResource,municipalitesResource,categorieProblemesResource,etatsResource) {

	/*--------------------------------------------get current user ------------------*/
     
     $scope.user=$cookies.getObject('user');

     $scope.carouselIndex = 0;

	 $scope.emptyArray=true;
	
	 $scope.emptyPhotos=true;

	
	/*--------------------------------Define scopes--------------------------------------*/
	
	$scope.reclamations = [];
	
	$scope.Reclamation={};
	
	$scope.comments=[];
	

  /*---------------get reclamation By Id---------------*/
 
   var editClaim = angular.isUndefined($routeParams.id);

	if (!editClaim) {
		
		refreshReclamation();
	}
	
	
	
	function refreshReclamation () {
		
		$http.get('getDetailOfClaim/'+$routeParams.id).then(function (response) {
			
			$scope.Reclamation=response.data;
			
			$scope.emptyPhotos=$scope.Reclamation.photos.length > 0;

		},function(error){
			
	        $location.path('/errors');
	    });
	}
	
	
	
	/*--------------------------previous poicture-----------------------------------*/

	$scope.previous=function(){

		if($scope.carouselIndex > 0){

			$scope.carouselIndex--;

		}

	};
	


	/*--------------------------next picture-----------------------------------*/

	$scope.next=function(){

		if($scope.Reclamation.photos.length-1 > $scope.carouselIndex){

			$scope.carouselIndex++;

		}

	};

	
	



	/*---------------------------------------Autorise suppression réclamation------------------------------------*/
	
	$scope.remove = function(id) {		
		
		 $http.delete('deleteClaim/' + id).then(function (response) {
    	  
    	    refresh($scope.user.id);
		 
        }, function (error) {

            Materialize.toast("Opération échoué",3000,"red");
        });
	};
	
	

	
	
	/*-----------------------------------remove réclamation from view update--------------------------------*/
	
	$scope.deleteClaim = function(id) {
		
		$http.delete('deleteClaim/'+id).then(function (response) {

           $location.path('/abusesIndex');

        }, function (response) {

            Materialize.toast("Opération échoué",3000,"red");

        });	
		
	};
	

    


	/*---------- get All réclamations ----------*/
	
	refresh($scope.user.id);

	function refresh(id) {
		
		$http.get('getAbuses/'+id).then(function (response) {
		
		   $scope.nbre=response.data.total; 
     	 	
           $scope.reclamations=response.data.data;

           $scope.emptyArray=$scope.reclamations.length > 0;   
           
        });
	};



  /*----------------------------------change page------------------------------------------*/ 

	$scope.changePage=function(page){

		$http.get("getAbuses/"+$scope.user.id+"?page="+page).then(function (response) {

		   $scope.nbre=response.data.total; 
     	 	
           $scope.reclamations=response.data.data;

		});  
	};



	/*--------------------comments of réclamation------------------------*/
	
	if (!editClaim) {
		
		getComments();
	
	}
    

     function getComments() {
     	
     	 $http.get('showComments/'+$routeParams.id).then(function (response) {
     	 	
           $scope.comments=response.data;
           
        });
       
     }


	/*------------------------------send comment-----------------------------------*/
	$scope.sendComment=function(isValid){

		if(isValid){

			$http.post('commentaires',{
				idReclamation: $routeParams.id,
				idUser:$scope.user.id,
				estAbuse:0,
				message:$scope.message,
				date: $filter('date')(new Date(),"yyyy-MM-dd HH:mm:ss")        

			}).then(function (response) {

				$scope.message="";

				$route.reload();

				getComments();

			});
		}
	};



    /*-----------------------when I click to remove réclamation ---------------------------*/
   
	$scope.setIdReclamation = function(IdReclamation) {
		
		$scope.IdReclamation = IdReclamation;
	};
	
	
	
	


}]);

