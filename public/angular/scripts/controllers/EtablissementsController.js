'use strict';

var app = angular.module("myApp");

app.controller('EtablissementsController', ['$scope','$cookies', '$http', '$location', '$routeParams', '$rootScope', 'municipalitesResource', 'categoriesEtablissementsResource', 'etablissementResource',
function($scope,$cookies,$http, $location, $routeParams, $rootScope, municipalitesResource, categoriesEtablissementsResource, etablissementResource) {

	/*----------------------Define scopes----------------------------------------*/
	$scope.user=$cookies.getObject('user');
	
	$scope.etablissements = [];

	$scope.Etab = {};

	$scope.emptyArray = true;
	
	


	/*------------------------------- get All etablisements --------------------------*/

	refresh($scope.user.id);

	function refresh(id) {

		$http.get("getEstablishement/"+id).then(function(response) {

			$scope.nbre = response.data.total;

			$scope.etablissements = response.data.data;

			$scope.emptyArray = $scope.etablissements.length > 0;

		});
	};




	/*-----------------------------------change page-------------------------------------------*/

	$scope.changePage = function(page) {

		$http.get("getEstablishement/"+$scope.user.id+"?page=" + page).then(function(response) {

			$scope.nbre = response.data.total;

			$scope.etablissements = response.data.data;

		});
	};




	/*------------------delete Etablisement------------*/

	$scope.remove = function(id) {
		
		angular.element('#demoModal').closeModal();

		etablissementResource.delete({id : id},function(response){
			
			 refresh($scope.user.id);
		});

	};



    getMunicipalites($scope.user.id);

	function getMunicipalites(id) {
		
		$http.get('getMunicipalites/'+id).then(function(response){
			
			$scope.municipalites=response.data;	
				
		});
		
	};
	
		
	
	/*----------get All categories Etablissemnts-------*/
	
	$scope.categoriesEtablissements = categoriesEtablissementsResource.query();




	/*-------------------------------get Etablissement by id------------------------------*/
    var editEtab = angular.isUndefined($routeParams.id);
	


	if (!editEtab) {

		$http.get('getEtabs/'+$scope.user.id+'/'+$routeParams.id).then(function(response){
			
			$scope.Etablissement=response.data;	
				
		},function(error){
          
          $location.path('/errors');
               
		});

	}

		
		
	

	
	
	
	/*-----------------save Etablissement--------------*/

	$scope.ajouter = function(isValid) {

		if (isValid) {

			etablissementResource.save($scope.Etab,function(){
				
				Materialize.toast("Opération avec succés", 3000, "green");
		
			   $location.path('/etablissementsIndex');
			
			},function(error){
				
				Materialize.toast("Opération échoué", 3000, "red");
				
			});
		}
	};





	
	
	
	/*-----------------------update établissement------------------------------*/

	$scope.update = function(isValid) {

		if (isValid) {

			etablissementResource.update($scope.Etablissement,function(response){
				
				Materialize.toast("Opération avec succés", 3000, "green");
               
                $location.path('/etablissementsIndex');
            
			},function(){
				
				Materialize.toast("Opération échoué", 3000, "red");
				
			});
		}
	};
	
	

	/*-----------------------when I click to remove etablissement ---------------------------*/

	$scope.setIdEtablissement = function(IdEtablissement) {

		$scope.IdEtablissement = IdEtablissement;
	};

}]);
