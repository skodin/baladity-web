'use strict';

var app = angular.module("myApp");

app.controller('IndexController',['$scope','$location','$cookies','$http', '$routeParams','authServices','customizeServices', function($scope,$location,$cookies,$http, $routeParams,authServices,customizeServices) {

    /*---------------------------------------templates----------------------------------*/
      $scope.global={};
   
      $scope.global.sideNavUrl="angular/views/template.html";
      
       $scope.isActive = function (viewLocation) { 
       
	    return $location.path().match(viewLocation);
	
	  };
	  
      
       $scope.user=$cookies.getObject('user');
      
       $scope.person=$cookies.getObject('user');     
   
      
        	  

    try {
	     	 if($scope.user.role=="superAdmin"){
	      	
	      	  $scope.profile="angular/views/profile/superAdmin.html"; 
		
		      }else if($scope.user.role=="admin"){
		      	
	      	$http.get("getAdmin/"+$scope.user.id).then(function (response) {
	
				$scope.gouvernorat=response.data.gouvernorat.nom;
				
				$cookies.put('idGouvernorat', response.data.gouvernorat.id); 
	
			});  
	
	      	 $scope.profile="angular/views/profile/admin.html";
	      	 
	      }else if($scope.user.role=="agent") {
	      	
	      		$http.get("getAgent/"+$scope.user.id).then(function (response) {
	
				$scope.establishement=response.data.etablissement.nom; 
	
			   });  
	      	
	      	 $scope.profile="angular/views/profile/agent.html";
	      }
       
    }catch(e) { 
    	//
     }
     
      
     
    /*------------------------------------------function logout-----------------------*/
     
      $scope.logout = function(){

           $cookies.remove('token');
           
           $cookies.remove('role');  
           
           $cookies.remove('user'); 	
        
           $location.path('/administrateursLogin');	
        
      };
     
     
     
    /*--------------------------------update profile--------------------------------------------*/
     
      $scope.updateImage=function(element){
   	
   	    $scope.photo = [];
         $scope.profile={};
   	
   	    $scope.$apply(function($scope) {
			
			for (var i = 0; i < element.files.length; i++) {
				
				$scope.photo.push(element.files[i]);
			}
		});	

      customizeServices.updatePhoto($scope.user.id,$scope.photo,
            function(response){
                $scope.user.photo=response.data.result;
                $scope.profile.id=$scope.user.id;
                $scope.profile.nom=$scope.user.nom;
                $scope.profile.prenom=$scope.user.prenom;
                $scope.profile.username=$scope.user.username;
                $scope.profile.email=$scope.user.email;
                $scope.profile.password="";
                $scope.profile.photo=response.data.result;
                $scope.profile.role=$scope.user.role;
                $scope.profile.token=$scope.user.token;
                $cookies.putObject('user',$scope.profile);

                Materialize.toast("opération avec succés ", 1000, "green");
            },
            function(response){
            	
                 Materialize.toast("opération échoué ", 1000, "red");
            });
         
   };


   /*---------------------------------------updateProfile----------------------------------------------*/
   
    $scope.updateProfile=function(isValid){
         	
     if(isValid){

          $scope.profile={};
          
          $scope.profile.nom=$scope.person.nom;
          $scope.profile.prenom=$scope.person.prenom;
          $scope.profile.username=$scope.person.username;
          $scope.profile.email=$scope.person.email;
         
          var ifPassword= angular.isUndefined($scope.person.password);
         
          if(!ifPassword){
            $scope.profile.password=$scope.person.password;
          }
          
          $http.put($scope.user.role+'/'+$scope.user.id,$scope.profile).then(function (response) {
                
                Materialize.toast("Modification avec succés",1000,"blue");
                $scope.profile.id=$scope.user.id;
                $scope.profile.password="";
                $scope.profile.photo=$scope.user.photo;
                $scope.profile.role=$scope.user.role;
                $scope.profile.token=$scope.user.token;
                $cookies.putObject('user',$scope.profile);
                
                $location.path('claimsIndex');

            }, function (error) {

                Materialize.toast("Opération échoué",1000,"red");

            });  
        }
   };


}]); 