'use strict';

var app = angular.module("myApp");

app.controller('DemandeController',['$scope','$route', '$location','$cookies','$filter', '$routeParams','$http','reclamationsResource','municipalitesResource','categorieProblemesResource','etatsResource', function($scope,$route,$location,$cookies,$filter, $routeParams,$http,reclamationsResource,municipalitesResource,categorieProblemesResource,etatsResource) {

	 /*--------------------------------------------get current user ------------------*/
     
      $scope.user=$cookies.getObject('user');

	  $scope.carouselIndex = 0;

	  $scope.emptyArray=true;
	
      $scope.emptyPhotos=true;

	/*--------------------------------Define scopes--------------------------------------*/
		
	$scope.Reclamation={};
	
	$scope.DeleteReclamations = [];

	$scope.comments=[];
	
	$scope.etats=[];
	
	$scope.idClaims=[];
	


  /*---------------get reclamation By Id---------------*/
 
    var editClaim = angular.isUndefined($routeParams.id);

	if (!editClaim) {
		
		refreshReclamation();
	}	
	
	
	function refreshReclamation () {
		
		$http.get('getDetailOfClaim/'+$routeParams.id).then(function (response) {
			
			$scope.Reclamation=response.data;
			
			$scope.emptyPhotos=$scope.Reclamation.photos.length > 0;

		},function(error){
			
	        $location.path('/errors');
	    });
	
	}
	
	
	/*--------------------------previous poicture-----------------------------------*/

	$scope.previous=function(){

		if($scope.carouselIndex > 0){

			$scope.carouselIndex--;

		}

	};
	


	/*--------------------------next picture-----------------------------------*/

	$scope.next=function(){

		if($scope.Reclamation.photos.length-1 > $scope.carouselIndex){

			$scope.carouselIndex++;

		}

	};

	



	/*---------------------------------------Autorise suppression réclamation------------------------------------*/
	
	$scope.remove = function(id) {		
		
		 $http.delete('deleteClaim/' + id).then(function (response) {

           getSupressionClaims($scope.user.id);
		

        }, function (error) {

            Materialize.toast("Opération échoué",3000,"red");

        });
		
		
	};
	
	
	
	
	
	/*------------------------------------RéAffecter Réclamation------------------------------------------------*/
	
	$scope.reaffecte=function(id){
		
		$http.post('affectations',{
			idAgent :  $scope.user.id,
			idReclamation : $routeParams.id,
			message : $scope.messageAffectation,
			idMunicipalite : $scope.Reclamation.idMunicipalite,
			idCategorieProleme: $scope.Reclamation.idCategorieProleme
			
		}).then(function (response) {

            Materialize.toast("Opération avec succés",3000,"green");
            
            $scope.messageAffectation="";

        }, function (error) {

            Materialize.toast("Opération échoué",3000,"red");

        });	
		
		
	};
	
	
	
	
	
	/*-----------------------------------remove réclamation from view update--------------------------------*/
	
	$scope.deleteClaim = function(id) {
		
		$http.delete('deleteClaim/' + id).then(function (response) {

           $location.path('/SuppressionClaimsIndex');

        }, function (error) {

            Materialize.toast("Opération échoué",3000,"red");

        });	
		
	};
	
	
	
	
	
	/*--------------------------------demande Suppression------------------------------------*/

     $scope.demandeSupp=function(id) {
		
		$http.post('modifications', {
                idReclamation: id,
                idEtat:3,
                date:$filter('date')(new Date(), "yyyy-MM-dd HH:mm:ss"),
                message:'demande de suppression'         
                
            }).then(function (response) {

            Materialize.toast("Opération avec succés",3000,"green");

        }, function (response) {

            Materialize.toast("Opération échoué",3000,"red");

        });	
		
	};

	
    /*----------------------------------------------get All municipalites-------------------------------------*/
	
		
	$scope.municipalites = municipalitesResource.query();
   
   
    $scope.categorieProblemes=categorieProblemesResource.query();

   
	


	/*--------------------comments of réclamation------------------------*/
	
	if (!editClaim) {
		
		getComments();
	}
	

     function getComments() {
     	
     	 $http.get('showComments/'+$routeParams.id).then(function (response) {
     	 	
           $scope.comments=response.data;
           
        }); 
        
     }



	/*------------------------------send comment-----------------------------------*/
	$scope.sendComment=function(isValid){

		if(isValid){

			$http.post('commentaires',{
				idReclamation: $routeParams.id,
				idUser:$scope.user.id,
				estAbuse:0,
				message:$scope.message,
				date: $filter('date')(new Date(), "yyyy-MM-dd HH:mm:ss")        

			}).then(function (response) {

				$scope.message="";

				$route.reload();

				getComments();

			});

		}
	};




    /*-----------------------update agent------------------------------*/
   
     $scope.update=function(){
     	
		reclamationsResource.update($scope.Reclamation,function(response){
			
			Materialize.toast("Opération avec succés",3000,"green");
			
			$location.path('/claimsIndex');
			
		},function(error){
			
			Materialize.toast("Opération échoué",3000,"red");
	
		});
		
	};






    /*-----------------------when I click to remove réclamation ---------------------------*/
   
	$scope.setIdReclamation = function(IdReclamation) {
		
		$scope.IdReclamation = IdReclamation;
		
	};
	
	
	
	/*-----------------------when I click to remove réclamation ---------------------------*/
   
	$scope.setIdReclamationSemblable = function(IdReclamation) {
		
		$scope.IdReclamation = IdReclamation;
		
		 $http.get('getidClaims/'+IdReclamation).then(function (response) {
     	 	
           $scope.idClaims=response.data;
           
        });
	};
	


	
	
	
	/*------------------------------------add Semblable-------------------------------------------*/
	
	 $scope.addSemblable=function(id){
	 	
	 	$http.post('semblances',{
	 		idReclamation1: id,
	 		idReclamation2 : $scope.idRec,
	 		message : $scope.messageSemblable,
	 		idUser : $scope.user.id
	 		
	 	}).then(function (response) {
     	 	
          Materialize.toast("Opération avec succés",3000,"green");
           
        }, function (response) {

           Materialize.toast("Opération échoué ",3000,"red");

        });

	};
	
	
	
	
	
	
	/*------------------------------les réclamations à suprimés------------------------------------------*/

      getSupressionClaims($scope.user.id);

     function getSupressionClaims(id) {
     	
     	 $http.get('getSuppressionClaim/'+id).then(function (response) {
     	 	
     	   $scope.nbre=response.data.total; 
     	 	
           $scope.DeleteReclamations=response.data.data;

           $scope.emptyArray=$scope.DeleteReclamations.length > 0; 
           
        });
     };
     
     
     
    /*----------------------------------change page------------------------------------------*/ 

	$scope.changePage=function(page){

		$http.get("getSuppressionClaim/"+$scope.user.id+"?page="+page).then(function (response) {

			$scope.nbre=response.data.total; 

			$scope.DeleteReclamations = response.data.data;

		});  
	};







    /*---------------------------------les etats ------------------------------------*/
        if (!editClaim) {        	
        	getEtats();	
        } 
      
      
      function getEtats() {
      	
      	 $scope.etats=etatsResource.query();
      
      };
      
    
     


     

    /*----------------------------update Etat-------------------------------*/
   
    $scope.updateEtat=function(){
    	$http.post('modifications', {
                idReclamation: $routeParams.id,
                idEtat:$scope.selectedEtat,
                date:$filter('date')(new Date(), "yyyy-MM-dd HH:mm:ss"),
                message:$scope.messageEtat         
                
            }).then(function (response) {

            refreshReclamation();     
            $scope.messageEtat="";
            getEtats();
            
            Materialize.toast("Opération avec succés",3000,"green");

        }, function (error) {

            Materialize.toast("Opération échoué",3000,"red");

        });	
	};






}]);
