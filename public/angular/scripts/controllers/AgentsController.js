'use strict';

var app = angular.module("myApp");

app.controller('AgentsController',['$scope','$cookies','$location', '$routeParams','$http','$resource','agentsResource','etablissementResource','customizeServices', function($scope,$cookies,$location, $routeParams,$http,$resource,agentsResource,etablissementResource,customizeServices) {

	/*------------Define scopes----------------*/
	
	$scope.agents = [];
	
	$scope.emptyArray=true;

    $scope.user=$cookies.getObject('user');


	/*------------------delete Agent------------*/
	
	$scope.remove = function(id) {
		
		angular.element('#demoModal').closeModal();
		
		agentsResource.delete({id : id},function(){
		 
		  refresh($scope.user.id);
		
		});
	};



	/*--------------------set photo in Add Agent ----------------------------*/
	
	$scope.setFiles = function(element) {
		
		$scope.$apply(function($scope) {
			
			$scope.photo = [];
			
			for (var i = 0; i < element.files.length; i++) {
				
				$scope.photo.push(element.files[i]);
			}
		});
	};
	



	/*-----------------save Etablissement--------------*/
	
	$scope.insert = function(isValid) {
		
		if(isValid){
			 var fd = new FormData();
		
        for(var i in $scope.photo ){
        	
            fd.append("photo", $scope.photo[i]);
        }
        
        fd.append("nom",$scope.nom);
        
        fd.append("prenom",$scope.prenom);
        
        fd.append("username",$scope.username);
        
        fd.append("email",$scope.email);
        
        fd.append("password",$scope.password);
        
        fd.append("role","agent");
        
        fd.append("idEtablissement",$scope.selectedOption);
	    
	    $resource('agents/:id',
	              {id: '@id'},
	               {save: {
	                    method: "POST",
	                    params:fd,
	                    transformRequest: angular.indentity,
	                    headers: { 'Content-Type':undefined}
	                  }
	                }
	               
	               ).save(fd).$promise.then(function(res){
	               	
		               	Materialize.toast("Opération avec succés", 3000, "green");

						$location.path('/agentsIndex');
	               	
	               }).catch(function(err){
	               	
	               	  Materialize.toast("Adresse email existe déja ", 3000, "red");
	               	
	               });	
		}

	};

	

	

	/*------------------------get All etablissements--------------------------*/
	

	getEtablissements($scope.user.id);

	function getEtablissements(id) {

		$http.get("getEstablishement/"+id).then(function(response) {

			$scope.etablissements = response.data.data;

		});
	};



	/*------------------------------- get All agents ------------------------------------*/
	
	refresh($scope.user.id);
		
	function refresh(id) {
		
		$http.get("getAgents/"+id).then(function(response) {
			
			$scope.nbre=response.data.total; 

			$scope.agents = response.data.data;

			$scope.emptyArray=$scope.agents.length > 0; 

		}); 
	};
	
	
	/*----------------------------------change page------------------------------------------*/ 

	$scope.changePage=function(page){

		$http.get("getAgents/"+$scope.user.id+"?page="+page).then(function (response) {

			$scope.nbre=response.data.total; 

			$scope.agents = response.data.data;

		});  
	};
	



	/*---------------get Agent By Id---------------*/
	
	var editAgent = angular.isUndefined($routeParams.id);

	$scope.val={};

	if (!editAgent) {	

		$http.get("getAgnt/"+$scope.user.id+"/"+$routeParams.id).then(function(response) {
			
			$scope.Agent =response.data;

		},function(error){

			 $location.path('/errors');
		}); 

	}
	
	




   /*---------------------------update Image Agent-----------------------------*/
   
   $scope.updateImage=function(element){
   	
   	    $scope.photo = [];
   	
   	    $scope.$apply(function($scope) {
			
			for (var i = 0; i < element.files.length; i++) {
				
				$scope.photo.push(element.files[i]);
			}
		});	

      customizeServices.updatePhoto($scope.Agent.user.id,$scope.photo,function(response){
               
                $scope.Agent.user.photo=response.data.result;
                
                 Materialize.toast("Opération avec succés", 3000, "green");
           
            },function(response){
           
                 Materialize.toast("Opération échoué", 3000, "red");
           
            });
         
   };



    /*-----------------------update agent------------------------------*/
   
     $scope.update=function(isValid){
     	console.log($scope.Agent.idEtablissement);
  	
        if(isValid){

        	$scope.User={};
	     	$scope.User.id=$routeParams.id;
	     	$scope.User.nom=$scope.Agent.user.nom;
	     	$scope.User.prenom=$scope.Agent.user.prenom;
	     	$scope.User.username=$scope.Agent.user.username;
	     	$scope.User.email=$scope.Agent.user.email;
	     	$scope.User.idEtablissement=$scope.Agent.idEtablissement;
	     	
			agentsResource.update($scope.User,function(response){
				
				Materialize.toast("Opération avec succés",3000,"green");
			
			    $location.path('/agentsIndex');
				
			},function(error){
				Materialize.toast("Opération échoué",3000,"red");
			});
        }
        
	};



    /*-----------------------when I click to remove agent ---------------------------*/
   
	$scope.setIdAgent = function(IdAgent) {
		
		$scope.IdAgent = IdAgent;
	};







}]);
