'use strict';

var app = angular.module("myApp");

app.controller('ReclamationsController',['$scope','$location','$cookies','$filter','$route', '$routeParams','$http','reclamationsResource','municipalitesResource','categorieProblemesResource','etatsResource', function($scope,$location,$cookies,$filter,$route, $routeParams,$http,reclamationsResource,municipalitesResource,categorieProblemesResource,etatsResource) {

	
   
   /*--------------------------------------------get current user ------------------*/

	$scope.user=$cookies.getObject('user');

	$scope.carouselIndex = 0;

	$scope.emptyArray=true;
	
	$scope.emptyPhotos=true;


	
	if($scope.user.role === "agent"){

		$scope.titleModalDelete ="Demande de Suppression";

		$scope.messageDelete ="Demande de supprimer cette Réclamation ?";

	}else{

		$scope.titleModalDelete ="Confirmation";

		$scope.messageDelete ="Vous voulez supprimer cette Réclamation ?";
	}



	

	
	/*--------------------------------Define scopes--------------------------------------*/
	
	$scope.reclamations = [];
	
	$scope.Reclamation={};
	
	$scope.comments=[];
	
	$scope.etats=[];
	
	$scope.idClaims=[];



	
	/*---------- get All réclamations ----------*/
	
	refresh($scope.user.id);

	function refresh(id) {

		$http.get("getClaims/"+id).then(function (response) {

			$scope.nbre=response.data.total; 

			$scope.reclamations=response.data.data;

			$scope.emptyArray=$scope.reclamations.length > 0;              

		});  

	};
	
	



	/*------------------------change page-----------------------------*/ 

	$scope.changePage=function(page){

		$http.get("getClaims/"+$scope.user.id+"?page="+page).then(function (response) {

			$scope.nbre=response.data.total;

			$scope.reclamations=response.data.data;

		});  

	};



	/*--------------------------previous poicture-----------------------------------*/

	$scope.previous=function(){

		if($scope.carouselIndex > 0){

			$scope.carouselIndex--;

		}

	};
	


	/*--------------------------next picture-----------------------------------*/

	$scope.next=function(){

		if($scope.Reclamation.photos.length-1 > $scope.carouselIndex){

			$scope.carouselIndex++;

		}

	};



	/*---------------get reclamation By Id---------------*/
	
	var editClaim = angular.isUndefined($routeParams.id);

	if (!editClaim) {
		
	  refreshReclamation();
	}
	
	
	function refreshReclamation () {
		
		$http.get('getDetailOfClaim/'+$routeParams.id).then(function (response) {
			
			$scope.Reclamation=response.data;
			
			$scope.emptyPhotos=$scope.Reclamation.photos.length > 0;
			
			if(angular.isUndefined($scope.Reclamation.etablissement) || $scope.Reclamation.etablissement === null){
				
				$scope.Reclamation.etablissement="-";
			}

		},function(error){
			
	        $location.path('/errors');
	    });
		
	}


	
	/*----------------------------------------------get All municipalites-------------------------------------*/
	
	$scope.municipalites = municipalitesResource.query();

	$scope.categorieProblemes=categorieProblemesResource.query();



	/*---------------------------------les etats ------------------------------------*/
     
     if (!editClaim) {
     	
     	getEtats();
     }
    

	function getEtats() {

		$scope.etats=etatsResource.query();

	};


	
	/*--------------------comments of réclamation------------------------*/
	 if (!editClaim) {
	   
	   getComments();
	  
	 }
	

	function getComments() {

		$http.get('showComments/'+$routeParams.id).then(function (response) {

			$scope.comments=response.data;

		});

	}
	
	
	
	/*------------------------------------RéAffecter Réclamation------------------------------------------------*/
	
	$scope.reaffecte=function(id,isValid){

		if(isValid){
			
		angular.element('#demoReaffecter').closeModal();
		
		$http.post('affectations',{
			idReclamation : $routeParams.id,
			message : $scope.messageAffectation,
			idMunicipalite : $scope.Reclamation.idMunicipalite,
			idCategorieProleme: $scope.Reclamation.idCategorieProleme
			
		}).then(function (response) {

			Materialize.toast("Opération avec succés",3000,"green");

			$scope.messageAffectation="";

			$scope.Reclamation.idMunicipalite="";

			$scope.Reclamation.idCategorieProleme="";
             
            $route.reload();

		}, function (response) {

			Materialize.toast("Opération échoué",3000,"red");

		});	
		}
		
	};
	
	
	
	
	
	/*--------------------------------demande Suppression------------------------------------*/

	$scope.demandeSupp=function(id,i) {

		if($scope.user.role === "agent"){

			$http.post('modifications', {
				idReclamation: id,
				idEtat:4,
				date:$filter('date')(new Date(), "yyyy-MM-dd HH:mm:ss"),
				message:'Demande de suppression de cette réclamation'         

			}).then(function (response) {

				Materialize.toast("Opération avec succés",3000,"green");

			}, function (error) {

				Materialize.toast("Opération échoué",3000,"red");

			});	

		}else{


			$http.delete('deleteClaim/' + id).then(function (response) {

				Materialize.toast("Opération avec succés",3000,"green");

				if(i === 1){
					$location.path('/claimsIndex');
				}else{
                  refresh($scope.user.id);
				}

			}, function (response) {

				Materialize.toast("Opération échoué",3000,"red");

			});
		}

	};



	/*------------------------------send comment-----------------------------------*/
	$scope.sendComment=function(isValid){

		if(isValid){

			$http.post('commentaires',{
				idReclamation: $routeParams.id,
				idUser:$scope.user.id,
				estAbuse:0,
				message:$scope.message,
				date: $filter('date')(new Date(), "yyyy-MM-dd HH:mm:ss")        

			}).then(function (response) {

				$scope.message="";

				$route.reload();

				getComments();

			});

		}
	};



	/*-----------------------update réclamation------------------------------*/

	$scope.update=function(){

		reclamationsResource.update($scope.Reclamation,function(response){
			
			Materialize.toast("Opération avec succés",3000,"green");
		
		    $location.path('/claimsIndex');
		    
		},function(error){
			
			Materialize.toast("Opération échoué ",3000,"red");
			
		});
	};



	/*-----------------------when I click to remove réclamation ---------------------------*/

	$scope.setIdReclamation = function(IdReclamation) {
		
		$scope.IdReclamation = IdReclamation;
		
	};
	
	
	/*-----------------------when I click to remove réclamation ---------------------------*/

	$scope.setIdReclamationSemblable = function(IdReclamation) {
		
		$scope.IdReclamation = IdReclamation;
		
		$http.get('getidClaims/'+IdReclamation).then(function (response) {

			$scope.idClaims=response.data;

		});
	};
	
	
	
	
	/*------------------------------------add Semblable-------------------------------------------*/
	
	$scope.addSemblable=function(isValid,id){
		
		if(isValid){
		
		    angular.element('#demoModalSemblable').closeModal();
		
			$http.post('semblances',{
			idReclamation1: id,
			idReclamation2 : $scope.idRec,
			message : $scope.messageSemblable,
			idUser : $scope.user.id }).then(function (response) {
	
				Materialize.toast("Opération avec succés",3000,"green");
	
				$scope.idRec="";
	
				$scope.messageSemblable="";
	
				$route.reload();

			}, function (response) {
	
				Materialize.toast("Opération échoué",3000,"red");
	
			});
		}

	};
	
	



	/*----------------------------update Etat-------------------------------*/

	$scope.updateEtat=function(isValid){	
		
		if(isValid){
			
			$http.post('modifications', {
			idReclamation: $routeParams.id,
			idEtat:$scope.selectedEtat,
			date:$filter('date')(new Date(), "yyyy-MM-dd HH:mm:ss"),
			message:$scope.messageEtat         

			}).then(function (response) {
				Materialize.toast("Opération avec succés",3000,"green");
				refreshReclamation();   
				getEtats();
				$scope.messageEtat="";
				$scope.selectedEtat = "";
				
				 $route.reload();			
	
			}, function (response) {
	
				Materialize.toast("Opération échoué",3000,"red");
	
			});	
		}
	};
	
	
	
	/*-------------------------------hide Modal Reaffceter-----------------------------------*/
	
	$scope.hideModalReaffceter=function(){
		
		angular.element('#demoReaffecter').closeModal();
		
	};
	
	/*-------------------------------hide Modal Reaffceter-----------------------------------*/
	
	$scope.hideModalSemblable=function(){
		
		angular.element('#demoModalSemblable').closeModal();
		
	};
	
	


}]);
