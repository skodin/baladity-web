'use strict';

var myapp = angular.module('myApp');

myapp.filter('ucFirst',function(){
	
    return function (x){
         if (!x || !x.length) { return; }
        return x.charAt(0).toUpperCase()+ x.slice(1);
    };
    
});

myapp.filter('getdate',function(){
	
    return function (x){
        if (!x || !x.length) { return; }
        return x.slice(0,10);
    };
    
});

myapp.filter('getdateTime',function(){
	
    return function (x){
        if (!x || !x.length) { return; }
        return x.slice(0,16);
    };
    
});