'use strict';

var myapp = angular.module('myApp');

myapp.directive('backToTop', function(){
	
	return {
		restrict: 'E'
		, replace: true
		, template: '<a class="btn btn-icon waves-effect  back-to-top"><i class="material-icons white-text">expand_less</i></a>'
		, link: function($scope, element, attrs) {

			$(window).scroll(function(){

				if ($(window).scrollTop() <= 0) {
					$(element).fadeOut();
				}
				else {
					$(element).fadeIn();
				}

			});

			$(element).on('click', function(){
				$('html, body').animate({ scrollTop: 0 }, 'fast');
			});

		}
	};

});


app.directive("passwordVerify", function() {
   return {
      require: "ngModel",
      scope: {
        passwordVerify: '='
      },
      link: function(scope, element, attrs, ctrl) {
        scope.$watch(function() {
            var combined;

            if (scope.passwordVerify || ctrl.$viewValue) {
               combined = scope.passwordVerify + '_' + ctrl.$viewValue; 
            }                    
            return combined;
        }, function(value) {
            if (value) {
                ctrl.$parsers.unshift(function(viewValue) {
                    var origin = scope.passwordVerify;
                    if (origin !== viewValue) {
                        ctrl.$setValidity("passwordVerify", false);
                        return undefined;
                    } else {
                        ctrl.$setValidity("passwordVerify", true);
                        return viewValue;
                    }
                });
            }
        });
     }
   };
});